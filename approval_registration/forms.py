"""
"""

from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _


User = get_user_model()


class ActivationForm(forms.Form):
    """Form for activating a user account.

    Requires the password to be entered twice to catch typos.

    Subclasses should feel free to add any additional validation they need, but
    should avoid defining a ``save()`` method -- the actual saving of collected
    user data is delegated to the active registration backend.

    """

    required_css_class = 'required'

    password1 = forms.CharField(widget=forms.PasswordInput, label=_("Password"))

    password2 = forms.CharField(widget=forms.PasswordInput, label=_("Verify Password"))

    def clean(self):
        """Check the passed two password are equal
        Verifiy that the values entered into the two password fields match.
        Note that an error here will end up in ``non_field_errors()`` because it
        doesn't apply to a single field.

        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields didn't match."))
        return self.cleaned_data


class RegistrationForm(forms.Form):
    """Form for registration a user account.

    Validates that the requested username is not already in use, and requires
    the email to be entered twice to catch typos.

    Subclasses should feel free to add any additional validation they need, but
    should avoid defining a ``save()`` method -- the actual saving of collected
    user data is delegated to the active registration backend.

    """

    required_css_class = 'required'

    username = forms.RegexField(
        regex=r'^[\w.@+-]{3,}$',
        max_length=30,
        label=_("Username"),
        help_text=_("Between 3 and 30 characters. Only letters, numbers or @/./+/-/_ ."),
        error_messages={
            'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")
        },
    )

    email1 = forms.EmailField(
        label=_("E-mail"),
        help_text=_("A real address please. You will receive a confirmation mail.")
    )

    email2 = forms.EmailField(
        label=_("Confirm E-mail"),
        help_text=_("The same. Just to avoid miss-typing."),
    )

    justification = forms.CharField(
        required=False,
        label=_("Request justification"),
        help_text=_("An optional message for the administrators of the site. <br/>"
                    "You may explain why do you need access here."),
        widget=forms.Textarea,
    )

    def clean_username(self):
        """Validate that the username is alphanumeric and is not already in use."""
        try:
            User.objects.get(username__iexact=self.cleaned_data['username'])
        except User.DoesNotExist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_("A user with that username already exists."))

    def clean(self):
        """Check the passed two email are equal
        Verifiy that the values entered into the two email fields match.
        Note that an error here will end up in ``non_field_errors()`` because it
        doesn't apply to a single field.

        """
        if 'email1' in self.cleaned_data and 'email2' in self.cleaned_data:
            if self.cleaned_data['email1'] != self.cleaned_data['email2']:
                raise forms.ValidationError(_("The two email fields didn't match."))
        return self.cleaned_data


class RegistrationFormTermsOfService(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which adds a required checkbox for agreeing
    to a site's Terms of Service.

    """

    tos = forms.BooleanField(widget=forms.CheckboxInput,
                             label=_(u'I have read and agree to the Terms of Service'),
                             error_messages={'required': _("You must agree to the terms to register.")})


class RegistrationFormUniqueEmail(RegistrationForm):
    """Subclass of ``RegistrationForm`` which enforces uniqueness of email address"""

    def clean_email1(self):
        """Validate that the supplied email address is unique for the site."""
        if User.objects.filter(email__iexact=self.cleaned_data['email1']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email1']


class RegistrationFormNoFreeEmail(RegistrationForm):
    """
    Subclass of ``RegistrationForm`` which disallows registration with email
    addresses from popular free webmail services; moderately useful for
    preventing automated spam registration.

    To change the list of banned domains, subclass this form and override the
    attribute ``bad_domains``.

    """

    bad_domains = ['aim.com', 'aol.com', 'email.com', 'gmail.com',
                   'googlemail.com', 'hotmail.com', 'hushmail.com',
                   'msn.com', 'mail.ru', 'mailinator.com', 'live.com',
                   'yahoo.com']

    def clean_email1(self):
        """Check the supplied email address against a list of known free webmail domains."""
        email_domain = self.cleaned_data['email1'].split('@')[1]
        if email_domain in self.bad_domains:
            raise forms.ValidationError(_("Free email addresses are forbidden. Please supply a different email address."))
        return self.cleaned_data['email1']
