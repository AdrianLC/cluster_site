"""
"""

from django.conf import settings

from models import RegistrationRequest
from utils import get_site, generate_random_password

import signals


class Backend(object):
    pass


def register(username, email, request, request_justification=''):
    """
    """
    send_email = getattr(settings, 'REGISTRATION_REGISTRATION_EMAIL', True)
    site = get_site(request)

    new_user = RegistrationRequest.objects.register(
        username, email,
        site,
        send_email=send_email,
        request_justification=request_justification
    )

    signals.user_registered.send(
        sender=Backend,
        user=new_user,
        request=request,
    )

    return new_user


def accept(registration_request, request, message=None):
    """
    """
    send_email = getattr(settings, 'REGISTRATION_ACCEPTANCE_EMAIL', True)
    site = get_site(request)

    accepted_user = RegistrationRequest.objects.accept_registration(
        registration_request,
        site,
        send_email=send_email,
        message=message
    )

    if accepted_user:
        signals.user_accepted.send(
            sender=Backend,
            user=accepted_user,
            registration_request=registration_request,
            request=request,
        )

    return accepted_user


def reject(registration_request, request, message=None):
    """
    """
    send_email = getattr(settings, 'REGISTRATION_REJECTION_EMAIL', True)
    site = get_site(request)

    rejected_user = RegistrationRequest.objects.reject_registration(
        registration_request,
        site,
        send_email=send_email,
        message=message
    )

    if rejected_user:
        signals.user_rejected.send(
            sender=Backend,
            user=rejected_user,
            registration_request=registration_request,
            request=request,
        )

    return rejected_user


def activate(activation_key, request, password=None, message=None, delete_registration_request=True):
    """
    """
    send_email = getattr(settings, 'REGISTRATION_ACTIVATION_EMAIL', True)
    site = get_site(request)

    is_generated = not password

    if is_generated:
        pass_length = getattr(settings, 'REGISTRATION_GENERATED_PASSWORD_LENGTH', 10)
        password = generate_random_password(pass_length)

    activated = RegistrationRequest.objects.activate_user(
        activation_key=activation_key,
        site=site,
        password=password,
        send_email=send_email,
        message=message,
        is_generated=is_generated,
        delete_registration_request=delete_registration_request
    )

    if activated:
        user, password, is_generated = activated
        signals.user_activated.send(
            sender=Backend,
            user=user,
            request=request,
        )
        return user
    return
