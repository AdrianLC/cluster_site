"""
"""

from django.dispatch import Signal

# A new user has registered.
user_registered = Signal(providing_args=["user", "request"])

# A user has activated his or her account.
user_activated = Signal(providing_args=["user", "request"])

# A user has been accepted his/her registration
user_accepted = Signal(providing_args=['user', 'registration_request', 'request'])

# A user has been rejected his/her registration
user_rejected = Signal(providing_args=['user', 'registration_request', 'request'])
