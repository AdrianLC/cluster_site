"""
"""

from django.db import transaction
from django.http import HttpResponseRedirect
from django.contrib import admin
from django.contrib.admin.util import unquote
from django.core.exceptions import PermissionDenied
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_protect
csrf_protect = method_decorator(csrf_protect)
from django.utils.translation import ugettext_lazy as _

from ..models import RegistrationRequest

from ..utils import get_site

from forms import RegistrationRequestAdminForm

import approval_registration.backend as backend


class RegistrationRequestAdmin(admin.ModelAdmin):

    """Admin class of RegistrationRequest

    Admin users can accept/reject registration and activate user in Django Admin
    page.

    ``RegistrationRequest`` is not supposed to be handled by hand thus adding/changing/deleting
    is not accepted even in Admin page. ``RegistrationProfile`` can only be
    accepted/rejected or activated. To prevent these disallowed functions, the special
    AdminForm called ``RegistrationAdminForm`` is used. Its ``save`` method is overridden
    and it actually does not save the instance. It just calls ``accept``, ``reject`` or
    ``activate`` method of current registration backend. So you don't want to override
    the ``save`` method of the form.

    """
    list_display = ('user', 'get_status_display', 'activation_key_expired')
    raw_id_fields = ['user']
    search_fields = ('user__username', 'user__first_name', 'user__last_name')
    list_filter = ('_status', )

    form = RegistrationRequestAdminForm

    readonly_fields = ('user', '_status', 'justification')

    actions = (
        'accept_users',
        'reject_users',
        'force_activate_users',
        'resend_acceptance_email',
    )

    def get_form(self, request, obj=None, **kwargs):
        generated_form = super(RegistrationRequestAdmin, self).get_form(request, obj, **kwargs)
        generated_form.request = request
        return generated_form

    def has_add_permission(self, request):
        """registration profile should not be created by hand"""
        return False

    def has_delete_permission(self, request, obj=None):
        """registration profile should not be created by hand"""
        return False

    def has_accept_permission(self, request, obj):
        """whether the user has accept permission"""
        return request.user.has_perm('approval_registration.accept_registration', obj)

    def has_reject_permission(self, request, obj):
        """whether the user has reject permission"""
        return request.user.has_perm('approval_registration.reject_registration', obj)

    def has_activate_permission(self, request, obj):
        """whether the user has activate permission"""
        return request.user.has_perm('approval_registration.activate_user', obj)

    def get_actions(self, request):
        """get actions displaied in admin site

        RegistrationProfile should not be deleted in admin site thus
        'delete_selected' is disabled in default.

        Each actions has permissions thus delete the action if the accessed
        user doesn't have appropriate permission.

        """
        actions = super(RegistrationRequestAdmin, self).get_actions(request)
        del actions['delete_selected']
        if not request.user.has_perm('approval_registration.accept_registration'):
            del actions['accept_users']
        if not request.user.has_perm('approval_registration.reject_registration'):
            del actions['reject_users']
        if not request.user.has_perm('approval_registration.accept_registration') or \
           not request.user.has_perm('approval_registration.activate_user'):
            del actions['force_activate_users']
        return actions

    def accept_users(self, request, queryset):
        """Accept the selected users, if they are not already accepted"""
        for registration_request in queryset:
            backend.accept(registration_request, request)
    accept_users.short_description = _("Accept registrations of selected users")

    def reject_users(self, request, queryset):
        """Reject the selected users, if they are not already accepted"""
        for registration_request in queryset:
            backend.reject(registration_request, request)
    reject_users.short_description = _("Reject registrations of selected users")

    def force_activate_users(self, request, queryset):
        """Activates the selected users, if they are not already activated"""
        self.accept_users(request, queryset)

        for registration_request in queryset:
            backend.activate(registration_request.activation_key, request)
    force_activate_users.short_description = _("Activate selected users forcibly")

    def resend_acceptance_email(self, request, queryset):
        """Re-sends acceptance emails for the selected users

        Note that this will *only* send acceptance emails for users
        who are eligible to activate; emails will not be sent to users
        whose activation keys have expired or who have already
        activated or rejected.

        """
        site = get_site(request)
        for registration_request in queryset:
            if not registration_request.activation_key_expired():
                registration_request.send_acceptance_email(site=site)
    resend_acceptance_email.short_description = _("Re-send acceptance emails to selected users")

    @csrf_protect
    @transaction.atomic
    def change_view(self, request, object_id, form_url='', extra_context=''):
        """called for change view

        Check permissions of the admin user for ``POST`` request depends on what
        action is requested and raise PermissionDenied if the action is not
        accepted for the admin user.

        """
        obj = self.get_object(request, unquote(object_id))

        # Permissin check
        if request.method == 'POST':
            action = request.POST.get('action')
            if action == 'accept' and not self.has_accept_permission(request, obj):
                raise PermissionDenied
            elif action == 'reject' and not self.has_reject_permission(request, obj):
                raise PermissionDenied
            elif action == 'activate' and not self.has_activate_permission(request, obj):
                raise PermissionDenied
            elif action == 'force_activate' and (
                not self.has_accept_permission(request, obj) or
                not self.has_activate_permission(request, obj)
            ):
                raise PermissionDenied

        response = super(RegistrationRequestAdmin, self).change_view(request, object_id, extra_context)
        if request.method == 'POST' and action in ('activate', 'force_activate'):
            # if the requested data is valid then response will be an instance
            # of ``HttpResponseRedirect`` otherwise ``TemplateResponse``
            if isinstance(response, HttpResponseRedirect):
                obj.delete()
        return response

admin.site.register(RegistrationRequest, RegistrationRequestAdmin)
