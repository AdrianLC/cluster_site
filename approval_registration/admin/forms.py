"""
"""

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from ..models import RegistrationRequest

import approval_registration.backend as backend


class RegistrationRequestAdminForm(forms.ModelForm):
    """A special form for handling ``RegistrationRequest``

    This form handle ``RegistrationRequest`` correctly in ``save()``
    method. Because ``RegistrationRequest`` is not assumed to be handled
    by hand, instance modification by hand is not allowed. Thus subclasses
    should feel free to add any additions they need, but should avoid overriding
    a ``save()`` method.

    """

    UNTREATED_ACTIONS = (
        ('accept', _('Accept this registration')),
        ('reject', _('Reject this registration')),
        ('force_activate', _('Activate the associated user of this registration forcibly')),
    )
    ACCEPTED_ACTIONS = (
        ('activate', _('Activate the associated user of this registration')),
    )
    REJECTED_ACTIONS = (
        ('accept', _('Accept this registration')),
        ('force_activate', _('Activate the associated user of this registration forcibly')),
    )

    action = forms.ChoiceField(label=_('Action'))
    message = forms.CharField(
        label=_('Message'),
        widget=forms.Textarea,
        required=False,
        help_text=_(
            'You can use the value of this field in templates for acceptance, '
            'rejection and activation email with "{{ message }}". It is displayed '
            'in rejection email as "Rejection reasons" in default templates.'
        )
    )

    class Meta:
        model = RegistrationRequest
        excludes = ('user', '_status')

    def __init__(self, *args, **kwargs):
        super(RegistrationRequestAdminForm, self).__init__(*args, **kwargs)
        # dynamically set choices of _status field
        if self.instance._status == 'untreated':
            self.fields['action'].choices = self.UNTREATED_ACTIONS
        elif self.instance._status == 'accepted':
            self.fields['action'].choices = self.ACCEPTED_ACTIONS
        else:  # self.instance._status == 'rejected':
            self.fields['action'].choices = self.REJECTED_ACTIONS

    def clean_action(self):
        """clean action value

        Insted of raising AttributeError, validate the current registration
        request status and the requested action and then raise ValidationError

        """
        action = self.cleaned_data['action']
        if action == 'accept':
            if self.instance._status == 'accepted':
                raise ValidationError(_("You cannot accept a registration which has already been accepted."))
        elif action == 'reject':
            if self.instance._status == 'accepted':
                raise ValidationError(_("You cannot reject a registration which has already been accepted."))
        elif action == 'activate':
            if self.instance._status != 'accepted':
                raise ValidationError(_("You cannot activate an user without accepting his registration"
                                        " request."))
        elif action != 'force_activate':
            # while using django admin page, the code below should never be called.
            raise ValidationError("Unknown action '%s' was requested." % action)
        return self.cleaned_data['action']

    def save(self, commit=True):
        """Call appropriate action via current registration backend

        Insted of modifing the registration profile, this method call current
        registration backend's accept/reject/activate method as requested.

        """

        opts = self.instance._meta
        if self.errors:
            failed_action = 'updated' if self.instance.pk else 'created'
            raise ValueError("The %s could not be %s because the data didn't"
                             " validate." % (opts.object_name, failed_action))
        action = self.cleaned_data['action']
        message = self.cleaned_data['message']

        if action == 'accept':
            backend.accept(self.instance, self.request, message=message)
        elif action == 'reject':
            backend.reject(self.instance, self.request, message=message)
        elif action in ('activate', 'force_activate'):
            if action == 'force_activate':
                backend.accept(self.instance, self.request)
            # DO NOT delete request otherwise Django Admin will raise IndexError
            backend.activate(
                self.instance.activation_key,
                self.request,
                message=message,
                delete_registration_request=False,
            )
        else:
            raise AttributeError('Unknwon action "%s" was requested.' % action)

        if action not in ('activate', 'force_activate'):
            new_instance = self.instance.__class__.objects.get(pk=self.instance.pk)
        else:
            new_instance = self.instance
            # the instance has been deleted by activate method however
            # ``save()`` method will be called, thus set mock save method
            new_instance.save = lambda *args, **kwargs: new_instance
        return new_instance

    # this form doesn't have ``save_m2m()`` method and it is required
    # in default ModelAdmin class to use. thus set mock save_m2m method
    def save_m2m(self):
        pass
