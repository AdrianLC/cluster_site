"""
"""

import random
import hashlib

from django.contrib.sites.models import Site
from django.contrib.sites.models import RequestSite


def get_site(request):
    """get current ``django.contrib.Site`` instance

    return ``django.contrib.RequestSite`` instance when the ``Site`` is
    not installed.

    """
    if Site._meta.installed:
        return Site.objects.get_current()
    else:
        return RequestSite(request)


def generate_activation_key(username):
    """generate activation key with username

    originally written by ubernostrum in django-registration_

    .. _django-registration: https://bitbucket.org/ubernostrum/django-registration
    """
    if isinstance(username, unicode):
        username = username.encode('utf-8')
    salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
    activation_key = hashlib.sha1(salt+username).hexdigest()
    return activation_key


def generate_random_password(length=10):
    """generate random password with passed length"""
    # Without 1, l, O, 0 because those character are hard to tell
    # the difference between in same fonts
    chars = '23456789abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'
    password = "".join([random.choice(chars) for i in xrange(length)])
    return password
