"""Form unit tests."""

import datetime

from django.conf import settings
from django.core import mail
from django.contrib.auth import get_user_model
User = get_user_model()
from django.test import TestCase
from django.test.client import RequestFactory
from django.test.utils import override_settings


from ..models import RegistrationRequest
from ..utils import get_site


@override_settings(REGISTRATION_OPEN=True, ACCOUNT_ACTIVATION_DAYS=7)
class RegistrationRequestTestCase(TestCase):
    user_info = {
        'username': 'alice',
        'email': 'alice@example.com',
        'password': 'password'
    }

    def setUp(self):
        self.mock_site = get_site(RequestFactory().get(''))

    def create_inactive_user(self):
        new_user = User.objects.create_user(**self.user_info)
        new_user.set_unusable_password()
        new_user.is_active = False
        return new_user

    def test_request_creation(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)

        self.assertEqual(RegistrationRequest.objects.count(), 1)
        self.assertEqual(request.user.id, new_user.id)
        self.assertEqual(request.status, 'untreated')
        self.assertFalse(request.activation_key)
        self.assertEqual(unicode(request), "Registration information for alice")

    def test_request_status_modification(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)

        request.status = 'accepted'

        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)
        self.assertFalse(request.activation_key_expired())

        request.status = 'rejected'

        self.assertEqual(request.status, 'rejected')
        self.assertFalse(request.activation_key)
        self.assertFalse(request.activation_key_expired())

        request.status = 'accepted'

        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)
        self.assertFalse(request.activation_key_expired())

        request.status = 'untreated'

        self.assertEqual(request.status, 'untreated')
        self.assertFalse(request.activation_key)
        self.assertFalse(request.activation_key_expired())

        new_user.date_joined -= datetime.timedelta(settings.ACCOUNT_ACTIVATION_DAYS+1)
        request.status = 'untreated'

        self.assertEqual(request.status, 'untreated')
        self.assertFalse(request.activation_key_expired())

        request.status = 'rejected'

        self.assertEqual(request.status, 'rejected')
        self.assertFalse(request.activation_key_expired())

        request.status = 'accepted'
        # status = accepted change date_joined
        new_user.date_joined -= datetime.timedelta(settings.ACCOUNT_ACTIVATION_DAYS+1)

        self.assertEqual(request.status, 'expired')
        self.assertTrue(request.activation_key_expired())

    def test_status_display(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)

        # new request should start as untreated
        self.assertEqual(request.status, 'untreated')
        self.assertEqual(request.get_status_display(), 'Untreated yet')

        request.status = 'rejected'

        self.assertEqual(request.status, 'rejected')
        self.assertEqual(request.get_status_display(), 'Registration has been rejected')

        request.status = 'accepted'

        self.assertEqual(request.status, 'accepted')
        self.assertEqual(request.get_status_display(), 'Registration has been accepted')

        new_user.date_joined -= datetime.timedelta(settings.ACCOUNT_ACTIVATION_DAYS+1)

        self.assertEqual(request.status, 'expired')
        self.assertEqual(request.get_status_display(), 'Activation key has expired')

    def test_send_registration_email(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)
        request.send_registration_email(site=self.mock_site)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user_info['email']])

    def test_send_activation_email(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)
        request.send_activation_email(site=self.mock_site)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user_info['email']])

    def test_send_acceptance_email(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)
        request.status = 'accepted'
        request.save()
        request.send_acceptance_email(site=self.mock_site)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user_info['email']])

    def test_send_rejection_email(self):
        new_user = self.create_inactive_user()
        request = RegistrationRequest.objects.create(user=new_user)
        request.status = 'rejected'
        request.save()
        request.send_rejection_email(site=self.mock_site)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.user_info['email']])


@override_settings(REGISTRATION_OPEN=True, ACCOUNT_ACTIVATION_DAYS=7)
class RegistrationRequestManagerTestCase(TestCase):
    user_info = {
        'username': 'alice',
        'email': 'alice@example.com',
    }

    def setUp(self):
        self.mock_site = get_site(RequestFactory().get(''))

    def test_register(self):
        new_user = RegistrationRequest.objects.register(site=self.mock_site, **self.user_info)

        self.assertEqual(new_user.username, self.user_info['username'])
        self.assertEqual(new_user.email, self.user_info['email'])
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())

    def test_register_sending_email(self):
        RegistrationRequest.objects.register(site=self.mock_site, **self.user_info)

        self.assertEqual(len(mail.outbox), 1)

    def test_register_without_sending_email(self):
        RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )

        self.assertEqual(len(mail.outbox), 0)

    def test_acceptance(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )

        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(request, site=self.mock_site)

        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)

    def test_acceptance_sending_email(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )

        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(request, site=self.mock_site)

        self.assertEqual(len(mail.outbox), 1)

    def test_acceptance_without_sending_email(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(
            request,
            site=self.mock_site,
            send_email=False
        )

        self.assertEqual(len(mail.outbox), 0)

    def test_rejection(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.reject_registration(request, site=self.mock_site)

        self.assertEqual(request.status, 'rejected')
        self.assertFalse(request.activation_key)

    def test_rejection_sending_email(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.reject_registration(request, site=self.mock_site)

        self.assertEqual(len(mail.outbox), 1)

    def test_rejection_without_sending_email(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.reject_registration(
            request,
            site=self.mock_site,
            send_email=False
        )

        self.assertEqual(len(mail.outbox), 0)

    def test_acceptance_after_rejection_success(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        # reject
        result = RegistrationRequest.objects.reject_registration(request, site=self.mock_site)

        self.assertTrue(result)
        self.assertEqual(request.status, 'rejected')
        self.assertFalse(request.activation_key)

        # accept should work even after rejection
        result = RegistrationRequest.objects.accept_registration(request, site=self.mock_site)

        self.assertTrue(result)
        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)

    def test_acceptance_after_acceptance_makes_no_changes(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request

        # accept
        result = RegistrationRequest.objects.accept_registration(request, site=self.mock_site)

        self.assertTrue(result)
        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)

        # accept should not return a newly accepted request
        result = RegistrationRequest.objects.accept_registration(request, site=self.mock_site)

        self.assertIsNone(result)

    def test_rejection_after_acceptance_fail(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        result = RegistrationRequest.objects.accept_registration(request, site=self.mock_site)

        self.assertTrue(result)
        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)

        # reject should not work
        result = RegistrationRequest.objects.reject_registration(request, site=self.mock_site)

        self.assertFalse(result)
        self.assertEqual(request.status, 'accepted')
        self.assertTrue(request.activation_key)

    def test_rejection_after_rejection_fail(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        # accept
        result = RegistrationRequest.objects.reject_registration(request, site=self.mock_site)

        self.assertTrue(result)
        self.assertEqual(request.status, 'rejected')
        self.assertFalse(request.activation_key)

        # reject should not work
        result = RegistrationRequest.objects.reject_registration(request, site=self.mock_site)

        self.assertFalse(result)
        self.assertEqual(request.status, 'rejected')
        self.assertFalse(request.activation_key)

    def test_activation(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site, send_email=False, **self.user_info)
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(
            request,
            site=self.mock_site,
            send_email=False
        )
        activated = RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='swordfish',
            send_email=False
        )

        self.assertTrue(activated)

        activated_user, password, is_generated = activated

        self.assertEqual(new_user, activated_user)
        self.assertEqual(password, 'swordfish')
        self.assertEqual(is_generated, False)
        # the user should be activated with the password
        self.assertTrue(activated_user.is_active)
        self.assertTrue(activated_user.has_usable_password())
        self.assertTrue(activated_user.check_password(password))
        # inspection request should be deleted
        self.assertFalse(RegistrationRequest.objects.filter(pk=request.pk).exists())

    def test_activation_sending_email(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(
            request,
            site=self.mock_site,
            send_email=False
        )
        RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='some_password'
        )

        self.assertEqual(len(mail.outbox), 1)

    def test_activation_without_sending_email(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(
            request,
            site=self.mock_site,
            send_email=False
        )
        RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='some_password',
            send_email=False
        )

        self.assertEqual(len(mail.outbox), 0)

    def test_activation_with_untreated_fail(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        result = RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='swordfish'
        )

        self.assertFalse(result)
        # the user should not be activated
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())
        self.assertFalse(new_user.check_password('swordfish'))
        # inspection request should not be deleted
        self.assertTrue(RegistrationRequest.objects.filter(pk=request.pk).exists())

    def test_activation_with_rejected_fail(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.reject_registration(request, site=self.mock_site)
        result = RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='swordfish'
        )

        self.assertFalse(result)
        # the user should not be activated
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())
        self.assertFalse(new_user.check_password('swordfish'))
        # inspection request should not be deleted
        self.assertTrue(RegistrationRequest.objects.filter(pk=request.pk).exists())

    def test_activation_with_expired_fail(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(request, site=self.mock_site)
        new_user.date_joined -= datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS + 1)
        new_user.save()
        result = RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='swordfish'
        )

        self.assertFalse(result)
        # the user should not be activated
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())
        self.assertFalse(new_user.check_password('swordfish'))
        # inspection request should not be deleted
        self.assertTrue(RegistrationRequest.objects.filter(pk=request.pk).exists())

    def test_activation_with_invalid_key_fail(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(request, site=self.mock_site)
        result = RegistrationRequest.objects.activate_user(
            activation_key='foo',
            site=self.mock_site,
            password='swordfish'
        )

        self.assertFalse(result)
        # the user should not be activated
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())
        self.assertFalse(new_user.check_password('swordfish'))
        # inspection request should not be deleted
        self.assertTrue(RegistrationRequest.objects.filter(pk=request.pk).exists())

    def test_activation_without_request_deletion(self):
        new_user = RegistrationRequest.objects.register(
            site=self.mock_site,
            send_email=False,
            **self.user_info
        )
        request = new_user.registration_request
        RegistrationRequest.objects.accept_registration(request, site=self.mock_site)
        RegistrationRequest.objects.activate_user(
            activation_key=request.activation_key,
            site=self.mock_site,
            password='swordfish',
            delete_registration_request=False
        )

        self.assertTrue(RegistrationRequest.objects.filter(pk=request.pk).exists())

    def test_expired_user_deletion(self):
        RegistrationRequest.objects.register(
            username='new untreated user',
            email='new_untreated_user@example.com',
            site=self.mock_site,
        )
        new_accepted_user = RegistrationRequest.objects.register(
            username='new accepted user',
            email='new_accepted_user@example.com',
            site=self.mock_site,
        )
        new_rejected_user = RegistrationRequest.objects.register(
            username='new rejected user',
            email='new_rejected_user@example.com',
            site=self.mock_site,
        )
        expired_untreated_user = RegistrationRequest.objects.register(
            username='expired untreated user',
            email='expired_untreated_user@example.com',
            site=self.mock_site,
        )
        expired_accepted_user = RegistrationRequest.objects.register(
            username='expired accepted user',
            email='expired_accepted_user@example.com',
            site=self.mock_site,
        )
        expired_rejected_user = RegistrationRequest.objects.register(
            username='expired rejected user',
            email='expired_rejected_user@example.com',
            site=self.mock_site,
        )

        RegistrationRequest.objects.accept_registration(
            new_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            new_rejected_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.accept_registration(
            expired_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            expired_rejected_user.registration_request,
            site=self.mock_site,
        )

        delta = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)

        expired_untreated_user.date_joined -= delta
        expired_untreated_user.save()
        expired_accepted_user.date_joined -= delta
        expired_accepted_user.save()
        expired_rejected_user.date_joined -= delta
        expired_rejected_user.save()

        RegistrationRequest.objects.delete_expired_users()

        # Only expired accepted user is deleted
        self.assertEqual(RegistrationRequest.objects.count(), 5)
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired accepted user')

    def test_rejected_user_deletion(self):
        RegistrationRequest.objects.register(
            username='new untreated user',
            email='new_untreated_user@example.com',
            site=self.mock_site,
        )
        new_accepted_user = RegistrationRequest.objects.register(
            username='new accepted user',
            email='new_accepted_user@example.com',
            site=self.mock_site,
        )
        new_rejected_user = RegistrationRequest.objects.register(
            username='new rejected user',
            email='new_rejected_user@example.com',
            site=self.mock_site,
        )
        expired_untreated_user = RegistrationRequest.objects.register(
            username='expired untreated user',
            email='expired_untreated_user@example.com',
            site=self.mock_site,
        )
        expired_accepted_user = RegistrationRequest.objects.register(
            username='expired accepted user',
            email='expired_accepted_user@example.com',
            site=self.mock_site,
        )
        expired_rejected_user = RegistrationRequest.objects.register(
            username='expired rejected user',
            email='expired_rejected_user@example.com',
            site=self.mock_site,
        )

        RegistrationRequest.objects.accept_registration(
            new_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            new_rejected_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.accept_registration(
            expired_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            expired_rejected_user.registration_request,
            site=self.mock_site,
        )

        delta = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)

        expired_untreated_user.date_joined -= delta
        expired_untreated_user.save()
        expired_accepted_user.date_joined -= delta
        expired_accepted_user.save()
        expired_rejected_user.date_joined -= delta
        expired_rejected_user.save()

        RegistrationRequest.objects.delete_rejected_users()
        # new_rejected_user and expired rejected user are deleted
        self.assertEqual(RegistrationRequest.objects.count(), 4)
        self.assertRaises(User.DoesNotExist, User.objects.get, username='new rejected user')
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired rejected user')
