"""Unit tests for the custom admin functionality."""

import datetime

from django.test import TestCase
from django.test.client import RequestFactory
from django.test.utils import override_settings
from django.contrib import admin
from django.contrib.auth import get_user_model
User = get_user_model()
from django.contrib.auth.models import Permission
from django.conf import settings
from django.core.urlresolvers import reverse
from django.core import mail

from .. import backend
from ..models import RegistrationRequest
from ..admin import RegistrationRequestAdmin
from ..admin import RegistrationRequestAdminForm


@override_settings(REGISTRATION_OPEN=True, ACCOUNT_ACTIVATION_DAYS=7)
class AdminViewsTestCase(TestCase):

    """Mostly RegistrationRequest ModelAdmin's change_view tests."""

    REGISTRATION_ADMIN_ROOT_URL = "%(admin_root)s%(module_root)s" % {
        'admin_root': reverse('admin:index'),
        'module_root': "approval_registration/registrationrequest/",
    }
    TEMPLATE_ROOT_PATH = REGISTRATION_ADMIN_ROOT_URL.lstrip('/')

    def setUp(self):
        self.mock_request = RequestFactory().get('')
        self.admin_user = User.objects.create_superuser(
            username='mark',
            email='mark@test.com',
            password='password'
        )
        self.client.login(username='mark', password='password')

    def test_change_list_view_get(self):
        url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'admin/change_list.html')

    def test_change_view_get(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')

    def test_change_view_get_404(self):
        backend.register(username='bob', email='bob@example.com', request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + "100/"
        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_change_view_post_valid_accept_from_untreated(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        redirect_url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.post(url, {'action': 'accept'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'accepted')

    def test_change_view_post_valid_inaccept_from_accepted(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.accept(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.post(url, {'action': 'accept'})

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')
        self.assertFalse(response.context['adminform'].form.is_valid())
        self.assertEqual(
            response.context['adminform'].form.errors['action'],
            [u"Select a valid choice. accept is not one of the available choices."]
        )

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'accepted')

    def test_change_view_post_valid_accept_from_rejected(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.reject(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        redirect_url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.post(url, {'action': 'accept'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'accepted')

    def test_change_view_post_valid_reject_from_untreated(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        redirect_url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.post(url, {'action': 'reject'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'rejected')

    def test_change_view_post_invalid_reject_from_accepted(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.accept(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.post(url, {'action': 'reject'})

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')
        self.assertFalse(response.context['adminform'].form.is_valid())
        self.assertEqual(
            response.context['adminform'].form.errors['action'],
            [u"Select a valid choice. reject is not one of the available choices."]
        )

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'accepted')

    def test_change_view_post_invalid_reject_from_rejected(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.reject(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.post(url, {'action': 'reject'})

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')
        self.assertFalse(response.context['adminform'].form.is_valid())
        self.assertEqual(
            response.context['adminform'].form.errors['action'],
            [u"Select a valid choice. reject is not one of the available choices."]
        )

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'rejected')

    def test_change_view_post_invalid_activate_from_untreated(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.post(url, {'action': 'activate'})

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')
        self.assertFalse(response.context['adminform'].form.is_valid())
        self.assertEqual(
            response.context['adminform'].form.errors['action'],
            [u"Select a valid choice. activate is not one of the available choices."]
        )

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'untreated')

    def test_change_view_post_valid_activate_from_accepted(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.accept(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        redirect_url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.post(url, {'action': 'activate'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

        registration_request = RegistrationRequest.objects.filter(user__pk=new_user.pk)
        self.assertFalse(registration_request.exists())

    def test_change_view_post_invalid_activate_from_rejected(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.reject(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.post(url, {'action': 'activate'})

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')
        self.assertFalse(response.context['adminform'].form.is_valid())
        self.assertEqual(
            response.context['adminform'].form.errors['action'],
            [u"Select a valid choice. activate is not one of the available choices."]
        )

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'rejected')

    def test_change_view_post_valid_force_activate_from_untreated(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        redirect_url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.post(url, {'action': 'force_activate'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

        registration_request = RegistrationRequest.objects.filter(user__pk=new_user.pk)
        self.assertFalse(registration_request.exists())

    def test_change_view_post_invalid_force_activate_from_accepted(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.accept(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        response = self.client.post(url, {'action': 'force_activate'})

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, self.TEMPLATE_ROOT_PATH + 'change_form.html')
        self.assertFalse(response.context['adminform'].form.is_valid())
        self.assertEqual(
            response.context['adminform'].form.errors['action'],
            [u"Select a valid choice. force_activate is not one of the available choices."]
        )

        registration_request = RegistrationRequest.objects.get(user__pk=new_user.pk)
        self.assertEqual(registration_request.status, 'accepted')

    def test_change_view_post_valid_force_activate_from_rejected(self):
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        backend.reject(registration_request, request=self.mock_request)

        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)
        redirect_url = self.REGISTRATION_ADMIN_ROOT_URL
        response = self.client.post(url, {'action': 'force_activate'})

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, redirect_url)

        registration_request = RegistrationRequest.objects.filter(user__pk=new_user.pk)
        self.assertFalse(registration_request.exists())

    def test_change_view_post_without_permissions(self):
        unallowed_user = User.objects.create_user(
            username='pepe',
            email='pepe@test.com',
            password='password'
        )
        unallowed_user.is_staff = True
        unallowed_user.save()
        self.client.login(username='pepe', password='password')

        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        registration_request = new_user.registration_request
        url = self.REGISTRATION_ADMIN_ROOT_URL + ("%d/" % registration_request.pk)

        response = self.client.post(url, {'action': 'accept'})

        self.assertEqual(response.status_code, 403)

        permission = Permission.objects.get(codename='accept_registration')
        unallowed_user.user_permissions.add(permission)
        unallowed_user.save()
        unallowed_user = User.objects.get(pk=unallowed_user.pk)

        response = self.client.post(url, {'action': 'reject'})

        self.assertEqual(response.status_code, 403)

        permission = Permission.objects.get(codename='reject_registration')
        unallowed_user.user_permissions.add(permission)
        unallowed_user.save()
        unallowed_user = User.objects.get(pk=unallowed_user.pk)

        response = self.client.post(url, {'action': 'activate'})

        self.assertEqual(response.status_code, 403)

        permission = Permission.objects.get(codename='accept_registration')
        unallowed_user.user_permissions.remove(permission)
        unallowed_user.save()
        unallowed_user = User.objects.get(pk=unallowed_user.pk)

        response = self.client.post(url, {'action': 'force_activate'})

        self.assertEqual(response.status_code, 403)


@override_settings(REGISTRATION_OPEN=True, ACCOUNT_ACTIVATION_DAYS=7)
class AdminActionsTestCase(TestCase):

    """Tests for the admin action functions and get_actions(). """

    def setUp(self):
        self.mock_request = RequestFactory().get('')
        self.admin_user = User.objects.create_superuser(
            username='mark',
            email='mark@test.com',
            password='password'
        )
        self.client.login(username='mark', password='password')

    def test_get_actions(self):
        admin_class = RegistrationRequestAdmin(RegistrationRequest, admin.site)
        partial_admin = User.objects.create_user(
            username='pepe',
            email='pepe@test.com',
            password='password'
        )
        partial_admin.is_active = True
        partial_admin.save()
        self.mock_request.user = partial_admin

        actions = admin_class.get_actions(self.mock_request)

        self.assertFalse("accept_users" in actions)
        self.assertFalse("reject_users" in actions)
        self.assertFalse("force_activate_users" in actions)

        for permission_name in ("accept_registration", "reject_registration", "activate_user"):
            permission = Permission.objects.get(codename=permission_name)
            partial_admin.user_permissions.add(permission)
        partial_admin.save()
        partial_admin = User.objects.get(username='pepe')
        self.mock_request.user = partial_admin

        actions = admin_class.get_actions(self.mock_request)

        self.assertTrue("accept_users" in actions)
        self.assertTrue("reject_users" in actions)
        self.assertTrue("force_activate_users" in actions)

    def test_resend_acceptance_email_action(self):
        admin_class = RegistrationRequestAdmin(RegistrationRequest, admin.site)

        new_user = backend.register(
            username='bob',
            email='bob@example.com',
            request=self.mock_request
        )
        backend.accept(new_user.registration_request, request=self.mock_request)

        admin_class.resend_acceptance_email(None, RegistrationRequest.objects.all())

        # one for registration, one for acceptance, one for resend
        self.assertEqual(len(mail.outbox), 3)

    def test_resend_acceptance_email_action_expired(self):
        admin_class = RegistrationRequestAdmin(RegistrationRequest, admin.site)

        expired_user = backend.register(
            username='bob',
            email='bob@example.com',
            request=self.mock_request
        )
        backend.accept(expired_user.registration_request, request=self.mock_request)
        expired_user.date_joined -= datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)
        expired_user.save()
        admin_class.resend_acceptance_email(None, RegistrationRequest.objects.all())

        # only registration and acceptance
        self.assertEqual(len(mail.outbox), 2)

    def test_accept_users_action(self):
        admin_class = RegistrationRequestAdmin(RegistrationRequest, admin.site)

        backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        admin_class.accept_users(None, RegistrationRequest.objects.all())

        for registration_request in RegistrationRequest.objects.all():
            self.assertEqual(registration_request.status, 'accepted')
            self.assertTrue(registration_request.activation_key)

    def test_reject_users_action(self):
        admin_class = RegistrationRequestAdmin(RegistrationRequest, admin.site)

        backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        admin_class.reject_users(None, RegistrationRequest.objects.all())

        for registration_request in RegistrationRequest.objects.all():
            self.assertEqual(registration_request.status, 'rejected')
            self.assertFalse(registration_request.activation_key)

    def test_force_activate_users_action(self):
        admin_class = RegistrationRequestAdmin(RegistrationRequest, admin.site)

        backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        admin_class.force_activate_users(None, RegistrationRequest.objects.all())

        self.assertEqual(RegistrationRequest.objects.count(), 0)


class RegistrationRequestAdminFormTestCase(TestCase):

    def setUp(self):
        self.mock_request = RequestFactory().get('')
        new_user = backend.register(username='bob', email='bob@example.com', request=self.mock_request)
        self.registration_request = new_user.registration_request

    def test_action_choices_for_status_untreated(self):
        form = RegistrationRequestAdminForm(instance=self.registration_request)

        choice_names = [choice[0] for choice in form.fields['action'].choices]

        self.assertTrue("accept" in choice_names)
        self.assertTrue("reject" in choice_names)
        self.assertTrue("force_activate" in choice_names)
        self.assertEqual(len(choice_names), 3)

    def test_action_choices_for_status_accepted(self):
        backend.accept(self.registration_request, request=self.mock_request)
        form = RegistrationRequestAdminForm(instance=self.registration_request)

        choice_names = [choice[0] for choice in form.fields['action'].choices]

        self.assertTrue("activate" in choice_names)
        self.assertEqual(len(choice_names), 1)

    def test_action_choices_for_status_rejected(self):
        backend.reject(self.registration_request, request=self.mock_request)
        form = RegistrationRequestAdminForm(instance=self.registration_request)

        choice_names = [choice[0] for choice in form.fields['action'].choices]

        self.assertTrue("accept" in choice_names)
        self.assertTrue("force_activate" in choice_names)
        self.assertEqual(len(choice_names), 2)

    def test_correct_data_validation(self):
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'accept'})

        self.assertTrue(form.is_valid())

        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'reject'})

        self.assertTrue(form.is_valid())

        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'force_activate'})

        self.assertTrue(form.is_valid())

        backend.accept(self.registration_request, request=self.mock_request)
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'activate'})

        self.assertTrue(form.is_valid())

    def test_invalid_action_validation(self):
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'bullshit'})

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['action'],
            [u"Select a valid choice. bullshit is not one of the available choices."]
        )

    def test_action_validation_accept_already_accepted_request(self):
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'accept'})
        backend.accept(self.registration_request, request=self.mock_request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['action'],
            [u"You cannot accept a registration which has already been accepted."]
        )

    def test_action_validation_reject_already_accepted_request(self):
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'reject'})
        backend.accept(self.registration_request, request=self.mock_request)

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['action'],
            [u"You cannot reject a registration which has already been accepted."]
        )

    def test_action_validation_activate_unaccepted_request(self):
        backend.accept(self.registration_request, request=self.mock_request)
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'activate'})
        self.registration_request._status = 'untreated'
        self.registration_request.save()

        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['action'],
            [u"You cannot activate an user without accepting his registration request."]
        )

    def test_save_unvalid_form(self):
        form = RegistrationRequestAdminForm(instance=self.registration_request,
                                            data={'action': 'bullshit'})

        self.assertRaisesRegexp(
            ValueError,
            "The RegistrationRequest could not be updated because the data didn't validate.",
            form.save
        )
