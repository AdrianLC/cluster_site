"""Backend unit tests."""

import datetime

from django.test import TestCase
from django.test.client import RequestFactory
from django.test.utils import override_settings
from django.conf import settings
from django.core import mail
from django.contrib.auth import get_user_model
User = get_user_model()
from django.contrib.auth.hashers import check_password

from .. import signals
from .. import backend
from ..models import RegistrationRequest


@override_settings(REGISTRATION_OPEN=True, ACCOUNT_ACTIVATION_DAYS=7)
class BackendBasicFunctionalityTestCase(TestCase):

    """
    Tests correct operation of the basic functionalities:
    registration, acceptance, rejection and activation.
    """

    def setUp(self):
        self.mock_request = RequestFactory().get('')

    def test_registration(self):
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        self.assertEqual(new_user.username, 'bob')
        self.assertEqual(new_user.email, 'bobe@example.com')

        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())

        # A request for approval suld have been created and a registration email sent.
        self.assertEqual(RegistrationRequest.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

    def test_acceptance(self):
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        registration_request = new_user.registration_request
        accepted_user = backend.accept(registration_request, request=self.mock_request)

        self.assertTrue(accepted_user)
        self.assertEqual(registration_request, accepted_user.registration_request)
        self.assertEqual(registration_request.status, 'accepted')
        self.assertTrue(registration_request.activation_key)

    def test_rejection(self):
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        registration_request = new_user.registration_request
        rejected_user = backend.reject(registration_request, request=self.mock_request)

        self.assertTrue(rejected_user)
        self.assertEqual(registration_request, rejected_user.registration_request)
        self.assertEqual(registration_request.status, 'rejected')
        self.assertFalse(registration_request.activation_key)

    def test_activation_with_password(self):
        """Tests normal scenario: password manually introduced."""
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        registration_request = new_user.registration_request
        backend.accept(registration_request, request=self.mock_request)
        activated_user = backend.activate(
            activation_key=registration_request.activation_key,
            request=self.mock_request,
            password='some_password')

        self.assertTrue(activated_user)
        self.assertEqual(activated_user, new_user)
        self.assertTrue(activated_user.is_active)
        self.assertTrue(activated_user.has_usable_password())
        self.assertTrue(activated_user.check_password('some_password'))

    def test_activation_without_password(self):
        """Tests automated password generation."""
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        registration_request = new_user.registration_request
        backend.accept(registration_request, request=self.mock_request)
        activated_user = backend.activate(
            activation_key=registration_request.activation_key,
            request=self.mock_request)

        self.assertTrue(activated_user)
        self.assertEqual(activated_user, new_user)
        self.assertTrue(activated_user.is_active)
        self.assertTrue(activated_user.has_usable_password())

    def test_untreated_activation(self):
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        request = new_user.registration_request
        activated_user = backend.activate(
            activation_key=request.activation_key,
            request=self.mock_request,
            password='some_password')

        self.assertFalse(activated_user)
        new_user = User.objects.get(pk=new_user.pk)
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())

    def test_rejected_activation(self):
        new_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        registration_request = new_user.registration_request
        backend.reject(registration_request, request=self.mock_request)
        activated_user = backend.activate(
            activation_key=registration_request.activation_key,
            request=self.mock_request,
            password='some_password')

        self.assertFalse(activated_user)
        new_user = User.objects.get(pk=new_user.pk)
        self.assertFalse(new_user.is_active)
        self.assertFalse(new_user.has_usable_password())

    def test_expired_request_activation(self):
        expired_user = backend.register(username='bob', email='bobe@example.com', request=self.mock_request)

        registration_request = expired_user.registration_request
        backend.accept(registration_request, request=self.mock_request)

        expired_user.date_joined -= datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)
        expired_user.save()

        activated_user = backend.activate(
            activation_key=registration_request.activation_key,
            request=self.mock_request,
            password='some_password')

        self.assertFalse(activated_user)
        expired_user = User.objects.get(pk=expired_user.pk)
        self.assertFalse(expired_user.is_active)
        self.assertFalse(expired_user.has_usable_password())


class BackendSignalEmissionTestCase(TestCase):

    """Tests the occurrence of signal emissions from the registration backend."""

    def setUp(self):
        self.mock_request = RequestFactory().get('')

    def test_registration_signal(self):
        def receiver(sender, user, request, **kwargs):
            self.assertEqual(user.username, 'bob')
            received_signals.append(kwargs.get('signal'))

        received_signals = []
        signals.user_registered.connect(receiver, sender=backend.Backend)

        backend.register(username='bob', email='bob@example.com', request=self.mock_request)

        self.assertEqual(len(received_signals), 1)
        self.assertEqual(received_signals, [signals.user_registered])

    def test_acceptance_signal(self):
        def receiver(sender, user, registration_request, request, **kwargs):
            self.assertEqual(user.username, 'bob')
            self.assertEqual(user.registration_request, registration_request)
            received_signals.append(kwargs.get('signal'))

        received_signals = []
        signals.user_accepted.connect(receiver, sender=backend.Backend)

        backend.register(username='bob', email='bobe@example.com', request=self.mock_request)
        registration_request = RegistrationRequest.objects.get(user__username='bob')
        backend.accept(registration_request, request=self.mock_request)

        self.assertEqual(len(received_signals), 1)
        self.assertEqual(received_signals, [signals.user_accepted])

    def test_no_acceptance_signal_for_previously_accepted_request(self):
        def receiver(sender, user, registration_request, request, **kwargs):
            self.assertEqual(user.username, 'bob')
            self.assertEqual(user.registration_request, registration_request)
            received_signals.append(kwargs.get('signal'))

        received_signals = []

        backend.register(username='bob', email='bobe@example.com', request=self.mock_request)
        registration_request = RegistrationRequest.objects.get(user__username='bob')
        backend.accept(registration_request, request=self.mock_request)

        signals.user_accepted.connect(receiver, sender=backend.Backend)
        # accept -> accept is not allowed thus fail
        backend.accept(registration_request, request=self.mock_request)

        self.assertEqual(len(received_signals), 0)

    def test_rejection_signal(self):
        def receiver(sender, user, registration_request, request, **kwargs):
            self.assertEqual(user.username, 'bob')
            self.assertEqual(user.registration_request, registration_request)
            received_signals.append(kwargs.get('signal'))

        received_signals = []
        signals.user_rejected.connect(receiver, sender=backend.Backend)

        backend.register(username='bob', email='bobe@example.com', request=self.mock_request)
        registration_request = RegistrationRequest.objects.get(user__username='bob')
        backend.reject(registration_request, request=self.mock_request)

        self.assertEqual(len(received_signals), 1)
        self.assertEqual(received_signals, [signals.user_rejected])

    def test_no_rejection_signal_for_previously_accepted_request(self):
        def receiver(sender, user, registration_request, request, **kwargs):
            self.assertEqual(user.username, 'bob')
            self.assertEqual(user.registration_request, registration_request)
            received_signals.append(kwargs.get('signal'))

        received_signals = []
        signals.user_rejected.connect(receiver, sender=backend.Backend)

        backend.register(username='bob', email='bobe@example.com', request=self.mock_request)
        registration_request = RegistrationRequest.objects.get(user__username='bob')
        backend.accept(registration_request, request=self.mock_request)
        # rejection is not allowed after acceptance
        backend.reject(registration_request, request=self.mock_request)

        self.assertEqual(len(received_signals), 0)

    def test_activation_signal(self):
        def receiver(sender, user, request, **kwargs):
            self.assertEqual(user.username, 'bob')
            self.assertTrue(check_password('some_password', user.password))
            received_signals.append(kwargs.get('signal'))

        received_signals = []
        signals.user_activated.connect(receiver, sender=backend.Backend)

        backend.register(username='bob', email='bobe@example.com', request=self.mock_request)
        registration_request = RegistrationRequest.objects.get(user__username='bob')
        backend.accept(registration_request, request=self.mock_request)
        backend.activate(
            activation_key=registration_request.activation_key,
            request=self.mock_request,
            password='some_password')

        self.assertEqual(len(received_signals), 1)
        self.assertEqual(received_signals, [signals.user_activated])
