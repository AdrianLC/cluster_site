"""Form unit tests."""

from django.test import TestCase
from django.contrib.auth import get_user_model
User = get_user_model()

from .. import forms


class ActivationFormTestCase(TestCase):

    """Test the registration activation form."""

    def test_activation_form(self):
        form = forms.ActivationForm(data={'password1': 'foo', 'password2': 'foo'})

        self.assertTrue(form.is_valid())

        form = forms.ActivationForm(data={'password1': 'foo'})

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['password2'], [u"This field is required."])

        form = forms.ActivationForm(data={'password1': 'foo',
                                          'password2': 'bar'})

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['__all__'], [u"The two password fields didn't match."])


class RegistrationFormTestCase(TestCase):

    """Test the different registration forms."""

    def test_registration_form(self):
        """Tests the default form with valid data as well as with different invalid data inputs."""

        form = forms.RegistrationForm(data={'username': 'foofoohogehoge',
                                            'email1': 'foo@example.com',
                                            'email2': 'foo@example.com'})

        self.assertTrue(form.is_valid())

        # Create a user so we can verify that duplicate usernames aren't permitted.
        User.objects.create_user('alice', 'alice@example.com', 'secret')

        invalid_data_set = [
            # Non-alphanumeric username.
            {'data': {'username': 'foo/bar',
                      'email1': 'foo@example.com',
                      'email2': 'foo@example.com'},
            'expected_error': (
                'username',
                [u"This value may contain only letters, numbers and @/./+/-/_ characters."]
            )},
            # Already-existing username.
            {'data': {'username': 'alice',
                      'email1': 'alice@example.com',
                      'email2': 'alice@example.com'},
            'expected_error': (
                'username',
                [u"A user with that username already exists."]
            )},
            # Mismatched email.
            {'data': {'username': 'foo',
                      'email1': 'foo@example.com',
                      'email2': 'bar@example.com'},
            'expected_error': (
                '__all__',
                [u"The two email fields didn't match."]
            )},
        ]

        for invalid_data in invalid_data_set:
            form = forms.RegistrationForm(data=invalid_data['data'])

            self.assertFalse(form.is_valid())
            self.assertEqual(form.errors[invalid_data['expected_error'][0]],
                             invalid_data['expected_error'][1])

    def test_registration_form_tos(self):
        """Tests the registration form with terms of service both agreeing and without agreeing."""

        form = forms.RegistrationFormTermsOfService(data={'username': 'foofoohogehoge',
                                                          'email1': 'foo@example.com',
                                                          'email2': 'foo@example.com',
                                                          'tos': 'on'})

        self.assertTrue(form.is_valid())

        form = forms.RegistrationFormTermsOfService(data={'username': 'foo',
                                                          'email1': 'foo@example.com',
                                                          'email2': 'foo@example.com'})

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['tos'], [u"You must agree to the terms to register."])

    def test_registration_form_unique_email(self):
        """Tests the registration form which restricts to unique emails."""

        User.objects.create_user('alice', 'alice@example.com', 'secret')

        form = forms.RegistrationFormUniqueEmail(data={'username': 'foo',
                                                       'email1': 'alice@example.com',
                                                       'email2': 'alice@example.com'})

        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['email1'],
                         [u"This email address is already in use. Please supply a different email address."])

        form = forms.RegistrationFormUniqueEmail(data={'username': 'foofoohogehoge',
                                                       'email1': 'foo@example.com',
                                                       'email2': 'foo@example.com'})

        self.assertTrue(form.is_valid())

    def test_registration_form_no_free_email(self):
        """Tests the registration form that doesn't allow free email domains."""

        base_data = {'username': 'foofoohogehoge'}
        base_data['email1'] = 'foo@example.com'
        base_data['email2'] = base_data['email1']
        form = forms.RegistrationFormNoFreeEmail(data=base_data)

        self.assertTrue(form.is_valid())

        for disallowed_domain in forms.RegistrationFormNoFreeEmail.bad_domains:
            invalid_data = base_data
            invalid_data['email1'] = u"foo@%s" % disallowed_domain
            invalid_data['email2'] = invalid_data['email1']
            form = forms.RegistrationFormNoFreeEmail(data=invalid_data)

            self.assertFalse(form.is_valid())
            self.assertEqual(form.errors['email1'],
                             [u"Free email addresses are forbidden. Please supply a different email address."])
