"""Management commands unit tests."""

import datetime

from django.conf import settings
from django.core import management
from django.contrib.auth import get_user_model
User = get_user_model()
from django.test import TestCase
from django.test.client import RequestFactory
from django.test.utils import override_settings


from ..models import RegistrationRequest
from ..utils import get_site


@override_settings(REGISTRATION_OPEN=True, ACCOUNT_ACTIVATION_DAYS=7)
class RegistrationManagementCommandsTestCase(TestCase):

    def setUp(self):
        self.mock_site = get_site(RequestFactory().get(''))

    def test_management_command_cleanup_expired_registrations(self):
        RegistrationRequest.objects.register(
            username='new untreated user',
            email='new_untreated_user@example.com',
            site=self.mock_site,
        )
        new_accepted_user = RegistrationRequest.objects.register(
            username='new accepted user',
            email='new_accepted_user@example.com',
            site=self.mock_site,
        )
        new_rejected_user = RegistrationRequest.objects.register(
            username='new rejected user',
            email='new_rejected_user@example.com',
            site=self.mock_site,
        )
        expired_untreated_user = RegistrationRequest.objects.register(
            username='expired untreated user',
            email='expired_untreated_user@example.com',
            site=self.mock_site,
        )
        expired_accepted_user = RegistrationRequest.objects.register(
            username='expired accepted user',
            email='expired_accepted_user@example.com',
            site=self.mock_site,
        )
        expired_rejected_user = RegistrationRequest.objects.register(
            username='expired rejected user',
            email='expired_rejected_user@example.com',
            site=self.mock_site,
        )

        RegistrationRequest.objects.accept_registration(
            new_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            new_rejected_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.accept_registration(
            expired_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            expired_rejected_user.registration_request,
            site=self.mock_site,
        )

        delta = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)

        expired_untreated_user.date_joined -= delta
        expired_untreated_user.save()
        expired_accepted_user.date_joined -= delta
        expired_accepted_user.save()
        expired_rejected_user.date_joined -= delta
        expired_rejected_user.save()

        management.call_command('cleanup_expired_registrations')
        # Only expired accepted user is deleted
        self.assertEqual(RegistrationRequest.objects.count(), 5)
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired accepted user')

    def test_management_command_cleanup_rejected_registrations(self):
        RegistrationRequest.objects.register(
            username='new untreated user',
            email='new_untreated_user@example.com',
            site=self.mock_site,
        )
        new_accepted_user = RegistrationRequest.objects.register(
            username='new accepted user',
            email='new_accepted_user@example.com',
            site=self.mock_site,
        )
        new_rejected_user = RegistrationRequest.objects.register(
            username='new rejected user',
            email='new_rejected_user@example.com',
            site=self.mock_site,
        )
        expired_untreated_user = RegistrationRequest.objects.register(
            username='expired untreated user',
            email='expired_untreated_user@example.com',
            site=self.mock_site,
        )
        expired_accepted_user = RegistrationRequest.objects.register(
            username='expired accepted user',
            email='expired_accepted_user@example.com',
            site=self.mock_site,
        )
        expired_rejected_user = RegistrationRequest.objects.register(
            username='expired rejected user',
            email='expired_rejected_user@example.com',
            site=self.mock_site,
        )

        RegistrationRequest.objects.accept_registration(
            new_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            new_rejected_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.accept_registration(
            expired_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            expired_rejected_user.registration_request,
            site=self.mock_site,
        )

        delta = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)

        expired_untreated_user.date_joined -= delta
        expired_untreated_user.save()
        expired_accepted_user.date_joined -= delta
        expired_accepted_user.save()
        expired_rejected_user.date_joined -= delta
        expired_rejected_user.save()

        management.call_command('cleanup_rejected_registrations')
        # new_rejected_user and expired rejected user are deleted
        self.assertEqual(RegistrationRequest.objects.count(), 4)
        self.assertRaises(User.DoesNotExist, User.objects.get, username='new rejected user')
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired rejected user')

    def test_management_command_cleanup_registrations(self):
        RegistrationRequest.objects.register(
            username='new untreated user',
            email='new_untreated_user@example.com',
            site=self.mock_site,
        )
        new_accepted_user = RegistrationRequest.objects.register(
            username='new accepted user',
            email='new_accepted_user@example.com',
            site=self.mock_site,
        )
        new_rejected_user = RegistrationRequest.objects.register(
            username='new rejected user',
            email='new_rejected_user@example.com',
            site=self.mock_site,
        )
        expired_untreated_user = RegistrationRequest.objects.register(
            username='expired untreated user',
            email='expired_untreated_user@example.com',
            site=self.mock_site,
        )
        expired_accepted_user = RegistrationRequest.objects.register(
            username='expired accepted user',
            email='expired_accepted_user@example.com',
            site=self.mock_site,
        )
        expired_rejected_user = RegistrationRequest.objects.register(
            username='expired rejected user',
            email='expired_rejected_user@example.com',
            site=self.mock_site,
        )

        RegistrationRequest.objects.accept_registration(
            new_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            new_rejected_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.accept_registration(
            expired_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            expired_rejected_user.registration_request,
            site=self.mock_site,
        )

        delta = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)

        expired_untreated_user.date_joined -= delta
        expired_untreated_user.save()
        expired_accepted_user.date_joined -= delta
        expired_accepted_user.save()
        expired_rejected_user.date_joined -= delta
        expired_rejected_user.save()

        management.call_command('cleanup_registrations')
        # new_rejected_user, expired rejected_user and expired_accepted_user
        # are deleted
        self.assertEqual(RegistrationRequest.objects.count(), 3)
        self.assertRaises(User.DoesNotExist, User.objects.get, username='new rejected user')
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired rejected user')
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired accepted user')

    def test_management_command_cleanupregistration(self):
        RegistrationRequest.objects.register(
            username='new untreated user',
            email='new_untreated_user@example.com',
            site=self.mock_site,
        )
        new_accepted_user = RegistrationRequest.objects.register(
            username='new accepted user',
            email='new_accepted_user@example.com',
            site=self.mock_site,
        )
        new_rejected_user = RegistrationRequest.objects.register(
            username='new rejected user',
            email='new_rejected_user@example.com',
            site=self.mock_site,
        )
        expired_untreated_user = RegistrationRequest.objects.register(
            username='expired untreated user',
            email='expired_untreated_user@example.com',
            site=self.mock_site,
        )
        expired_accepted_user = RegistrationRequest.objects.register(
            username='expired accepted user',
            email='expired_accepted_user@example.com',
            site=self.mock_site,
        )
        expired_rejected_user = RegistrationRequest.objects.register(
            username='expired rejected user',
            email='expired_rejected_user@example.com',
            site=self.mock_site,
        )

        RegistrationRequest.objects.accept_registration(
            new_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            new_rejected_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.accept_registration(
            expired_accepted_user.registration_request,
            site=self.mock_site,
        )
        RegistrationRequest.objects.reject_registration(
            expired_rejected_user.registration_request,
            site=self.mock_site,
        )

        delta = datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)

        expired_untreated_user.date_joined -= delta
        expired_untreated_user.save()
        expired_accepted_user.date_joined -= delta
        expired_accepted_user.save()
        expired_rejected_user.date_joined -= delta
        expired_rejected_user.save()

        # django-registration compatibility
        management.call_command('cleanupregistration')
        # new_rejected_user, expired rejected_user and expired_accepted_user
        # are deleted
        self.assertEqual(RegistrationRequest.objects.count(), 3)
        self.assertRaises(User.DoesNotExist, User.objects.get, username='new rejected user')
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired rejected user')
        self.assertRaises(User.DoesNotExist, User.objects.get, username='expired accepted user')
