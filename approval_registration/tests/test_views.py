"""View unit tests."""

import datetime

from django.test import TestCase
from django.test.client import RequestFactory
from django.test.utils import override_settings
from django.conf import settings
from django.contrib.auth import get_user_model
User = get_user_model()
from django.core import mail
from django.core.urlresolvers import reverse

from .. import forms
from .. import backend
from ..models import RegistrationRequest


@override_settings(REGISTRATION_OPEN=True)
class RegistrationViewsTestCase(TestCase):
    """Registration views tests."""

    def test_registration_view_get(self):
        """
        A ``GET`` to the ``register`` view uses the appropriate
        template and populates the registration form into the context.
        """
        response = self.client.get(reverse('registration_register'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/registration_form.html')
        self.assertTrue(isinstance(response.context['form'], forms.RegistrationForm))

    def test_registration_view_post_success(self):
        """
        A ``POST`` to the ``register`` view with valid data properly
        creates a new user and issues a redirect.
        """
        response = self.client.post(reverse('registration_register'),
                                    data={'username': 'alice',
                                          'email1': 'alice@example.com',
                                          'email2': 'alice@example.com'})

        self.assertRedirects(response, reverse('registration_complete'))
        self.assertEqual(RegistrationRequest.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

    def test_registration_view_post_failure(self):
        """
        A ``POST`` to the ``register`` view with invalid data does not
        create a user, and displays appropriate error messages.
        """
        response = self.client.post(reverse('registration_register'),
                                    data={'username': 'bob',
                                          'email1': 'bobe@example.com',
                                          'email2': 'mark@example.com'})

        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['form'].is_valid())
        self.assertFormError(response, 'form', field=None,
                             errors=u"The two email fields didn't match.")
        self.assertEqual(len(mail.outbox), 0)

    @override_settings(REGISTRATION_OPEN=False)
    def test_registration_view_closed(self):
        """
        Any attempt to access the ``register`` view when registration
        is closed fails and redirects.
        """

        closed_redirect = reverse('registration_disallowed')

        response = self.client.get(reverse('registration_register'))

        self.assertRedirects(response, closed_redirect)

        # Even if valid data is posted, it still shouldn't work.
        response = self.client.post(reverse('registration_register'),
                                    data={'username': 'alice',
                                          'email1': 'alice@example.com',
                                          'email2': 'alice@example.com'})

        self.assertRedirects(response, closed_redirect)
        self.assertEqual(RegistrationRequest.objects.count(), 0)


class ActivationViewsTestCase(TestCase):
    """Activation views tests."""

    def setUp(self):
        self.mock_request = RequestFactory().get('')

    def test_activation_view_get_success(self):
        """
        A ``GET`` to the ``ActivationView`` view with valid activation_key.
        """
        new_user = backend.register(username='alice', email='alice@example.com', request=self.mock_request)
        new_user = backend.accept(new_user.registration_request, request=self.mock_request)

        activation_url = reverse('registration_activate', kwargs={
            'activation_key': new_user.registration_request.activation_key})

        response = self.client.get(activation_url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/activation_form.html')
        self.assertTrue(isinstance(response.context['form'], forms.ActivationForm))

    def test_activation_view_get_fail(self):
        """
        A ``GET`` to the ``ActivationView`` view whit invalid activation_key
        raises Http404.
        """
        activation_url = reverse('registration_activate', kwargs={
            'activation_key': 'invalidactivationkey'})

        response = self.client.get(activation_url)

        self.assertEqual(response.status_code, 404)

    def test_activation_view_post_success(self):
        """
        A ``POST`` to the ``ActivationView`` view with valid data properly
        handles a valid activation.
        """
        new_user = backend.register(username='alice', email='alice@example.com', request=self.mock_request)
        new_user = backend.accept(new_user.registration_request, request=self.mock_request)

        activation_url = reverse('registration_activate', kwargs={
            'activation_key': new_user.registration_request.activation_key})

        response = self.client.post(activation_url, {
            'password1': 'swordfish',
            'password2': 'swordfish'})

        self.assertRedirects(response, reverse('registration_activation_complete'))
        # RegistrationRequest should be removed with activation:
        self.assertEqual(RegistrationRequest.objects.count(), 0)
        self.assertEqual(len(mail.outbox), 3)  # <- registration, acceptance and activation
        self.assertTrue(User.objects.get(username='alice').is_active)

    def test_activation_view_post_failure(self):
        """
        A ``POST`` to the ``ActivationView`` view with invalid data does not
        activate a user, and raise Http404.
        """
        expired_user = backend.register(username='alice', email='alice@example.com', request=self.mock_request)
        expired_user = backend.accept(expired_user.registration_request, request=self.mock_request)
        expired_user.date_joined -= datetime.timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS+1)
        expired_user.save()

        activation_url = reverse('registration_activate', kwargs={
            'activation_key': expired_user.registration_request.activation_key})

        response = self.client.post(activation_url, {
            'password1': 'swordfish',
            'password2': 'swordfish'})

        self.assertEqual(response.status_code, 404)
