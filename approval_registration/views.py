"""
"""

from django.http import Http404
from django.shortcuts import redirect
from django.views.generic import TemplateView, FormView
from django.views.generic.detail import SingleObjectMixin
from django.utils.text import ugettext_lazy as _
from django.core.urlresolvers import reverse_lazy as reverse
from django.conf import settings

from models import RegistrationRequest
from forms import RegistrationForm, ActivationForm
import backend


class _RequestPassingFormView(FormView):

    """
    A version of FormView which passes extra arguments to certain
    methods, notably passing the HTTP request nearly everywhere, to
    enable finer-grained processing.

    """
    def get(self, request, *args, **kwargs):
        # Pass request to get_form_class and get_form for per-request
        # form control.
        form_class = self.get_form_class(request)
        form = self.get_form(form_class)
        return self.render_to_response(self.get_context_data(form=form))

    def post(self, request, *args, **kwargs):
        # Pass request to get_form_class and get_form for per-request
        # form control.
        form_class = self.get_form_class(request)
        form = self.get_form(form_class)
        if form.is_valid():
            # Pass request to form_valid.
            return self.form_valid(request, form)
        else:
            return self.form_invalid(form)

    def get_form_class(self, request=None):
        return super(_RequestPassingFormView, self).get_form_class()

    def get_form_kwargs(self, request=None, form_class=None):
        return super(_RequestPassingFormView, self).get_form_kwargs()

    def get_initial(self, request=None):
        return super(_RequestPassingFormView, self).get_initial()

    def get_success_url(self, request=None, user=None):
        # We need to be able to use the request and the new user when
        # constructing success_url.
        return super(_RequestPassingFormView, self).get_success_url()

    def form_valid(self, form, request=None):
        return super(_RequestPassingFormView, self).form_valid(form)

    def form_invalid(self, form, request=None):
        return super(_RequestPassingFormView, self).form_invalid(form)


class RegistrationView(_RequestPassingFormView):

    """
    Base class for user registration views.

    """
    form_class = RegistrationForm
    http_method_names = ['get', 'post', 'head', 'options', 'trace']
    disallowed_url = 'registration_disallowed'
    success_url = None
    template_name = 'registration/registration_form.html'

    def dispatch(self, request, *args, **kwargs):
        """
        Check that user signup is allowed before even bothering to
        dispatch or do other processing.

        """
        if not self.registration_allowed(request):
            return redirect(self.disallowed_url)
        return super(RegistrationView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, request, form):
        new_user = self.register(request, **form.cleaned_data)
        success_url = self.get_success_url(request, new_user)

        # success_url may be a simple string, or a tuple providing the
        # full argument set for redirect(). Attempting to unpack it
        # tells us which one it is.
        try:
            to, args, kwargs = success_url
            return redirect(to, *args, **kwargs)
        except ValueError:
            return redirect(success_url)

    def registration_allowed(self, request):
        """
        Override this to enable/disable user registration, either
        globally or on a per-request basis.

        """
        return True

    def register(self, request, **cleaned_data):
        """
        Implement user-registration logic here. Access to both the
        request and the full cleaned_data of the registration form is
        available here.

        """
        raise NotImplementedError


class RegistrationRequestView(RegistrationView):

    """A complex view for registration

    GET:
        Display an RegistrationForm which has ``username``, ``email1`` and ``email2``
        for registration.
        ``email1`` and ``email2`` should be equal to prepend typo.

        ``form`` is in context to display these form.

    POST:
        Register the user with passed ``username`` and ``email1``
    """

    form_class = RegistrationForm

    template_name = r'registration/registration_form.html'

    success_url = reverse('registration_complete')

    def register(self, request, **cleaned_data):
        """register new user with ``username`` and ``email``

        Given a username, email address, register a new user account, which will
        initially be inactive and has unusable password.

        Along with the new ``User`` object, a new
        ``registration.models.RegistrationProfile`` will be created, tied to that
        ``User``, containing the inspection status and activation key which will
        be used for this account (activation key is not generated untill its
        inspection status is set to ``accepted``)

        An email will be sent to the supplied email address; The email will be
        rendered using two templates. See the documentation for
        ``RegistrationProfile.send_registration_email()`` for information about
        these templates and the contexts provided to them.

        After the ``User`` and ``RegistrationProfile`` are created and the
        registration email is sent, the signal
        ``registration.signals.user_registered`` will be sent, with the new
        ``User`` as the keyword argument ``user``, the ``RegistrationProfile``
        of the new ``User`` as the keyword argument ``profile`` and the class
        of this backend as the sender.

        """

        username, email = cleaned_data['username'], cleaned_data['email1'],
        request_justification = cleaned_data['justification']

        return backend.register(username, email, request, request_justification=request_justification)

    def registration_allowed(self, request):
        """
        Indicate whether account registration is currently permitted, based on
        the value of the setting ``REGISTRATION_OEPN``. This is determined as
        follows:

        *   If ``REGISTRATION_OPEN`` is not specified in settings, or is set
            to ``True``, registration is permitted.

        *   If ``REGISTRATION_OPEN`` is both specified and set to ``False``,
            registration is not permitted.

        """
        return getattr(settings, 'REGISTRATION_OPEN', True)


class RegistrationCompleteView(TemplateView):

    """A simple template view for registration complete"""

    template_name = r'registration/registration_complete.html'


class RegistrationClosedView(TemplateView):

    """A simple template view for registraion closed

    This view is called when user accessed to RegistrationView
    with REGISTRATION_OPEN = False
    """

    template_name = r'registration/registration_closed.html'


class BaseActivationView(_RequestPassingFormView):

    """
    Base class for user activation views.

    """

    model = RegistrationRequest

    form_class = ActivationForm

    http_method_names = ['get', 'post', 'head', 'options', 'trace']

    success_url = None

    template_name = r'registration/activation_form.html'

    success_url = reverse('registration_activation_complete')

    def form_valid(self, request, form):
        new_user = self.activate(request, **form.cleaned_data)
        success_url = self.get_success_url(request, new_user)

        # success_url may be a simple string, or a tuple providing the
        # full argument set for redirect(). Attempting to unpack it
        # tells us which one it is.
        try:
            to, args, kwargs = success_url
            return redirect(to, *args, **kwargs)
        except ValueError:
            return redirect(success_url)

    def activate(self, request, **cleaned_data):
        """
        Implement user-activation logic here. Access to both the
        request and the full cleaned_data of the registration form is
        available here.

        """
        raise NotImplementedError


class ActivationView(BaseActivationView, SingleObjectMixin):

    def get_queryset(self):
        """get ``RegistrationRequest`` queryset which status is 'accepted'"""
        return self.model.objects.filter(_status='accepted')

    def get_object(self, queryset=None):
        """get ``RegistrationProfile`` instance by ``activation_key``

        ``activation_key`` should be passed by URL
        """
        queryset = queryset or self.get_queryset()
        try:
            self.obj = queryset.get(activation_key=self.kwargs['activation_key'])
            if self.obj.activation_key_expired():
                raise Http404(_('Activation key has expired'))
        except self.model.DoesNotExist:
            raise Http404(_('An invalid activation key has passed'))
        return self.obj

    def get(self, request, *args, **kwargs):
        # self.object have to be set
        self.object = self.get_object()
        return super(ActivationView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # self.object have to be set
        self.object = self.get_object()
        return super(ActivationView, self).post(request, *args, **kwargs)

    def activate(self, request, **cleaned_data):
        activation_key = self.obj.activation_key
        password = cleaned_data['password1']

        return backend.activate(activation_key, request, password=password)


class ActivationCompleteView(TemplateView):

    """A simple template view for activation complete"""

    template_name = r'registration/activation_complete.html'
