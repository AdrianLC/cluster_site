"""
"""

from django.conf.urls import url, patterns

from views import (RegistrationRequestView, RegistrationClosedView, RegistrationCompleteView,
                   ActivationView, ActivationCompleteView)


urlpatterns = patterns('',
    url(r'^activate/complete/$', ActivationCompleteView.as_view(), name='registration_activation_complete'),
    url(r'^activate/(?P<activation_key>\w+)/$', ActivationView.as_view(), name='registration_activate'),
    url(r'^register/$', RegistrationRequestView.as_view(), name='registration_register'),
    url(r'^register/closed/$', RegistrationClosedView.as_view(), name='registration_disallowed'),
    url(r'^register/complete/$', RegistrationCompleteView.as_view(), name='registration_complete'),
)
