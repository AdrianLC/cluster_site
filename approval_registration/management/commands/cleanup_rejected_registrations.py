"""A django management command which cleans all rejected registration requests from the DB."""

from django.core.management.base import NoArgsCommand

from ...models import RegistrationRequest


class Command(NoArgsCommand):
    help = "Delete rejected user registrations from the database"

    def handle_noargs(self, **options):
        RegistrationRequest.objects.delete_rejected_users()
