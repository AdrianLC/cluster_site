"""A django management command which cleans from the DB both expired and rejected registration
requests."""


from django.core.management.base import NoArgsCommand

from ...models import RegistrationRequest


class Command(NoArgsCommand):
    help = "Delete expired/rejected user registrations from the database."

    def handle_noargs(self, **options):
        RegistrationRequest.objects.delete_expired_users()
        RegistrationRequest.objects.delete_rejected_users()
