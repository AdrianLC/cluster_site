"""Django management command which cleans all the expired registration requests from the database."""

from django.core.management.base import NoArgsCommand

from ...models import RegistrationRequest


class Command(NoArgsCommand):
    help = "Delete expired user registrations from the database."

    def handle_noargs(self, **options):
        RegistrationRequest.objects.delete_expired_users()
