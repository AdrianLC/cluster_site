
from django import forms

from .generic import GrantAccessFormMixin, GrantAccessInlineFormSet, FixedLengthFormSetMixin
from ..models import Context, SSHContext, JobService, JobDescription, Job, JobFile


class ContextForm(GrantAccessFormMixin, forms.ModelForm):
    class Meta:
        model = Context
        fields = []


class SSHContextForm(ContextForm):
    class Meta(ContextForm.Meta):
        model = SSHContext
        fields = ['alias', 'user_id', ]


class JobServiceForm(GrantAccessFormMixin, forms.ModelForm):
    class Meta:
        model = JobService
        fields = ['scheme', 'host', 'port', 'contexts', ]

    def adapt_to_user(self, user):
        self.fields['contexts'].queryset = Context.objects.select_subclasses().accessible_by(user)


class JobDescriptionForm(GrantAccessFormMixin, forms.ModelForm):
    class Meta:
        model = JobDescription
        fields = [
            'name',
            'executable', 'arguments',
            'working_directory', 'environment',
            'queue', 'project',
            'total_cpu_count', 'wall_time_limit',
        ]


class JobForm(GrantAccessFormMixin, forms.ModelForm):
    class Meta:
        model = Job
        fields = ['service', 'description', ]

    def adapt_to_user(self, user):
        self.fields['service'].queryset = JobService.objects.accessible_by(user)
        self.fields['description'].queryset = JobDescription.objects.accessible_by(user)


class JobFileForm(GrantAccessFormMixin, forms.ModelForm):
    class Meta:
        model = JobFile
        fields = ['remote_path', ]


class FileDownloadForm(JobFileForm):
    class Meta(JobFileForm.Meta):
        pass


class FileUploadForm(JobFileForm):
    class Meta(JobFileForm.Meta):
        fields = ['remote_path', 'local', ]

    def __init__(self, *args, **kwargs):
        super(FileUploadForm, self).__init__(*args, **kwargs)
        self.instance.action = JobFile.ACTIONS.upload


class JobFilesInlineFormSet(FixedLengthFormSetMixin, GrantAccessInlineFormSet):

    fixed_length = 2

    JOBFILE_ORDER = [
        JobFile.PECULIAR_FILES.output,
        JobFile.PECULIAR_FILES.error,
    ]

    def _construct_form(self, i, **kwargs):
        form = super(JobFilesInlineFormSet, self)._construct_form(i, **kwargs)
        form.instance.peculiarity = self.JOBFILE_ORDER[i]
        self.set_dynamic_label(form, 'remote_path')
        return form

    def set_dynamic_label(self, form, field):
        if field == 'remote_path':
            form.fields['remote_path'].label = form.instance.get_peculiarity_display().capitalize()
