
from django.forms.models import BaseInlineFormSet


class UserAdaptableFormMixin(object):

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(UserAdaptableFormMixin, self).__init__(*args, **kwargs)
        if self.user:
            self.adapt_to_user(self.user)

    def adapt_to_user(self, user):
        pass


class GrantAccessFormMixin(UserAdaptableFormMixin):

    def save(self, commit=True):
        instance = super(GrantAccessFormMixin, self).save(commit)
        if self.user and commit:
            instance.grant_access(self.user)
        return instance


class UserAdaptableFormSetMixin(UserAdaptableFormMixin):

    def _construct_form(self, i, **kwargs):
        if self.user:
            kwargs['user'] = self.user
        return super(UserAdaptableFormSetMixin, self)._construct_form(i, **kwargs)


class GrantAccessFormSetMixin(UserAdaptableFormSetMixin):

    def save_new(self, form, commit=True):
        instance = super(GrantAccessFormSetMixin, self).save_new(form, commit)
        if self.user and commit:
            instance.grant_access(self.user)
        return instance


class FixedLengthFormSetMixin(object):

    fixed_length = None

    def __init__(self, *args, **kwargs):
        super(FixedLengthFormSetMixin, self).__init__(*args, **kwargs)
        if self.fixed_length:
            self.extra = self.max_num = self.fixed_length

    def _construct_form(self, i, **kwargs):
        kwargs['empty_permitted'] = False
        return super(FixedLengthFormSetMixin, self)._construct_form(i, **kwargs)


class GrantAccessInlineFormSet(GrantAccessFormSetMixin, BaseInlineFormSet):
    pass
