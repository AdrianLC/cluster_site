
from collections import Iterable

from django.db.models import get_model

from celery.app import shared_task as task
from celery.utils.log import get_task_logger
import saga

from .models import JobService, Job, FileTransfer
import settings


logger = get_task_logger(__name__)


# avoid loading Saga's adaptors for every task:
_saga_engine = saga.engine.Engine()


@task(acks_late=True)
def try_job_service(pk):
    """Creates a test instance of the service.

    Returns nothing if the service works properly.
    Otherwise returns a short string describing the issue encountered.
    """
    service = JobService.objects.get(pk=pk)
    try:
        with service.connect():
            return  # No errors
    except saga.SagaException as error:
        return error.type


@task(acks_late=True)
def send_job(pk):
    job = Job.objects.get(pk=pk)
    if not job.sent:
        job.send()
    return job.saga_id


@task(acks_late=True, max_retries=None)
def poll_job(pk):
    job = Job.objects.get(pk=pk)
    if not job.finished:
        job.update()
        logger.info("Job %s is '%s'" % (job, job.state))
        if not job.finished:
            start, step, top = settings.JOB_POLLING_INTERVALS
            countdown = start + step * poll_job.request.retries
            if countdown > top:
                countdown = top
            raise poll_job.retry(countdown=countdown)
    return job.exit_code


@task(acks_late=True, ignore_result=True)
def cancel_job(pk):
    job = Job.objects.get(pk=pk)
    job.cancel()


@task(ignore_result=True)
def handle_jobs(pk):
    if not isinstance(pk, Iterable):
        pk = [pk]
    jobs = Job.objects.filter(pk__in=pk)
    for job in jobs:
        job.handle_async()


@task(acks_late=True)
def transfer_file(job_pk, file_pk):
    job = Job.objects.get(pk=job_pk)
    file_ = job.description.files.get(pk=file_pk)
    transfer, is_new = FileTransfer.objects.get_or_create(job=job, file=file_)
    if is_new or not transfer.done:
        transfer.run()


@task(acks_late=True)
def zip_results(pk):
    job = Job.objects.get(pk=pk)
    job.zip_results()


@task(ignore_result=True)
def cleanup_files(model, fieldname_filename):
    Model = get_model(*model)
    for field_name, file_name in fieldname_filename.iteritems():
        field = Model._meta.get_field(field_name)
        storage = field.storage
        if (
            storage.exists(file_name) and
            not Model.objects.select_for_update().filter(**{field_name: file_name}).exists()
        ):
            try:
                storage.delete(file_name)
            except Exception:
                logger.exception("Unexpected error when deleting file '%s'." % file_name)
