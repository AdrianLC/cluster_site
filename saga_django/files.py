
import os
import stat

from django.core.files.storage import FileSystemStorage
from django.db.models import FileField
from django.utils.functional import memoize

from . import settings


class PrivateFileSystemStorage(FileSystemStorage):
    def __init__(self, location=None, base_url=None, *args, **kwargs):
        if location is None:
            location = settings.PRIVATE_MEDIA_ROOT
        if base_url is None:
            base_url = settings.PRIVATE_MEDIA_URL
        return super(PrivateFileSystemStorage, self).__init__(location, base_url, *args, **kwargs)


default_private_storage = PrivateFileSystemStorage()


class PrivateKeyFilesStorage(PrivateFileSystemStorage):

    def _save(self, name, content):
        name = super(PrivateKeyFilesStorage, self)._save(name, content)
        file_permissions = stat.S_IRUSR | stat.S_IWUSR  # 0600
        original_permission_mask = os.umask(0)  # reset permission mask
        os.chmod(self.path(name), file_permissions)
        os.umask(original_permission_mask)  # restore previous permission mask
        return name

    def url(self, name):
        raise NotImplementedError("Not relevant")


class FileCleaner(object):

    _file_field_cache = {}

    def get_file_fields(cls, Model):
        return filter(lambda field: isinstance(field, FileField), Model._meta.fields)
    get_file_fields = classmethod(memoize(get_file_fields, _file_field_cache, 2))

    @classmethod
    def on_delete(cls, instance, **kwargs):
        Model = instance.__class__
        file_fields = cls.get_file_fields(Model)
        if file_fields:
            replaced_files = {}
            for file_field in file_fields:
                old_file = getattr(instance, file_field.name)
                if old_file:
                    replaced_files[file_field.name] = old_file.name
            cls._queue_cleanup(Model, replaced_files)

    @classmethod
    def on_change(cls, instance, raw, update_fields, **kwargs):
        if raw or not instance.pk:
            return

        Model = instance.__class__
        file_fields = cls.get_file_fields(Model)
        if update_fields is not None:  # partial save
            file_fields = filter(lambda field: field.name in update_fields, file_fields)
        if file_fields:
            try:
                old_instance = Model.objects.get(pk=instance.pk)
            except Model.DoesNotExist:
                return

            replaced_files = {}
            for file_field in file_fields:
                old_file = getattr(old_instance, file_field.name)
                new_file = getattr(instance, file_field.name)
                if old_file and old_file != new_file:
                    replaced_files[file_field.name] = old_file.name
            cls._queue_cleanup(Model, replaced_files)

    @classmethod
    def _queue_cleanup(cls, Model, replaced_files):
        if replaced_files:
            model = (Model._meta.app_label, Model._meta.model_name)
            from .tasks import cleanup_files
            cleanup_files.delay(model, replaced_files)


# https://gist.github.com/btimby/2175107
def walk(storage, top='/', topdown=True, onerror=None):
    """An implementation of os.walk() which uses the Django storage for
    listing directories."""
    try:
        dirs, nondirs = storage.listdir(top)
    except os.error as err:
        if onerror is not None:
            onerror(err)
        return

    if topdown:
        yield top, dirs, nondirs
    for name in dirs:
        new_path = os.path.join(top, name)
        for x in walk(storage, new_path):
            yield x
    if not topdown:
        yield top, dirs, nondirs
