
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


ENCRYPTED_FIELD_KEYS_SIZE = getattr(settings, 'ENCRYPTION_KEYS_SIZE', 256)

try:
    PRIVATE_MEDIA_ROOT = settings.PRIVATE_MEDIA_ROOT
except AttributeError:
    raise ImproperlyConfigured("'PRIVATE_MEDIA_ROOT' setting must be set.")
try:
    PRIVATE_MEDIA_URL = settings.PRIVATE_MEDIA_URL
except AttributeError:
    raise ImproperlyConfigured("'PRIVATE_MEDIA_URL' setting must be set.")


SSH_KEYS_DIR = getattr(settings, 'SSH_KEYS_DIR', 'ssh')

DEPENDENCY_FILES_DIR = getattr(settings, 'DEPENDENCY_FILES_DIR', 'jobfiles/dependencies')
RESULT_FILES_DIR = getattr(settings, 'DEPENDENCY_FILES_DIR', 'jobfiles/results')


SUPPORTED_JOB_SERVICE_ADAPTORS = set(['sge+ssh', 'pbs+ssh', ])

ENABLED_JOB_SERVICE_ADAPTORS = set(
    getattr(settings, 'ENABLED_JOB_SERVICE_ADAPTORS', SUPPORTED_JOB_SERVICE_ADAPTORS)
) & SUPPORTED_JOB_SERVICE_ADAPTORS

JOB_POLLING_INTERVALS = getattr(settings, 'JOB_POLLING_INTERVALS', (10, 15, 600))  # start, step, max (secs)
