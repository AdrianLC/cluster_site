
import re

from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


hostname_label_re = re.compile(r'^(?!-)[a-z\d-]+(?<!-)$', re.IGNORECASE)


def full_domain(hostname):
    """
    Fully validates a domain name as compilant with the standard rules:
        - Composed of series of labels concatenated with dots, as are all domain names.
        - Each label must be between 1 and 63 characters long.
        - The entire hostname (including the delimiting dots) has a maximum of 255 characters.
        - Only characters 'a' through 'z' (in a case-insensitive manner), the digits '0' through '9'.
        - Labels can't start or end with a hyphen.
    """
    if len(hostname) > 255:
        raise ValidationError(_("The domain name cannot be composed of more than 255 characters."))
    hostname = hostname.rstrip('.')  # strip exactly one dot from the right, if present
    for label in hostname.split('.'):
        if len(label) > 63:
            raise ValidationError(
                _("The label '%(label)s' is too long (maximum is 63 characters)."), params={'label': label}
            )
        if not hostname_label_re.match(label):
            raise ValidationError(_("Unallowed characters in label '%(label)s'."), params={'label': label})


### Shell validators ###

_safe_sh_characters = r'[\w@%+\-=:,./]'


sh_identifier_re = re.compile(r'^_*[a-z]\w*$', re.IGNORECASE)


def sh_identifier(identifier):
    if not sh_identifier_re.match(identifier):
        raise ValidationError(_("Identifier: \"%(identifier)s\" is not valid."),
                              params={'identifier': identifier})


def sh_declaration(declaration):
    try:
        identifier, value = declaration.split('=', 1)
    except ValueError:
        raise ValidationError(_("Invalid declaration \"%(declaration)s\". '=' not found."),
                              params={'declaration': declaration})
    sh_identifier(identifier)


def sh_declaration_list(declaration_list):
    if isinstance(declaration_list, basestring):
        declaration_list = filter(None, (line.strip() for line in declaration_list.splitlines()))
    for declaration in declaration_list:
        sh_declaration(declaration)


file_path_re = re.compile(
    r"""
    ^(?:
        (?# Allow escaped \\. characters)
        # QUOTED VALUES:
            (?P<q>["']) (?: \\(?P=q) | (?!(?P=q)). )+ (?P=q)
        # SIMPLE VALUE:
        |   (?: \\. | %(safe_chars)s )+
    )$
    """ % {'safe_chars': _safe_sh_characters},
    re.VERBOSE
)


file_path = RegexValidator(regex=file_path_re)
