
from django.contrib import admin

from .models import (
    SSHContext,
    JobService, JobDescription, Job,
    JobFile, FileTransfer
)


admin.site.register(SSHContext)
admin.site.register(JobService)
admin.site.register(JobDescription)
admin.site.register(Job)
admin.site.register(JobFile)
admin.site.register(FileTransfer)
