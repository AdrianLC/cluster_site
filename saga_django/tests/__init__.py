
import os.path
from tempfile import gettempdir

from django.conf import settings


TEMPDIR = os.path.join(gettempdir(), '%s_testfiles' % getattr(settings, 'SITE_NAME', 'saga_django'))

TEMP_STORAGE_SETTINGS = {
    'MEDIA_ROOT': TEMPDIR,
    'DEFAULT_FILE_STORAGE': 'django.core.files.storage.FileSystemStorage'
}
