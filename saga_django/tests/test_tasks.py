
from django.test import TestCase

from celery.exceptions import Retry as CeleryRetry
import mock
from saga.exceptions import SagaException

from ..models import JobService, JobDescription, Job, JobFile, FileTransfer
from ..tasks import (
    cleanup_files,
    try_job_service,
    send_job, poll_job, cancel_job, handle_jobs,
    transfer_file, zip_results,
)
from .models import FileFieldCleanupTestModel


@mock.patch.object(FileFieldCleanupTestModel._meta.get_field('file2'), 'storage')
@mock.patch.object(FileFieldCleanupTestModel._meta.get_field('file1'), 'storage')
class FileCleanupTaskTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        model_opts = FileFieldCleanupTestModel._meta
        cls.model = (model_opts.app_label, model_opts.model_name)

    def test_correct_file_deletion(self, file1_mock_storage, file2_mock_storage):
        file1_mock_storage.exists.return_value = True
        file2_mock_storage.exists.return_value = True
        cleanup_files(self.model, {'file1': "file1", 'file2': "file2"})
        file1_mock_storage.delete.assert_called_once_with("file1")
        file2_mock_storage.delete.assert_called_once_with("file2")

    def test_with_unexistent_files(self, file1_mock_storage, file2_mock_storage):
        file1_mock_storage.exists.return_value = False
        file2_mock_storage.exists.return_value = False
        cleanup_files(self.model, {'file1': "random_file_name", 'file2': "and_another_one"})
        self.assertFalse(file1_mock_storage.delete.called)
        self.assertFalse(file2_mock_storage.delete.called)

    def test_with_files_still_in_use(self, file1_mock_storage, file2_mock_storage):
        FileFieldCleanupTestModel.objects.create(file1="file1", file2="file2")
        file1_mock_storage.exists.return_value = True
        file2_mock_storage.exists.return_value = True
        cleanup_files(self.model, {'file1': "file1", 'file2': "file2"})
        self.assertFalse(file1_mock_storage.delete.called)
        self.assertFalse(file2_mock_storage.delete.called)


class JobHandlingTasksTestCase(TestCase):

    def setUp(self):
        self.job_service = JobService.objects.create(scheme='ssh', host="example.com")
        self.job_description = JobDescription.objects.create()
        self.job = Job.objects.create(description=self.job_description, service=self.job_service)

    @mock.patch.object(JobService, 'to_saga')
    def test_try_job_service(self, mock_service_to_saga):
        error = try_job_service(self.job_service.pk)
        self.assertIsNone(error)

        saga_exception = SagaException("Filler message")
        mock_service_to_saga.side_effect = saga_exception
        error = try_job_service(self.job_service.pk)
        self.assertIsNotNone(error)
        self.assertEqual(error, saga_exception.type)

    @mock.patch.object(Job, 'send')
    def test_send_job(self, mock_job_send):
        send_job(self.job.pk)
        mock_job_send.assert_called_once_with()

        mock_job_send.reset_mock()
        self.job.saga_id = "[sge+ssh@example.com:22/]-[1000].zip"
        self.job.save()
        saga_id = send_job(self.job.pk)
        self.assertFalse(mock_job_send.called)
        self.assertEqual(saga_id, self.job.saga_id)

    @mock.patch.object(Job, 'update')
    def test_poll_job(self, mock_job_update):
        self.assertRaises(CeleryRetry, poll_job, self.job.pk)
        mock_job_update.assert_called_once_with()

        mock_job_update.reset_mock()
        self.job.state = Job.STATES.Done
        self.job.exit_code = 0
        self.job.save()
        exit_code = poll_job(self.job.pk)
        self.assertFalse(mock_job_update.called)
        self.assertEqual(exit_code, 0)

    @mock.patch.object(Job, 'cancel')
    def test_cancel_job(self, mock_job_cancel):
        cancel_job(self.job.pk)
        mock_job_cancel.assert_called_once_with()

    @mock.patch.object(Job, 'handle_async')
    def test_handle_jobs(self, mock_job_handle_async):
        another_job = Job.objects.get(pk=self.job.pk)
        another_job.pk = None
        another_job.save()
        handle_jobs([self.job.pk, another_job.pk])
        self.assertEqual(mock_job_handle_async.call_count, 2)

    @mock.patch.object(FileTransfer, 'run')
    def test_transfer_file(self, mock_transfer_run):
        file_ = self.job.description.files.create()
        transfer_file(self.job.pk, file_.pk)
        mock_transfer_run.assert_called_once_with()

        mock_transfer_run.reset_mock()
        file_transfer = self.job.file_transfers.get()  # should have been created from the task
        file_transfer.done = True
        file_transfer.save()
        transfer_file(self.job.pk, file_.pk)
        self.assertFalse(mock_transfer_run.called)

    @mock.patch.object(Job, 'zip_results')
    def test_zip_results(self, mock_job_zip_results):
        zip_results(self.job.pk)
        mock_job_zip_results.assert_called_once_with()
