
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.utils import override_settings

import mock

from ...forms import SSHContextForm
from ...models.contexts import SSHContext


User = get_user_model()


# Use app level templates only.
@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class SSHContextCreateViewTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.client.login(username="pepe", password="0000")

    def test_get_context_creation_form(self):
        response = self.client.get(reverse('create_ssh_context'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/contexts/sshcontext_create_form.html")
        self.assertIsInstance(response.context['form'], SSHContextForm)

    # Avoid key file generation IO.
    @mock.patch.object(SSHContext, 'generate', mock.MagicMock(spec=[]))
    def test_post_creating_context(self):
        response = self.client.post(
            reverse('create_ssh_context'),
            data={'alias': "My SSH credential", 'user_id': "pepito"}
        )
        self.assertEqual(SSHContext.objects.accessible_by(self.user).count(), 1)
        created_context = SSHContext.objects.accessible_by(self.user).get()
        self.assertRedirects(response, reverse('detail_ssh_context', args=[created_context.pk]))

    def test_post_invalid(self):
        response = self.client.post(reverse('create_ssh_context'), data={'user_id': "_pepito"})
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['form'].is_valid())


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class SSHContextDetailViewTestCase(TestCase):

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock(spec=[]))
    def setUp(self):
        user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.ssh_context = SSHContext.objects.create_and_grant_access(
            user,
            alias="my key",
            user_id="pepe",
            public_key=("ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxWfn1x+dRqImaeUVtBG86tZv6nhOsm"
                        "MplqZX6y/Z61bqMWrc2+Jl+y+UaECnQGCsO0GQKU9PfqByvqRWdRbUZPbApuzsBqmgCYL"
                        "seXrFi6LrURS4eWZVP0LmokpNn8zeuA+4/u8P6ICv3bEteH0/NXEiipfs9KXwQsHIbVWD"
                        "OsMFte+Ap64X2nyT9i7otIqCucJAw6URj4IDUAak92xFE1saGX+aiILU1RKLAY3tGtxAP"
                        "r7it4D6VLyTaklJeDWitWANP9Y8MWWym3lz7THr0MNOUY145Y03KSMNr1gy4axJtvQklC"
                        "onm4kWhdk0PT53j5aLy/RyK/wNiPONfGZhR"),
            key_fingerprint="2940a024fefc21b8d6a6c96f1a4bdc0b",
        )
        self.client.login(username="pepe", password="0000")

    def test_get_details(self):
        response = self.client.get(reverse('detail_ssh_context', args=[self.ssh_context.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/contexts/sshcontext_detail.html")
        self.assertEqual(response.context['sshcontext'], self.ssh_context)
        self.assertIn(self.ssh_context.alias, response.content)
        self.assertIn(self.ssh_context.user_id, response.content)
        self.assertIn(self.ssh_context.public_key, response.content)
        self.assertIn(self.ssh_context.fingerprint, response.content)


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class SSHContextDeleteViewTestCase(TestCase):

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock(spec=[]))
    def setUp(self):
        user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.ssh_context = SSHContext.objects.create_and_grant_access(user, user_id="pepe")
        self.client.login(username="pepe", password="0000")

    def test_get_deletion_confirmation_request(self):
        response = self.client.get(reverse('delete_ssh_context', args=[self.ssh_context.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/contexts/sshcontext_confirm_delete.html")
        self.assertEqual(response.context['sshcontext'], self.ssh_context)

    def test_post_confirming_deletion(self):
        response = self.client.post(reverse('delete_ssh_context', args=[self.ssh_context.pk]))
        self.assertRedirects(response, reverse('list_ssh_contexts'))
        self.assertFalse(SSHContext.objects.exists())


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class SSHContextListViewTestCase(TestCase):

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock(spec=[]))
    def setUp(self):
        user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.ssh_contexts = [SSHContext.objects.create_and_grant_access(user, user_id="pepito")
                             for i in xrange(2)]
        self.client.login(username="pepe", password="0000")

    def test_get_list_of_contexts(self):
        response = self.client.get(reverse('list_ssh_contexts'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/contexts/sshcontext_list.html")
        self.assertItemsEqual(response.context['sshcontext_list'], self.ssh_contexts)


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class SSHContextUpdateViewTestCase(TestCase):

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock(spec=[]))
    def setUp(self):
        user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.ssh_context = SSHContext.objects.create_and_grant_access(user, user_id="pepe")
        self.client.login(username="pepe", password="0000")

    def test_get_context_update_form(self):
        response = self.client.get(reverse('update_ssh_context', args=[self.ssh_context.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/contexts/sshcontext_update_form.html")
        self.assertIsInstance(response.context['form'], SSHContextForm)
        self.assertEqual(response.context['sshcontext'], self.ssh_context)
        self.assertIn(self.ssh_context.user_id, response.content)

    def test_post_updating_context(self):
        response = self.client.post(
            reverse('update_ssh_context', args=[self.ssh_context.pk]),
            data={'alias': "My SSH credential", 'user_id': "pepito"}
        )
        self.assertRedirects(response, reverse('list_ssh_contexts'))
        self.ssh_context = SSHContext.objects.get(pk=self.ssh_context.pk)
        self.assertEqual(self.ssh_context.user_id, "pepito")

    def test_post_invalid(self):
        response = self.client.post(
            reverse('update_ssh_context', args=[self.ssh_context.pk]),
            data={'user_id': "_pepito"}
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['form'].is_valid())
