
import json

from django.contrib.auth import get_user_model
from django.contrib.messages import constants as message_levels
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.utils import override_settings

import mock

from ...forms import JobServiceForm
from ...models import Context, JobService
from ...views import jobs as views


User = get_user_model()


# Use app level templates only.
@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class JobServiceCreateViewTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.context = Context.objects.create_and_grant_access(self.user)
        self.client.login(username="pepe", password="0000")

    def test_get_job_service_creation_form(self):
        response = self.client.get(reverse('register_job_service'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/jobservice_create_form.html")
        form = response.context['form']
        self.assertIsInstance(form, JobServiceForm)
        self.assertItemsEqual(form.fields['contexts'].queryset, Context.objects.accessible_by(self.user))

    @mock.patch.object(views, 'try_job_service', autospec=True)
    def test_post_creating_job_service(self, mock_try_job_service_task):
        fake_task_id = '3859f94e-230a-4ada-b562-dd918b454639'
        mock_try_job_service_task.delay.return_value.id = fake_task_id
        response = self.client.post(
            reverse('register_job_service'),
            follow=True, HTTP_HOST="testserver",
            data={
                'scheme': 'pbs+ssh',
                'host': "example.com",
                'port': 455,
                'contexts': [self.context.pk]
            }
        )
        self.assertEqual(Context.objects.accessible_by(self.user).count(), 1)
        js = Context.objects.accessible_by(self.user).get()
        mock_try_job_service_task.delay.assert_called_once_with(js.pk)
        wait_page_url = "http://testserver" + reverse('wait_test_job_service', args=[js.pk, fake_task_id])
        self.assertIn((wait_page_url, 302), response.redirect_chain)

    def test_post_form_invalid(self):
        response = self.client.post(
            reverse('register_job_service'),
            follow=True, HTTP_HOST="testserver",
            data={
                'scheme': '???+ssh',
                'host': "example.com",
                'port': 455,
                'contexts': [self.context.pk]
            }
        )
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context['form'].is_valid())


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class JobServiceUpdateViewTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.context = Context.objects.create_and_grant_access(self.user)
        self.instance = JobService.objects.create_and_grant_access(
            self.user,
            scheme='pbs+ssh',
            host="example.com"
        )
        self.client.login(username="pepe", password="0000")

    def test_get_job_service_update_form(self):
        response = self.client.get(reverse('update_job_service', args=[self.instance.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/jobservice_update_form.html")
        form = response.context['form']
        self.assertIsInstance(form, JobServiceForm)
        self.assertItemsEqual(form.fields['contexts'].queryset, Context.objects.accessible_by(self.user))

    # POST shares implementation with JobServiceCreateView so, already tested.


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class JobServiceWaitTestTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.client.login(username="pepe", password="0000")
        self.js = JobService.objects.create_and_grant_access(
            self.user,
            scheme='pbs+ssh',
            host="example.com"
        )
        self.fake_task_id = '3859f94e-230a-4ada-b562-dd918b454639'

        patcher = mock.patch.object(views, 'try_job_service', autospec=True)
        mock_try_job_service_task = patcher.start()
        self.mock_task_result = mock_try_job_service_task.AsyncResult.return_value
        self.addCleanup(patcher.stop)

    def test_get_and_test_not_ready(self):
        self.mock_task_result.ready.return_value = False
        response = self.client.get(reverse('wait_test_job_service', args=[self.js.pk, self.fake_task_id]))
        self.assertTrue(self.mock_task_result.ready.called)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/jobservice_wait_test.html")

    def test_get_and_service_test_failed(self):
        self.mock_task_result.ready.return_value = True
        self.mock_task_result.get.return_value = "Some error"
        response = self.client.get(
            reverse('wait_test_job_service', args=[self.js.pk, self.fake_task_id]), follow=True
        )
        self.assertTrue(self.mock_task_result.ready.called)
        self.assertTrue(self.mock_task_result.get.called)
        self.assertRedirects(response, reverse('update_job_service', args=[self.js.pk]))
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].level, message_levels.ERROR)

    def test_get_and_service_test_passed(self):
        self.mock_task_result.ready.return_value = True
        self.mock_task_result.get.return_value = None
        response = self.client.get(
            reverse('wait_test_job_service', args=[self.js.pk, self.fake_task_id]), follow=True
        )
        self.assertTrue(self.mock_task_result.ready.called)
        self.assertTrue(self.mock_task_result.get.called)
        self.assertRedirects(response, reverse('list_job_services'))
        messages = list(response.context['messages'])
        self.assertEqual(len(messages), 1)
        self.assertEqual(messages[0].level, message_levels.SUCCESS)

    def test_get_ajax_response(self):
        self.mock_task_result.ready.return_value = False
        response = self.client.get(
            reverse('wait_test_job_service', args=[self.js.pk, self.fake_task_id]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        content = json.loads(response.content)
        self.assertIsNone(content)

        self.mock_task_result.ready.return_value = True
        self.mock_task_result.get.return_value = "Some error"
        response = self.client.get(
            reverse('wait_test_job_service', args=[self.js.pk, self.fake_task_id]),
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'
        )
        content = json.loads(response.content)
        self.assertIn('redirect', content)
        self.assertEqual(content['redirect'], reverse('update_job_service', args=[self.js.pk]))


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class JobServiceListViewTestCase(TestCase):

    def setUp(self):
        user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.job_services = [JobService.objects.create_and_grant_access(
            user, scheme='pbs+ssh', host="example.com"
        ) for i in xrange(2)]
        self.client.login(username="pepe", password="0000")

    def test_get_list_of_job_services(self):
        response = self.client.get(reverse('list_job_services'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/jobservice_list.html")
        self.assertItemsEqual(response.context['jobservice_list'], self.job_services)


@override_settings(TEMPLATE_LOADERS=('django.template.loaders.app_directories.Loader', ))
class JobServiceDeleteViewTestCase(TestCase):

    def setUp(self):
        user = User.objects.create_user(
            username="pepe",
            password="0000",
            email="pepe@example.com"
        )
        self.instance = JobService.objects.create_and_grant_access(
            user,
            scheme='pbs+ssh',
            host="example.com"
        )
        self.client.login(username="pepe", password="0000")

    def test_get_deletion_confirmation_request(self):
        response = self.client.get(reverse('delete_job_service', args=[self.instance.pk]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "saga_django/jobservice_confirm_delete.html")
        self.assertEqual(response.context['jobservice'], self.instance)

    def test_post_confirming_deletion(self):
        response = self.client.post(reverse('delete_job_service', args=[self.instance.pk]))
        self.assertRedirects(response, reverse('list_job_services'))
        self.assertFalse(JobService.objects.exists())
