
from django.db import models

from ..models.base import SagaModel


class SagaTestModel(SagaModel):
    class Meta(SagaModel.Meta):
        app_label = 'tests'  # Discard inherited 'app_label'


class FileFieldCleanupTestModel(models.Model):
    file1 = models.FileField(upload_to='.')
    file2 = models.FileField(upload_to='some_folder')
    non_file = models.CharField(max_length=10, null=True)
