
from django.test import TestCase
from django.test.utils import override_settings

from Crypto.PublicKey import RSA
from django_extensions.db.fields.encrypted import EncryptedCharField
import mock

from ...models.contexts import SSHContext


class SSHContextTestCase(TestCase):

    def setUp(self):
        self.instance = SSHContext()

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock())
    def test_can_store_alias(self):
        alias = "key for my cluster"
        self.instance.alias = alias
        self.assertEqual(self.instance.alias, alias)
        self.instance.save()
        self.instance = SSHContext.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.alias, alias)

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock())
    def test_can_store_remote_username(self):
        username = "JohnDoe"
        self.instance.user_id = username
        self.assertEqual(self.instance.user_id, username)
        self.instance.save()
        self.instance = SSHContext.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.user_id, username)

    @mock.patch.object(SSHContext, 'generate', mock.MagicMock())
    def test_can_store_password(self):
        password = "0000000000"
        self.instance.user_pass = password
        self.assertEqual(self.instance.user_pass, password)
        self.instance.save()
        self.instance = SSHContext.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.user_pass, password)

    def test_random_password_automatic_creation(self):
        self.assertIsNotNone(self.instance.user_pass)
        self.assertNotEqual(self.instance.user_pass, "")

    def test_sensible_fields_are_encrypted(self):
        self.assertIsInstance(SSHContext._meta.get_field('user_id'), EncryptedCharField)
        self.assertIsInstance(SSHContext._meta.get_field('user_pass'), EncryptedCharField)

    def test__unicode__value(self):
        self.assertEqual(unicode(self.instance), "SSHContext object")
        self.instance.alias = "My SSH credential"
        self.instance.key_fingerprint = "2940a024fefc21b8d6a6c96f1a4bdc0b"
        self.assertIn(self.instance.alias, unicode(self.instance))
        self.assertIn(self.instance.fingerprint, unicode(self.instance))

    @override_settings(SITE_NAME="Some site Name")
    def test_key_pair_generation(self):
        private_key, public_key = SSHContext.generate_key_pair()
        key = RSA.importKey(private_key)
        # For private keys with password the exported key is different each time
        # so test only if it looks like a valid private PEM key.
        self.assertIn("-----BEGIN RSA PRIVATE KEY-----", private_key)
        self.assertIn("-----END RSA PRIVATE KEY-----", private_key)
        self.assertEqual(len(private_key), len(key.exportKey()))

        self.assertIn("ssh-rsa", public_key)
        self.assertIn(key.publickey().exportKey(format='OpenSSH'), public_key)
        self.assertIn("some-site-name", public_key)

    def test_fingerprint_construction(self):
        test_public_key = ("ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCxWfn1x+dRqImaeUVtBG86tZv6nhOsm"
                           "MplqZX6y/Z61bqMWrc2+Jl+y+UaECnQGCsO0GQKU9PfqByvqRWdRbUZPbApuzsBqmgCYL"
                           "seXrFi6LrURS4eWZVP0LmokpNn8zeuA+4/u8P6ICv3bEteH0/NXEiipfs9KXwQsHIbVWD"
                           "OsMFte+Ap64X2nyT9i7otIqCucJAw6URj4IDUAak92xFE1saGX+aiILU1RKLAY3tGtxAP"
                           "r7it4D6VLyTaklJeDWitWANP9Y8MWWym3lz7THr0MNOUY145Y03KSMNr1gy4axJtvQklC"
                           "onm4kWhdk0PT53j5aLy/RyK/wNiPONfGZhR")
        # Result of `ssh-keygen -l -f (file_with_that_key)` :
        expected_fingerprint = "2940a024fefc21b8d6a6c96f1a4bdc0b"
        fingerprint = SSHContext.construct_fingerprint(test_public_key)
        self.assertEqual(fingerprint, expected_fingerprint)

    def test_fingerprint_property(self):
        self.assertEqual(self.instance.fingerprint, "")
        self.instance.key_fingerprint = "2940a024fefc21b8d6a6c96f1a4bdc0b"
        expected_fingerprint = "29:40:a0:24:fe:fc:21:b8:d6:a6:c9:6f:1a:4b:dc:0b"
        self.assertEqual(self.instance.fingerprint, expected_fingerprint)

    @mock.patch.object(SSHContext, 'construct_fingerprint')
    @mock.patch.object(SSHContext, 'generate_key_pair')
    def test_automatic_field_generation(self, mock_generate_key_pair, mock_construct_fingerprint):
        mock_generate_key_pair.return_value = ("private_key", "public_key")
        mock_construct_fingerprint.return_value = "2940a024fefc21b8d6a6c96f1a4bdc0b"
        self.instance.generate(save=False)
        self.assertEqual(self.instance.public_key, "public_key")
        self.assertEqual(self.instance.key_fingerprint, "2940a024fefc21b8d6a6c96f1a4bdc0b")
        mock_generate_key_pair.assert_called_once_with(passphrase=self.instance.user_pass)
        mock_construct_fingerprint.assert_called_once_with(self.instance.public_key)

    @mock.patch.object(SSHContext, 'generate')
    def test_automatically_generated_only_on_first_save(self, mock_generate):
        self.assertFalse(mock_generate.called)
        self.instance.save()
        self.assertTrue(mock_generate.called)
        self.assertEqual(mock_generate.call_count, 1)
        self.instance.save()
        self.assertEqual(mock_generate.call_count, 1)

    @mock.patch('saga.Context', autospec=True)
    def test_to_saga(self, MockContext):
        self.instance.user_id = "pepe_account_at_remote_cluster"
        self.instance.user_cert = "private_key"
        mock_context = self.instance.to_saga()
        MockContext.assert_called_once_with('SSH')
        self.assertEqual(mock_context.user_id, self.instance.user_id)
        self.assertEqual(mock_context.user_pass, self.instance.user_pass)
        self.assertEqual(mock_context.user_cert, self.instance.user_cert.path)
