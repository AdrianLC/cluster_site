
from django.contrib.auth import get_user_model
from django.test import TestCase

from guardian.shortcuts import assign_perm

from ..models import SagaTestModel


User = get_user_model()


class SagaBaseModelTestCase(TestCase):

    def setUp(self):
        self.instance = SagaTestModel()

    def test_filter_by_access(self):
        self.instance.save()
        user = User.objects.create(username="pepe", password="whatever", email="pepe@example.com")
        self.assertEqual(SagaTestModel.objects.accessible_by(user).count(), 0)
        assign_perm('can_access', user, self.instance)
        self.assertItemsEqual(SagaTestModel.objects.accessible_by(user), [self.instance])

    def test_create_granting_access_shortcut(self):
        user = User.objects.create(username="pepe", password="whatever", email="pepe@example.com")
        instance = SagaTestModel.objects.create_and_grant_access(user)
        self.assertTrue(user.has_perm('can_access', instance))

    def test_not_implemented_to_saga(self):
        self.assertRaises(NotImplementedError, self.instance.to_saga)
