
from django.db import transaction, IntegrityError
from django.test import TestCase

import mock

from ...models.files import JobFile, FileTransfer
from ...models.jobs import JobDescription, JobService, Job


class JobFileTestCase(TestCase):

    def setUp(self):
        self.job_description = JobDescription.objects.create()
        self.instance = JobFile(job_description=self.job_description)

    def test_must_store_job_description(self):
        self.instance.save()  # works cause service and description are set on setUp()
        self.assertRaises(IntegrityError, JobFile().save)

    def test_store_job_description(self):
        self.instance.save()
        self.instance = JobFile.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.job_description, self.job_description)

    def test_reverse_link_from_job_description(self):
        self.assertFalse(self.job_description.files.exists())
        self.instance.save()
        self.assertItemsEqual(self.job_description.files.all(), [self.instance])

    def test_store_action(self):
        self.assertEqual(self.instance.action, JobFile.ACTIONS.download)  # default
        self.instance.action = JobFile.ACTIONS.upload
        self.instance.save()
        self.instance = JobFile.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.action, JobFile.ACTIONS.upload)

    def test_store_peculiarity(self):
        self.assertIsNone(self.instance.peculiarity)  # default
        self.instance.peculiarity = JobFile.PECULIAR_FILES.output
        self.instance.save()
        self.instance = JobFile.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.peculiarity, JobFile.PECULIAR_FILES.output)

    def test_store_local_file_name(self):
        self.instance.local = "input.txt"
        self.instance.save()
        self.instance = JobFile.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.local.name, "input.txt")

    def test_store_remote_path(self):
        self.instance.remote_path = "/home/user/output.txt"
        self.instance.save()
        self.instance = JobFile.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.remote_path, "/home/user/output.txt")

    def test_manager_filters_by_action(self):
        kwargs = {'job_description': self.job_description, 'action': JobFile.ACTIONS.download}
        downloads = (
            JobFile.objects.create(remote_path="file1", **kwargs),
            JobFile.objects.create(remote_path="file2", **kwargs),
        )
        kwargs['action'] = JobFile.ACTIONS.upload
        uploads = (
            JobFile.objects.create(remote_path="file1", **kwargs),
            JobFile.objects.create(remote_path="file2", **kwargs),
        )
        self.assertItemsEqual(JobFile.objects.downloads(), downloads)
        self.assertItemsEqual(JobFile.objects.uploads(), uploads)

    def test_unique_constraints(self):
        self.instance.remote_path = "same_file.txt"
        self.instance.save()
        # different action is allowed:
        JobFile.objects.create(
            job_description=self.job_description,
            remote_path="same_file.txt",
            action=JobFile.ACTIONS.upload
        )
        # also different path:
        JobFile.objects.create(job_description=self.job_description, remote_path="different_file.txt")
        # but not same remote_path and action (download/upload same file more than once)
        with self.assertRaises(IntegrityError):
            # need atomic() because every test is ran inside a transaction and the DB error
            # prevents the next queries.
            with transaction.atomic():
                JobFile.objects.create(
                    job_description=self.job_description,
                    remote_path="same_file.txt"
                )

        self.instance.peculiarity = JobFile.PECULIAR_FILES.output
        self.instance.save()
        # only one file per peculiarity and description allowed:
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                JobFile.objects.create(
                    job_description=self.job_description,
                    peculiarity=JobFile.PECULIAR_FILES.output,
                )

    def test__unicode__value(self):
        self.assertEqual(unicode(self.instance), "JobFile object")
        self.instance.remote_path = "/etc/fstab"
        self.assertEqual(unicode(self.instance), "Download file: /etc/fstab")
        self.instance.action = JobFile.ACTIONS.upload
        self.assertEqual(unicode(self.instance), "Upload file: /etc/fstab")


class FileTransferTestCase(TestCase):

    def setUp(self):
        self.job_service = JobService.objects.create()
        self.job_description = JobDescription.objects.create()
        self.file = self.job_description.files.create()
        self.job = Job.objects.create(service=self.job_service, description=self.job_description)
        self.instance = FileTransfer(job=self.job, file=self.file)

    def test_must_store_job(self):
        self.instance.save()  # works cause job has been set on setUp()
        self.assertRaises(IntegrityError, FileTransfer(file=self.file).save)

    def test_store_job(self):
        self.instance.save()
        self.instance = FileTransfer.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.job, self.job)

    def test_reverse_link_from_job(self):
        self.assertFalse(self.job.file_transfers.exists())
        self.instance.save()
        self.assertItemsEqual(self.job.file_transfers.all(), [self.instance])

    def test_must_store_file(self):
        self.instance.save()  # works cause file has been set on setUp()
        self.assertRaises(IntegrityError, FileTransfer(job=self.job).save)

    def test_store_file(self):
        self.instance.save()
        self.instance = FileTransfer.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.file, self.file)

    def test_reverse_link_from_file(self):
        self.assertFalse(self.file.transfers.exists())
        self.instance.save()
        self.assertItemsEqual(self.file.transfers.all(), [self.instance])

    def test_store_done_flag(self):
        self.assertFalse(self.instance.done)  # default
        self.instance.done = True
        self.instance.save()
        self.instance = FileTransfer.objects.get(pk=self.instance.pk)
        self.assertTrue(self.instance.done)

    def test_store_result_file_name(self):
        self.instance.result = "obtained_file.txt"
        self.instance.save()
        self.instance = FileTransfer.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.result.name, "obtained_file.txt")

    def test_result_upload_to(self):
        self.job.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        self.job.save()
        self.assertEqual(
            self.instance.get_result_filename("obtained_file.txt"),
            "[sge+ssh@example.com:22/]-[1000]/obtained_file.txt"
        )

    def test_unique_constraints(self):
        self.instance.save()
        self.assertRaises(IntegrityError, FileTransfer(job=self.job, file=self.file).save)

    @mock.patch.object(FileTransfer, 'download')
    @mock.patch.object(FileTransfer, 'upload')
    def test_transfer_run(self, mock_upload, mock_download):
        self.instance.file.action = JobFile.ACTIONS.upload
        self.instance.file.save()
        self.instance.run(save=False)
        mock_upload.assert_called_once_with()
        self.assertTrue(self.instance.done)

        self.instance.file.action = JobFile.ACTIONS.download
        self.instance.file.save()
        self.instance.run(save=False)
        mock_download.assert_called_once_with()
        self.assertTrue(self.instance.done)

    @mock.patch.object(FileTransfer, '_get_session')
    @mock.patch.object(FileTransfer, '_get_local_url')
    @mock.patch.object(FileTransfer, '_get_remote_url')
    @mock.patch('saga.filesystem')
    def test_upload(self, mock_saga_filesystem,
                    mock_get_remote_url, mock_get_local_url,
                    mock_get_session):
        self.instance.file.action = JobFile.ACTIONS.upload
        self.instance.file.local = "file.txt"
        self.instance.file.remote_path = "remote_file.txt"
        self.instance.file.save()
        self.instance.job.description.working_directory = "/home"
        self.instance.job.description.save()

        mock_remote_url = mock_get_remote_url.return_value
        mock_local_url = mock_get_local_url.return_value
        mock_session = mock_get_session.return_value
        MockSagaDirecory = mock_saga_filesystem.Directory
        mock_saga_directory = MockSagaDirecory.return_value

        self.instance.upload()

        mock_get_remote_url.assert_has_calls([
            mock.call(self.instance.job.description.working_directory, absolute=True),
            mock.call()
        ])
        mock_get_local_url.assert_called_once_with()
        mock_get_session.assert_called_once_with()

        MockSagaDirecory.assert_called_once_with(
            mock_remote_url,
            flags=mock_saga_filesystem.CREATE_PARENTS,
            session=mock_session
        )

        mock_saga_directory.copy.assert_called_once_with(
            mock_local_url, mock_remote_url,
            flags=mock_saga_filesystem.CREATE_PARENTS | mock_saga_filesystem.RECURSIVE
        )

        mock_saga_directory.close.assert_called_once_with()

    @mock.patch.object(FileTransfer, '_get_session')
    @mock.patch.object(FileTransfer, '_get_local_url')
    @mock.patch.object(FileTransfer, '_get_remote_url')
    @mock.patch('saga.filesystem')
    def test_download(self, mock_saga_filesystem,
                      mock_get_remote_url, mock_get_local_url,
                      mock_get_session):
        self.instance.file.action = JobFile.ACTIONS.download
        self.instance.file.remote_path = "remote_file.txt"
        self.instance.file.save()
        self.instance.job.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        self.instance.job.save()

        mock_remote_url = mock_get_remote_url.return_value
        mock_local_url = mock_get_local_url.return_value
        mock_session = mock_get_session.return_value
        MockSagaFile = mock_saga_filesystem.File
        mock_saga_file = MockSagaFile.return_value

        self.instance.download()

        mock_get_remote_url.assert_called_once_with()
        mock_get_local_url.assert_called_once_with(self.instance.result.path)
        mock_get_session.assert_called_once_with()

        MockSagaFile.assert_called_once_with(mock_remote_url, session=mock_session)

        mock_saga_file.copy.assert_called_once_with(
            mock_local_url,
            flags=mock_saga_filesystem.CREATE_PARENTS | mock_saga_filesystem.RECURSIVE
        )

        mock_saga_file.close.assert_called_once_with()

    def test_saga_url_getters(self):
        self.instance.file.local = "file.txt"
        self.instance.file.remote_path = "remote_file.txt"
        self.instance.file.save()
        self.instance.job.description.working_directory = "/home"
        self.instance.job.description.save()
        self.instance.job.service.host = "example.com"
        self.instance.job.service.save()

        local_path = self.instance.file.local.path
        self.assertEqual(self.instance._get_local_url(), "file://localhost/" + local_path)
        self.assertEqual(self.instance._get_local_url("bashrc"), "file://localhost/bashrc")

        self.assertEqual(self.instance._get_remote_url(), "sftp://example.com//home/remote_file.txt")
        self.assertEqual(self.instance._get_remote_url("/etc/fstab"), "sftp://example.com//etc/fstab")
        self.assertEqual(self.instance._get_remote_url(absolute=True), "sftp://example.com/remote_file.txt")
