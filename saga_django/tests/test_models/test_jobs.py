
from django.db import IntegrityError
from django.test import TestCase

import mock
from saga import SagaException

from ... import tasks
from ...models import job as job_model_module
from ...models.contexts import Context
from ...models.files import JobFile, FileTransfer
from ...models.jobs import JobService, JobDescription, Job


class JobServiceTestCase(TestCase):

    def setUp(self):
        self.instance = JobService()

    def test_can_store_adaptors_scheme(self):
        self.instance.scheme = JobService.SCHEMES.sge_ssh
        self.instance.save()
        self.instance = JobService.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.scheme, 'sge+ssh')

    def test_can_store_host(self):
        self.instance.host = "some.remote.host.anywhere.es.us.co.uk"
        self.instance.save()
        self.instance = JobService.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.host, "some.remote.host.anywhere.es.us.co.uk")

    def test_can_store_port(self):
        self.assertGreater(self.instance.port, 0)
        self.instance.port = 22
        self.instance.save()
        self.instance = JobService.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.port, 22)

    def test_can_store_contexts(self):
        self.instance.save()
        self.assertFalse(self.instance.contexts.exists())
        contexts = [Context.objects.create() for i in xrange(2)]
        for context in contexts:
            self.instance.contexts.add(context)
        self.assertItemsEqual(self.instance.contexts.all(), contexts)

    def test_reverse_link_from_context(self):
        context = Context.objects.create()
        self.assertFalse(context.jobservice_set.exists())
        self.instance.save()
        context.jobservice_set.add(self.instance)
        self.assertItemsEqual(context.jobservice_set.all(), [self.instance])

    def test_job_description_relationship(self):
        self.instance.save()
        self.assertFalse(self.instance.jobs.exists())
        job = Job.objects.create(service=self.instance, description=JobDescription.objects.create())
        self.assertItemsEqual(self.instance.jobs.all(), [job.description])

    def test__unicode__value(self):
        self.assertEqual(unicode(self.instance), "JobService object")
        self.instance.scheme = JobService.SCHEMES.pbs_ssh
        self.instance.host = "some.remote.host.anywhere.es.us.co.uk"
        self.instance.port = 0
        self.assertEqual(
            unicode(self.instance),
            "(PBS over SSH) -> some.remote.host.anywhere.es.us.co.uk"
        )
        self.instance.port = 22
        self.assertEqual(
            unicode(self.instance),
            "(PBS over SSH) -> some.remote.host.anywhere.es.us.co.uk:22"
        )

    @mock.patch('saga.job.Service', autospec=True)
    @mock.patch('saga.Session', autospec=True)
    @mock.patch.object(Context, 'to_saga')
    def test_to_saga(self, mock_context_to_saga, MockSession, MockJobService):
        self.instance.scheme = JobService.SCHEMES.pbs_ssh
        self.instance.host = "some.remote.host.anywhere.es.us.co.uk"
        self.instance.port = 22
        self.instance.save()
        context = Context.objects.create()
        self.instance.contexts.add(context)
        job_service = self.instance.to_saga()
        MockSession.return_value.add_context.assert_called_with(mock_context_to_saga.return_value)
        MockJobService.assert_called_once_with("pbs+ssh://some.remote.host.anywhere.es.us.co.uk:22",
                                               session=MockSession.return_value)
        self.assertEqual(job_service, MockJobService.return_value)

    @mock.patch.object(JobService, 'to_saga')
    def test_connection_context_manager_with_working_job_service(self, mock_to_saga):
        self.instance.works = False
        self.instance.save()
        with self.instance.connect() as saga_js:
            self.assertTrue(mock_to_saga.called)
            self.assertEqual(saga_js, mock_to_saga.return_value)
        self.assertTrue(mock_to_saga.return_value.close.called)
        self.assertTrue(mock_to_saga.called)
        new_instance = JobService.objects.get(pk=self.instance.pk)
        self.assertTrue(new_instance.works)

    @mock.patch.object(JobService, 'to_saga')
    def test_connection_context_manager_with_faulty_job_service(self, mock_to_saga):
        self.instance.works = True
        self.instance.save()
        saga_exception = SagaException("Filler message")
        mock_to_saga.side_effect = saga_exception
        with self.assertRaisesRegexp(SagaException, "Filler message"):
            with self.instance.connect():
                self.fail("Shouldn't reach this line")
            self.assertTrue(mock_to_saga.called)
        new_instance = JobService.objects.get(pk=self.instance.pk)
        self.assertFalse(new_instance.works)
        self.assertIsNotNone(new_instance.last_attempt)


class JobDescriptionTestCase(TestCase):

    def setUp(self):
        self.instance = JobDescription()

    def test_store_job_name(self):
        self.instance.name = "Whatever"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.name, "Whatever")

    def test_store_executable(self):
        self.instance.executable = "/bin/ps"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.executable, "/bin/ps")

    def test_store_arguments(self):
        self.instance.arguments = "-e -l"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.arguments, "-e -l")

    def test_store_environment(self):
        self.instance.environment = "SOME_VAR=1000\n ANOTHER_VAR=2000+500"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.environment, "SOME_VAR=1000\n ANOTHER_VAR=2000+500")

    def test_store_working_directory(self):
        self.instance.working_directory = "some/subdirectory/"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.working_directory, "some/subdirectory/")

    def test_store_queue(self):
        self.instance.queue = "batch"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.queue, "batch")

    def test_store_project(self):
        self.instance.project = "saga-django test"
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.project, "saga-django test")

    def test_store_cpu_count(self):
        self.instance.total_cpu_count = 4
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.total_cpu_count, 4)

    def test_store_wall_time_limit(self):
        self.instance.wall_time_limit = 3600
        self.instance.save()
        self.instance = JobDescription.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.wall_time_limit, 3600)

    def test_job_service_relationship(self):
        self.instance.save()
        self.assertFalse(self.instance.jobservice_set.exists())
        job = Job.objects.create(service=JobService.objects.create(), description=self.instance)
        self.assertItemsEqual(self.instance.jobservice_set.all(), [job.service])

    def test_environment_as_dict(self):
        self.instance.environment = "FIRST=1000\n SECOND=2000\n THIRD=$FIRST+$SECOND\n"
        self.assertItemsEqual(
            self.instance.environment_as_dict(),
            {
                "FIRST": "1000",
                "SECOND": "2000",
                "THIRD": "$FIRST+$SECOND"
            }
        )

    @mock.patch('saga.job.Description', autospec=True)
    def test_to_saga(self, MockDescription):
        self.instance.name = "whatever"
        self.instance.executable = "/bin/ps"
        self.instance.arguments = "-el -y"
        self.instance.environment = "FIRST=1000\n SECOND=2000\n THIRD=$FIRST+$SECOND\n"
        self.instance.working_directory = "$HOME"
        self.instance.queue = "batch"
        self.instance.project = "saga-django test"
        self.instance.total_cpu_count = 4
        job_description = self.instance.to_saga()
        self.assertEqual(job_description, MockDescription.return_value)
        self.assertEqual(job_description.name, "whatever")
        self.assertEqual(job_description.executable, "/bin/ps")
        self.assertEqual(job_description.arguments, ["-el -y"])
        self.assertEqual(job_description.environment, self.instance.environment_as_dict())
        self.assertEqual(job_description.working_directory, "$HOME")
        self.assertEqual(job_description.queue, "batch")
        self.assertEqual(job_description.project, "saga-django test")
        self.assertEqual(job_description.total_cpu_count, 4)


class JobTestCase(TestCase):

    def setUp(self):
        self.job_service = JobService.objects.create()
        self.job_description = JobDescription.objects.create()
        self.instance = Job(service=self.job_service, description=self.job_description)

    def test_must_store_job_service_and_description(self):
        self.instance.save()  # works cause service and description are set on setUp()
        self.assertRaises(IntegrityError, Job().save)

    def test_store_job_service(self):
        self.instance.save()
        self.instance = Job.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.service, self.job_service)

    def test_store_job_description(self):
        self.instance.save()
        self.instance = Job.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.description, self.job_description)

    def test_reverse_link_from_job_service(self):
        self.assertFalse(self.job_service.job_set.exists())
        self.instance.save()
        self.assertItemsEqual(self.job_service.job_set.all(), [self.instance])

    def test_reverse_link_from_job_description(self):
        self.assertFalse(self.job_description.job_set.exists())
        self.instance.save()
        self.assertItemsEqual(self.job_description.job_set.all(), [self.instance])

    def test_store_saga_id(self):
        self.instance.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        self.instance.save()
        self.instance = Job.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.saga_id, "[sge+ssh://example.com:22/]-[1000]")

    def test_store_job_state(self):
        self.assertEqual(self.instance.state, Job.STATES.Unknown)  # default
        self.instance.state = Job.STATES.Running
        self.instance.save()
        self.instance = Job.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.state, Job.STATES.Running)

    def test_store_exit_code(self):
        self.instance.exit_code = 127
        self.instance.save()
        self.instance = Job.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.exit_code, 127)

    def test_store_zipped_results_file_name(self):
        self.instance.zipped_results = "[sge+ssh@example.com:22/]-[1000].zip"
        self.instance.save()
        self.instance = Job.objects.get(pk=self.instance.pk)
        self.assertEqual(self.instance.zipped_results.name, "[sge+ssh@example.com:22/]-[1000].zip")

    def test__unicode__value(self):
        self.assertEqual(unicode(self.instance), "Job object")
        self.instance.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        self.assertEqual(unicode(self.instance), "[sge+ssh://example.com:22/]-[1000]")

    def test_sent_property(self):
        self.assertFalse(self.instance.sent)
        self.instance.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        self.assertTrue(self.instance.sent)

    def test_finised_property(self):
        self.instance.state = Job.STATES.Running
        self.assertFalse(self.instance.finished)
        self.instance.state = Job.STATES.Done
        self.assertTrue(self.instance.finished)

    @mock.patch.object(JobDescription, 'to_saga')
    @mock.patch.object(JobService, 'to_saga')
    def test_to_saga(self, mock_service_to_saga, mock_description_to_saga):
        job = self.instance.to_saga()
        self.assertTrue(mock_service_to_saga.called)
        self.assertTrue(mock_description_to_saga.called)
        mock_job_service = mock_service_to_saga.return_value
        mock_job_description = mock_description_to_saga.return_value
        mock_job_service.create_job.assert_called_once_with(mock_job_description)
        self.assertEqual(job, mock_job_service.create_job.return_value)

        mock_service_to_saga.reset_mock()
        job = self.instance.to_saga(service=mock_service_to_saga.return_value)
        self.assertFalse(mock_service_to_saga.called)

        mock_service_to_saga.reset_mock()
        mock_description_to_saga.reset_mock()
        self.instance.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        job = self.instance.to_saga()
        self.assertTrue(mock_service_to_saga.called)
        self.assertFalse(mock_description_to_saga.called)
        mock_job_service.get_job.assert_called_once_with("[sge+ssh://example.com:22/]-[1000]")

    @mock.patch('saga_django.tasks')
    @mock.patch.object(job_model_module, 'pickle')
    @mock.patch.object(job_model_module, 'celery')
    def test_handle_async(self, mock_celery, mock_pickle, mock_tasks):
        input_file = self.job_description.files.create(
            action=JobFile.ACTIONS.upload, remote_path="input"
        )
        output_file = self.job_description.files.create(
            peculiarity=JobFile.PECULIAR_FILES.output, remote_path="otuput"
        )
        error_file = self.job_description.files.create(
            peculiarity=JobFile.PECULIAR_FILES.error, remote_path="error"
        )

        def exhaust_task_generator(tasks):
            """So that assertions about it can be made."""
            list(tasks)
            return mock.DEFAULT
        mock_celery.group.side_effect = exhaust_task_generator
        mock_pickle.dumps.return_value = "<pickled task chain serialization>"

        self.instance.save()
        async_result = self.instance.handle_async()

        mock_tasks.send_job.si.assert_called_once_with(self.instance.pk)
        mock_tasks.poll_job.si.assert_called_once_with(self.instance.pk)
        mock_tasks.transfer_file.si.assert_has_calls([
            mock.call(self.instance.pk, input_file.pk),
            mock.call(self.instance.pk, output_file.pk),
            mock.call(self.instance.pk, error_file.pk),
        ])
        self.assertEqual(mock_celery.group.call_count, 2)  # uploads and downloads
        mock_tasks.zip_results.si.assert_called_once_with(self.instance.pk)

        mock_celery.chain.assert_called_once_with(
            mock_celery.group.return_value,  # uploads
            mock_tasks.send_job.si.return_value,
            mock_tasks.poll_job.si.return_value,
            mock_celery.group.return_value,  # downloads
            mock_tasks.zip_results.si.return_value,
        )

        mock_celery.chain.return_value.delay.assert_called_once_with()
        mock_async_result = mock_celery.chain.return_value.delay.return_value
        mock_async_result.serializable.assert_called_once_with()

        self.assertEqual(self.instance._task, "<pickled task chain serialization>")
        self.assertEqual(async_result, mock_async_result)

    @mock.patch.object(JobService, 'connect')
    @mock.patch.object(Job, 'update')
    @mock.patch.object(Job, 'to_saga')
    def test_send_job(self, mock_job_to_saga, mock_job_update, mock_service_connect):
        mock_job = mock_job_to_saga.return_value
        mock_job.id = "[sge+ssh://example.com:22/]-[1000]"
        self.instance.send()
        self.assertTrue(mock_service_connect.return_value.__enter__.called)
        mock_service_connection = mock_service_connect.return_value.__enter__.return_value
        mock_job_to_saga.assert_called_once_with(mock_service_connection)
        mock_job.run.assert_called_once_with()
        self.assertTrue(mock_service_connect.return_value.__exit__.called)
        self.assertEqual(self.instance.saga_id, "[sge+ssh://example.com:22/]-[1000]")
        self.assertIsNotNone(self.instance.dispatch_time)
        mock_job_update.assert_called_once_with(mock_job, save=False)

    @mock.patch.object(JobService, 'connect')
    @mock.patch.object(Job, 'to_saga')
    def test_update_job(self, mock_job_to_saga, mock_service_connect):
        mock_job = mock_job_to_saga.return_value
        mock_job.state = Job.STATES.Done
        mock_job.exit_code = 0
        self.instance.update()
        self.assertTrue(mock_service_connect.return_value.__enter__.called)
        mock_service_connection = mock_service_connect.return_value.__enter__.return_value
        mock_job_to_saga.assert_called_once_with(mock_service_connection)
        self.assertTrue(mock_service_connect.return_value.__exit__.called)
        self.assertEqual(self.instance.state, Job.STATES.Done)
        self.assertEqual(self.instance.exit_code, 0)

    @mock.patch.object(job_model_module, 'pickle', mock.MagicMock())
    @mock.patch.object(job_model_module, 'celery')
    @mock.patch.object(JobService, 'connect')
    @mock.patch.object(Job, 'to_saga')
    def test_cancel_job(self, mock_job_to_saga, mock_service_connect, mock_celery):
        # create a linked list of mock task results:
        mock_task_chain = [mock.MagicMock() for i in xrange(2)]
        mock_task_chain[0].parent = mock_task_chain[1]
        mock_task_chain[1].parent = None
        # serialzable returns first mock:
        mock_celery.result.from_serializable.return_value = mock_task_chain[0]

        # test for job with dispatch pending:
        self.instance._task = "<pickled task chain serialization>"
        self.instance.cancel(save=False)

        self.assertTrue(mock_celery.result.from_serializable.called)
        for mock_task_result in mock_task_chain:
            mock_task_result.revoke.assert_called_once_with()
        self.assertEqual(self.instance._task, "")
        self.assertFalse(mock_service_connect.return_value.__enter__.called)
        self.assertFalse(mock_job_to_saga.called)
        self.assertEqual(self.instance.state, Job.STATES.Canceled)

        for mock_task_result in mock_task_chain:
            mock_task_result.revoke.reset_mock()

        # test for job already dispatched:
        self.instance._task = "<pickled task chain serialization>"
        self.instance.saga_id = "[sge+ssh://example.com:22/]-[1000]"
        self.instance.state = Job.STATES.Running
        self.instance.cancel(save=False)

        self.assertTrue(mock_celery.result.from_serializable.called)
        for mock_task_result in mock_task_chain:
            mock_task_result.revoke.assert_called_once_with()
        self.assertEqual(self.instance._task, "")
        self.assertTrue(mock_service_connect.return_value.__enter__.called)
        mock_service_connection = mock_service_connect.return_value.__enter__.return_value
        mock_job_to_saga.assert_called_once_with(mock_service_connection)
        mock_job_to_saga.return_value.cancel.assert_called_once_with()
        self.assertTrue(mock_service_connect.return_value.__exit__.called)
        self.assertEqual(self.instance.state, Job.STATES.Canceled)

    @mock.patch('os.path.isdir')
    @mock.patch.object(job_model_module, 'walk')
    @mock.patch.object(job_model_module, 'zipfile')
    def test_result_zipping(self, mock_zipfile_module, mock_walk, mock_isdir):
        self.instance.saga_id = "[sge+ssh://example.com:22/]-[1000]"  # filename is based on it
        # set up some file transfers:
        self.instance.save()
        file1 = self.instance.description.files.create(remote_path="dir")
        file2 = self.instance.description.files.create(remote_path=".bashrc")
        self.instance.file_transfers.create(file=file1, result="dir", done=True)
        self.instance.file_transfers.create(file=file2, result=".bashrc", done=True)
        # configure mocks, avoiding filesystem operations:
        mock_isdir.side_effect = lambda path: 'dir' in path
        mock_walk.return_value = iter((
            ("dir", ["subdir", "empty_subdir"], ["file1", "file2"]),
            ("dir/subdir", [], ["file3"]),
            ("dir/empty_subdir", [], []),
        ))

        self.instance.zip_results(save=False)

        self.assertEqual(self.instance.zipped_results.name, "[sge+ssh@example.com:22/]-[1000].zip")

        MockZipFile = mock_zipfile_module.ZipFile
        self.assertTrue(MockZipFile.called)
        self.assertIn(self.instance.zipped_results.path, MockZipFile.call_args[0])
        self.assertIn('w', MockZipFile.call_args[0])

        self.assertTrue(MockZipFile.return_value.__enter__.called)

        result_storage = FileTransfer._meta.get_field('result').storage
        mock_isdir.assert_any_call(result_storage.path("dir"))
        mock_isdir.assert_any_call(result_storage.path(".bashrc"))
        mock_walk.assert_any_call(result_storage, "dir")
        self.assertNotIn((result_storage, ".bashrc"), mock_walk.call_args_list)

        mock_zip_file = MockZipFile.return_value.__enter__.return_value
        mock_zip_file.write.assert_any_call(result_storage.path("dir/file1"), "dir/file1")
        mock_zip_file.write.assert_any_call(result_storage.path("dir/file2"), "dir/file2")
        mock_zip_file.write.assert_any_call(result_storage.path("dir/subdir/file3"), "dir/subdir/file3")
        mock_zip_file.write.assert_any_call(result_storage.path("dir/empty_subdir"), "dir/empty_subdir")
        mock_zip_file.write.assert_any_call(result_storage.path(".bashrc"), ".bashrc")

        self.assertTrue(MockZipFile.return_value.__exit__.called)
