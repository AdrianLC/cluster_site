
import os
import stat

from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.db.models import signals
from django.test import SimpleTestCase, TestCase
from django.test.utils import override_settings

import mock

from . import TEMP_STORAGE_SETTINGS
from .. import tasks
from ..files import FileCleaner, PrivateKeyFileSystemStorage, walk
from .models import FileFieldCleanupTestModel


@override_settings(**TEMP_STORAGE_SETTINGS)
class PrivateKeyFileSystemStorageTestCase(SimpleTestCase):

    def setUp(self):
        self.storage = PrivateKeyFileSystemStorage()

    def test_specified_name_is_ignored(self):
        """The storage must assign random/unique names to the files."""
        name = "some_file_name"
        assigned_name = self.storage.save(name, ContentFile("Random content..."))
        self.assertNotEqual(assigned_name, name)
        assigned_name = self.storage.save('', ContentFile("Other random content..."))
        self.assertIsNotNone(assigned_name)

    def test_file_permissions(self):
        """Checks if the files saved have the permissions as required by OpenSSH (0600)."""
        filename = self.storage.save('', ContentFile("Whatever"))
        permissions = os.stat(self.storage.path(filename)).st_mode
        self.assertEqual(stat.S_IMODE(permissions), 0o600)


@override_settings(**TEMP_STORAGE_SETTINGS)
class FileCleanerTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        model_opts = FileFieldCleanupTestModel._meta
        cls.model = (model_opts.app_label, model_opts.model_name)

        signals.pre_save.connect(FileCleaner.on_change, sender=FileFieldCleanupTestModel)
        signals.post_delete.connect(FileCleaner.on_delete, sender=FileFieldCleanupTestModel)

    @classmethod
    def tearDownClass(cls):
        signals.pre_save.disconnect(FileCleaner.on_change, sender=FileFieldCleanupTestModel)
        signals.post_delete.disconnect(FileCleaner.on_delete, sender=FileFieldCleanupTestModel)

    def setUp(self):
        self.instance = FileFieldCleanupTestModel()

    def test_file_field_filtering(self):
        # Test returns only the FileFields:
        fields = FileCleaner.get_file_fields(FileFieldCleanupTestModel)
        self.assertNotIn(FileFieldCleanupTestModel._meta.get_field('non_file'), fields)
        self.assertItemsEqual(
            fields, [FileFieldCleanupTestModel._meta.get_field(field) for field in ('file1', 'file2')]
        )

    @mock.patch.object(tasks, 'cleanup_files', autospec=True)
    def test_file_cleanup_on_delete(self, mock_task):
        # Test it won't do anything if the FileFields are empty:
        self.instance.save()
        self.instance.delete()
        self.assertFalse(mock_task.delay.called)

        # Test it calls the task when the FileFields are set:
        self.instance.file1.save("file1", ContentFile("Random text..."), save=False)
        self.instance.file2.save("file2", ContentFile("Random text..."))
        self.instance.delete()
        mock_task.delay.assert_called_once_with(
            self.model, {'file1': self.instance.file1.name, 'file2': self.instance.file2.name}
        )
        mock_task.reset_mock()

        # Test it calls it only with the FileFields that are set:
        self.instance.file2.delete()
        self.instance.delete()
        mock_task.delay.assert_called_once_with(self.model, {'file1': self.instance.file1.name})

    @mock.patch.object(tasks, 'cleanup_files', autospec=True)
    def test_file_cleanup_on_change(self, mock_task):
        # Test it won't do anything when the FileFields are empty:
        self.instance.save()
        self.assertFalse(mock_task.delay.called)

        # Test it won't do anything if the FileFields didn't have a previous file:
        self.instance.file1.save("file1", ContentFile("Random text..."), save=False)
        self.instance.file2.save("file2", ContentFile("Random text..."))
        self.assertFalse(mock_task.delay.called)

        # Test it calls the task when files pointed by the FileFields change:
        old_file1_name = self.instance.file1.name
        old_file2_name = self.instance.file2.name
        self.instance.file1.save("new_file", ContentFile("Random text..."), save=False)
        self.instance.file2.save("new_file", ContentFile("Random text..."))
        mock_task.delay.assert_called_once_with(
            self.model, {'file1': old_file1_name, 'file2': old_file2_name}
        )
        mock_task.reset_mock()

        # Test it calls it only with those FileFields whose file has been replaced:
        old_file_name = self.instance.file1.name
        self.instance.file1.save("another_new_file", ContentFile("Random text..."))
        mock_task.delay.assert_called_once_with(self.model, {'file1': old_file_name})


class StorageWalkTestCase(TestCase):

    def setUp(self):
        self.storage = mock.create_autospec(FileSystemStorage)(location='/')
        fake_file_tree = [
            (["tmp", "bin"], ["file1", "file2"]),
            (["dir"], ["file3", "file4"]),
            ([], []),
            ([], ["file5", "file6"]),
        ]
        self.storage.listdir.side_effect = fake_file_tree

    def test_topdown(self):
        result = walk(self.storage, '/', topdown=True)
        root, dirs, files = next(result)
        self.assertEqual(root, "/")
        self.assertItemsEqual(dirs, ["tmp", "bin"])
        self.assertItemsEqual(files, ["file1", "file2"])
        root, dirs, files = next(result)
        self.assertEqual(root, "/tmp")
        self.assertItemsEqual(dirs, ["dir"])
        self.assertItemsEqual(files, ["file3", "file4"])
        root, dirs, files = next(result)
        self.assertEqual(root, "/tmp/dir")
        self.assertItemsEqual(dirs, [])
        self.assertItemsEqual(files, [])
        root, dirs, files = next(result)
        self.assertEqual(root, "/bin")
        self.assertItemsEqual(dirs, [])
        self.assertItemsEqual(files, ["file5", "file6"])
        self.assertRaises(StopIteration, next, result)
        self.storage.listdir.assert_has_calls([
            mock.call("/"),
            mock.call("/tmp"), mock.call("/tmp/dir"),
            mock.call("/bin")
        ])

    def test_downtop(self):
        result = walk(self.storage, '/', topdown=False)
        root, dirs, files = next(result)
        self.assertEqual(root, "/tmp")
        self.assertItemsEqual(dirs, ["dir"])
        self.assertItemsEqual(files, ["file3", "file4"])
        root, dirs, files = next(result)
        self.assertEqual(root, "/tmp/dir")
        self.assertItemsEqual(dirs, [])
        self.assertItemsEqual(files, [])
        root, dirs, files = next(result)
        self.assertEqual(root, "/bin")
        self.assertItemsEqual(dirs, [])
        self.assertItemsEqual(files, ["file5", "file6"])
        root, dirs, files = next(result)
        self.assertEqual(root, "/")
        self.assertItemsEqual(dirs, ["tmp", "bin"])
        self.assertItemsEqual(files, ["file1", "file2"])
        self.assertRaises(StopIteration, next, result)
        self.storage.listdir.assert_has_calls([
            mock.call("/"),
            mock.call("/tmp"), mock.call("/tmp/dir"),
            mock.call("/bin")
        ])
