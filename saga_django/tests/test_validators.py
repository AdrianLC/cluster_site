
from django.test import SimpleTestCase
from django.core.exceptions import ValidationError

import mock

from .. import validators


class HostNameValidatorTestCase(SimpleTestCase):

    def test_with_correct_hostnames(self):
        hostnames = ("google.es", "9gag.com", "en.wikipedia.com", "BING.COM", "host.net.")
        # pass the test if no ValidationErrors are raised.
        for hostname in hostnames:
            validators.full_domain(hostname)

    def test_length_validation(self):
        hostname = (("x" * 63 + ".") * 4).rstrip('.')  # longest hostname allowed.
        validators.full_domain(hostname)

        hostname = (("x" * 63 + ".") * 4)  # 1 extra '.' at the end. Too long.
        with self.assertRaises(ValidationError) as ex:
            validators.full_domain(hostname)
        self.assertEqual(
            ex.exception.messages,
            [u"The domain name cannot be composed of more than 255 characters."]
        )

        hostname = ("x" * 64) + ".com"  # 1 substring too long.
        with self.assertRaises(ValidationError) as ex:
            validators.full_domain(hostname)
        self.assertEqual(
            ex.exception.messages,
            [u"The label '" + ("x" * 64) + "' is too long (maximum is 63 characters)."]
        )

    def test_with_unallowed_label_patterns(self):
        hostname = "-google.com"
        with self.assertRaises(ValidationError) as ex:
            validators.full_domain(hostname)
        self.assertEqual(ex.exception.messages, [u"Unallowed characters in label '-google'."])

        hostname = "google-.com"
        with self.assertRaises(ValidationError) as ex:
            validators.full_domain(hostname)
        self.assertEqual(ex.exception.messages, [u"Unallowed characters in label 'google-'."])

        hostname = "bi_ng.net"
        with self.assertRaises(ValidationError) as ex:
            validators.full_domain(hostname)
        self.assertEqual(ex.exception.messages, [u"Unallowed characters in label 'bi_ng'."])


class ShellValidatorsTestCase(SimpleTestCase):

    def test_sh_identifier_validation(self):
        for identifier in ("x", "_X", "x_", "___x", "_x__y_", "A1", "A_1"):
            validators.sh_identifier(identifier)

        for identifier in ("1A", "__", "x y"):
            self.assertRaises(ValidationError, validators.sh_identifier, identifier)

    @mock.patch.object(validators, 'sh_identifier')
    def test_sh_declaration_validation(self, mock_identifier_validator):
        validators.sh_declaration(("variable1=some_value"))
        mock_identifier_validator.assert_called_once_with("variable1")

        self.assertRaises(ValidationError, validators.sh_declaration, "something_without_equal_sign")

    @mock.patch.object(validators, 'sh_declaration')
    def test_sh_declaration_list_validator(self, mock_declaration_validator):
        declarations = ("a=b", "b=a", "c='d'", "...")
        validators.sh_declaration_list(declarations)
        for dec in declarations:
            mock_declaration_validator.assert_any_call(dec)
        mock_declaration_validator.reset_mock()
        validators.sh_declaration_list('\n'.join(declarations))
        for dec in declarations:
            mock_declaration_validator.assert_any_call(dec)

    def test_file_path_validation(self):
        paths = ("/", "///", "./local", "/home/john\\ doe/.conf/", "/var/opt/\\&", "docs/pdf/some_doc.pdf")
        for path in paths:
            validators.file_path(path)

        paths = ("''", "\"\"", "&", "|", ">path", "<path", "pa&th")
        for path in paths:
            self.assertRaises(ValidationError, validators.file_path, path)
