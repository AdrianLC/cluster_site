
from unittest import skipUnless

from django.conf import settings
from django.contrib.sites.models import Site
from django.test import TestCase
from django.test.utils import override_settings

from .. import utils


class UtilsTestCase(TestCase):

    @override_settings(SITE_NAME="XXXX")
    def test_get_current_site_from_settings(self):
        with self.assertNumQueries(0):
            self.assertEqual(utils.get_current_site_name(), settings.SITE_NAME)

    @override_settings(SITE_NAME=None)
    @skipUnless('django.contrib.sites' in settings.INSTALLED_APPS,
                "'django.contrib.sites' not installed")
    def test_get_current_site_from_DB(self):
        with self.assertNumQueries(1):
            self.assertEqual(utils.get_current_site_name(), Site.objects.get_current().name)
