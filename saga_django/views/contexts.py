
from django.core.urlresolvers import reverse_lazy
from django.views import generic

from guardian.mixins import LoginRequiredMixin

from ..forms import SSHContextForm
from ..models import SSHContext
from .mixins import AccessRequiredMixin, FilterByAccessMixin, UserFormKwargMixin


class SSHContextCreateView(LoginRequiredMixin, UserFormKwargMixin, generic.CreateView):
    model = SSHContext
    form_class = SSHContextForm
    # success_url -> use default object.get_absolute_url()
    template_name = "saga_django/contexts/sshcontext_create_form.html"

    def form_valid(self, form):
        response = super(SSHContextCreateView, self).form_valid(form)
        # assign permission over the parent object as well, for select_subclasses() queries
        self.object.context_ptr.grant_access(self.request.user)
        return response


class SSHContextDetailView(AccessRequiredMixin, generic.DetailView):
    model = SSHContext
    template_name = "saga_django/contexts/sshcontext_detail.html"


class SSHContextDeleteView(AccessRequiredMixin, generic.DeleteView):
    model = SSHContext
    success_url = reverse_lazy('list_ssh_contexts')
    template_name = "saga_django/contexts/sshcontext_confirm_delete.html"


class SSHContextListView(FilterByAccessMixin, generic.ListView):
    model = SSHContext
    template_name = "saga_django/contexts/sshcontext_list.html"


class SSHContextUpdateView(AccessRequiredMixin, UserFormKwargMixin, generic.UpdateView):
    model = SSHContext
    form_class = SSHContextForm
    success_url = reverse_lazy('list_ssh_contexts')
    template_name = "saga_django/contexts/sshcontext_update_form.html"
