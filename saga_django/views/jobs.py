
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.humanize.templatetags.humanize import naturaltime
from django.db import transaction, DatabaseError
from django.forms.models import inlineformset_factory
from django.http import HttpResponseRedirect
from django.views import generic
from django.templatetags.l10n import localize
from django.utils.translation import ugettext_lazy as _

from braces.views import AjaxResponseMixin, JSONResponseMixin
from django_downloadview.views import ObjectDownloadView
from guardian.mixins import LoginRequiredMixin

from ..forms import (
    JobServiceForm,
    JobDescriptionForm,
    JobFilesInlineFormSet, FileDownloadForm, FileUploadForm,
    JobForm,
    GrantAccessInlineFormSet,
)
from ..models import JobService, JobDescription, JobFile, Job
from ..tasks import try_job_service, cancel_job
from .mixins import AccessRequiredMixin, FilterByAccessMixin, UserFormKwargMixin


############################## JobService views ###############################

class JobServiceFormViewBase(UserFormKwargMixin):
    model = JobService
    form_class = JobServiceForm

    def form_valid(self, form):
        """Save service and launch test task."""
        self.object = job_service = form.save()
        self.test_task = try_job_service.delay(job_service.pk)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('wait_test_job_service', args=[self.object.pk, self.test_task.id])


class JobServiceCreateView(LoginRequiredMixin, JobServiceFormViewBase, generic.CreateView):
    template_name = "saga_django/jobservice_create_form.html"


class JobServiceUpdateView(AccessRequiredMixin, JobServiceFormViewBase, generic.UpdateView):
    template_name = "saga_django/jobservice_update_form.html"


class JobServiceTestWaitView(AccessRequiredMixin, AjaxResponseMixin, JSONResponseMixin,
                             generic.DetailView):
    model = JobService
    template_name = "saga_django/jobservice_wait_test.html"

    messages = {
        'success': _("New job service has been correctly set up."),
        # TODO: Find out if Saga's job.Service constructor raises more exceptions.
        'errors': {
            'AuthorizationFailed': _("Authentication refused by host. Please, check if the "
                                     "necessary context(s) are selected and correctly set up."),
            'AuthenticationFailed': _("Authentication refused by host. Please, check if the "
                                      "necessary context(s) are selected and correctly set up."),
            'PermissionDenied': _("Authentication refused by host. Please, check if the "
                                  "necessary context(s) are selected and correctly set up."),
            'Timeout': _("Timed out when stablishing a connection with the host. Network might "
                         "be too busy."),
            'NoSuccess': _("Unknown connection error. Wrong host or port?"),
            'UnknownError': _("An unknown error has been encountered when testing the service. "
                              "Please, try again later or contact administrators of this site."),
        }
    }

    def get(self, request, *args, **kwargs):
        finished, error = self.get_task_result()
        if finished:
            self.attach_message(error)
            return HttpResponseRedirect(self.get_redirect_url(error))
        self.object = self.get_object()
        context = self.get_context_data(object=self.object, task_id=self.kwargs['task_id'])
        return self.render_to_response(context)

    def get_ajax(self, request, *args, **kwargs):
        finished, error = self.get_task_result()
        if finished:
            self.attach_message(error)
            return self.render_json_response({'redirect': self.get_redirect_url(error)})
        return self.render_json_response(None)

    def get_task_result(self, task_id=None):
        task_id = task_id or self.kwargs['task_id']
        test_result = try_job_service.AsyncResult(task_id)
        if test_result.ready():
            return True, test_result.get()
        return False, None

    def attach_message(self, error):
        if error:
            level = messages.ERROR
            message = self.messages['errors'].get(error, self.messages['errors']['UnknownError'])
        else:
            level = messages.SUCCESS
            message = self.messages['success']
        messages.add_message(self.request, level, message)

    def get_redirect_url(self, error):
        if error:
            return reverse('update_job_service', kwargs={'pk': self.kwargs.get(self.pk_url_kwarg)})
        return reverse('list_job_services')


class JobServiceDetailView(AccessRequiredMixin, generic.DetailView):
    model = JobService
    template_name = "saga_django/jobservice_detail.html"


class JobServiceDeleteView(AccessRequiredMixin, generic.DeleteView):
    model = JobService
    template_name = "saga_django/jobservice_confirm_delete.html"
    success_url = reverse_lazy('list_job_services')


class JobServiceListView(FilterByAccessMixin, generic.ListView):
    model = JobService
    template_name = "saga_django/jobservice_list.html"


############################ JobDescription views #############################

class JobDescriptionFormBase(UserFormKwargMixin):
    model = JobDescription
    form_class = JobDescriptionForm
    prefix = 'job_description'
    # form prefix -> form class dict:
    inline_classes = {
        # to be filled at runtime with inlineformset_factory:
        'job_files': None,
        'file_uploads': None,
        'file_downloads': None,
        'job_executions': None,
    }
    success_url = reverse_lazy('list_job_descriptions')

    def get_context_data(self, **kwargs):
        if 'inlines' not in kwargs:
            inline_classes = self.get_inline_classes()
            inlines = self.get_inlines(inline_classes)
            kwargs['inlines'] = inlines
        return super(JobDescriptionFormBase, self).get_context_data(**kwargs)

    def form_valid(self, form):
        transaction.set_autocommit(False)
        try:

            self.object = form.save()
            inline_classes = self.get_inline_classes()
            inlines = self.get_inlines(inline_classes)
            for prefix, inline in inlines.items():
                if inline.is_valid():
                    inline._saved_instances = inline.save()
                else:
                    transaction.rollback()
                    return self.inlines_invalid(form, inlines)
            transaction.commit()
            transaction.set_autocommit(True)
            return self.inlines_valid(form, inlines)

        except DatabaseError:
            transaction.rollback()
            raise
        finally:
            transaction.set_autocommit(True)

    def inlines_valid(self, form, inlines):
        for job in inlines['job_executions']._saved_instances:
            if not job.sent:
                job.handle_async()
        messages.success(self.request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())

    def inlines_invalid(self, form, inlines):
        context = {'form': form, 'inlines': inlines}
        return self.render_to_response(self.get_context_data(**context))

    def get_inlines(self, form_classes):
        kwargs = self.get_form_kwargs()
        forms = {}
        for prefix, form_class in form_classes.items():
            kwargs['prefix'] = prefix
            forms[prefix] = form_class(**kwargs)
        return forms

    def get_inline_classes(self):
        def restrict_to_allowed_services(field, **kwargs):
            if field.name == 'service':
                kwargs.update({'queryset': JobService.objects.accessible_by(self.request.user)})
            return field.formfield(**kwargs)

        self.inline_classes.update({
            'job_files': inlineformset_factory(
                JobDescription, JobFile,
                form=FileDownloadForm, formset=JobFilesInlineFormSet,
                can_delete=False,
            ),
            'file_uploads': inlineformset_factory(
                JobDescription, JobFile,
                form=FileUploadForm, formset=GrantAccessInlineFormSet,
                extra=int(self.request.GET.get('num_uploads', 0)),
                can_delete=bool(self.object),
            ),
            'file_downloads': inlineformset_factory(
                JobDescription, JobFile,
                form=FileDownloadForm, formset=GrantAccessInlineFormSet,
                extra=int(self.request.GET.get('num_downloads', 0)),
                can_delete=bool(self.object),
            ),
            'job_executions': inlineformset_factory(
                JobDescription, Job,
                form=JobForm, formset=GrantAccessInlineFormSet,
                formfield_callback=restrict_to_allowed_services,
                extra=int(self.request.GET.get('num_executions', 0)),
                can_delete=False,
            ),
        })
        return self.inline_classes


class JobDescriptionCreateView(LoginRequiredMixin, JobDescriptionFormBase, generic.CreateView):
    template_name = "saga_django/jobdescription_create_form.html"
    success_message = _("Job has been successfully created.")


class JobDescriptionUpdateView(AccessRequiredMixin, JobDescriptionFormBase, generic.UpdateView):
    template_name = "saga_django/jobdescription_update_form.html"
    success_message = _("Job has been successfully updated.")
    cloned_message = _("Jobs with executions are not editable. "
                       "A copy of the description has been created instead.")

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.executions.exists():
            job_clone = self.clone_job(self.object)
            messages.info(request, self.cloned_message)
            return HttpResponseRedirect(reverse('update_job_description', args=[job_clone.pk]))
        return super(JobDescriptionUpdateView, self).dispatch(request, *args, **kwargs)

    def clone_job(self, job):
        # TODO: Optimizar queries
        original_files = job.files.all()
        job.pk = None
        if not job.name.endswith("_copy"):
            job.name += "_copy"
        job.save()
        job.grant_access(self.request.user)
        for job_file in original_files:
            job_file.pk = None
            job_file.job_description = job
            job_file.save()
            job_file.grant_access(self.request.user)
        return job

    def get_inlines(self, form_classes):
        inlines = super(JobDescriptionUpdateView, self).get_inlines(form_classes)
        inlines['job_files'].queryset = self.object.files.exclude(peculiarity=None)
        inlines['file_downloads'].queryset = self.object.files.downloads().filter(peculiarity=None)
        inlines['file_uploads'].queryset = self.object.files.uploads().filter(peculiarity=None)
        inlines['job_executions'].queryset = Job.objects.none()
        return inlines


class JobDescriptionDeleteView(AccessRequiredMixin, generic.DeleteView):
    model = JobDescription
    template_name = "saga_django/jobdescription_confirm_delete.html"
    success_url = reverse_lazy('list_job_descriptions')


class JobDescriptionDetailView(AccessRequiredMixin, generic.DetailView):
    model = JobDescription
    template_name = "saga_django/jobdescription_detail.html"


class JobDescriptionListView(FilterByAccessMixin, generic.ListView):
    model = JobDescription
    template_name = "saga_django/jobdescription_list.html"


################################## Job views ##################################


class JobCreateView(LoginRequiredMixin, UserFormKwargMixin, generic.CreateView):
    model = Job
    form_class = JobForm
    template_name = "saga_django/job_create_form.html"
    success_url = reverse_lazy('list_jobs')

    def form_valid(self, form):
        job = self.object = form.save()
        job.handle_async()
        return HttpResponseRedirect(self.get_success_url())


class JobCancelView(AccessRequiredMixin, generic.DeleteView):
    model = Job
    template_name = "saga_django/job_confirm_cancel.html"
    success_url = reverse_lazy('list_jobs')
    success_message = _("Job has been successfully canceled.")

    def delete(self, request, *args, **kwargs):
        job = self.object = self.get_object()
        cancel_job.delay(job.pk)
        messages.success(request, self.success_message)
        return HttpResponseRedirect(self.get_success_url())


class JobDetailView(AccessRequiredMixin, generic.DetailView):
    model = Job
    template_name = "saga_django/job_detail.html"


class JobListView(FilterByAccessMixin, AjaxResponseMixin, JSONResponseMixin, generic.ListView):
    model = Job
    template_name = "saga_django/job_list.html"

    def get_ajax(self, request, *args, **kwargs):
        self.object_list = jobs = self.get_queryset()
        job_data = self.format_data(jobs)
        return self.render_json_response(job_data)

    def format_data(self, jobs):
        data = []
        for job in jobs.select_related():
            data.append({
                'pk': job.pk,
                'description': {
                    'pk': job.description.pk,
                    '__str__': unicode(job.description),
                    'get_absolute_url': job.description.get_absolute_url()
                },
                'service': {
                    'pk': job.service.pk,
                    '__str__': unicode(job.service),
                    'get_absolute_url': job.service.get_absolute_url()
                },
                'dispatch_time': localize(job.dispatch_time) or unicode(_("Pending")),
                'state': job.get_state_display().capitalize(),
                'last_state_change': naturaltime(job.last_state_change),
                'exit_code': unicode(_("None")) if job.exit_code is None else job.exit_code,
                'user_action': self._get_user_action_link(job)
            })
        return data

    def _get_user_action_link(self, job):
        if job.state == Job.STATES.Canceled:
            return
        elif job.state == Job.STATES.Done:
            return (reverse('download_results', args=[job.pk]), unicode(_("Download results")))
        else:
            return (reverse('cancel_job', args=[job.pk]), unicode(_("Cancel")))


class DownloadJobResultsView(AccessRequiredMixin, ObjectDownloadView):
    model = Job
    file_field = 'zipped_results'

    # Temporary fix to avoid url-encoding twice:
    def get_file(self):
        file_ = super(DownloadJobResultsView, self).get_file()
        return file_.file
