
from collections import Iterable

from django.core.exceptions import ImproperlyConfigured

from guardian.mixins import PermissionRequiredMixin
from guardian.shortcuts import get_objects_for_user
from guardian.utils import get_anonymous_user


class AccessRequiredMixin(PermissionRequiredMixin):
    permission_required = 'can_access'


class PermissionMixin(object):

    def get_permissions(self, permissions=None):
        """
        Returns list of permissions in format *<app_label>.<codename>* that
        should be used with *request.user* and *object*. By default, it
        returns list from ``permission_required`` attribute.
        """
        permissions = permissions or self.permission_required
        if isinstance(permissions, basestring):
            permissions = [permissions]
        elif isinstance(permissions, Iterable):
            permissions = [p for p in permissions]
        else:
            raise ImproperlyConfigured(
                "'PermissionMixin' requires 'permission_required' attribute to be set to "
                "'<app_label>.<permission codename>' but is set to '%s' instead" % self.permissions
            )
        return permissions


class FilterByPermissionMixin(PermissionMixin):
    permission_required = None

    def get_queryset(self):
        user = self.request.user if self.request.user.is_authenticated() else get_anonymous_user()
        perms = self.get_permissions()
        queryset = super(FilterByPermissionMixin, self).get_queryset()
        return get_objects_for_user(user, perms, queryset)


class FilterByAccessMixin(FilterByPermissionMixin):
    permission_required = 'can_access'


class UserFormKwargMixin(object):

    def get_form_kwargs(self):
        kwargs = super(UserFormKwargMixin, self).get_form_kwargs()
        kwargs['user'] = self.request.user or get_anonymous_user()
        return kwargs
