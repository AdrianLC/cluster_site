# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Context'
        db.create_table(u'saga_django_context', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('saga_django', ['Context'])

        # Adding model 'SSHContext'
        db.create_table(u'saga_django_sshcontext', (
            (u'context_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['saga_django.Context'], unique=True, primary_key=True)),
            ('alias', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('user_id', self.gf('django.db.models.fields.CharField')(max_length=108)),
            ('user_pass', self.gf('django.db.models.fields.CharField')(default=u'T6iCjEDM4qfRf6hgyY69SziS3z4aMM', max_length=108)),
            ('user_cert', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('user_key', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('key_fingerprint', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('public_key', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('saga_django', ['SSHContext'])

        # Adding model 'JobFile'
        db.create_table(u'saga_django_jobfile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('job_description', self.gf('django.db.models.fields.related.ForeignKey')(related_name='files', to=orm['saga_django.JobDescription'])),
            ('action', self.gf('django.db.models.fields.SmallIntegerField')(default=2)),
            ('peculiarity', self.gf('django.db.models.fields.SmallIntegerField')(null=True)),
            ('local', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('remote_path', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal('saga_django', ['JobFile'])

        # Adding unique constraint on 'JobFile', fields ['job_description', 'remote_path', 'action']
        db.create_unique(u'saga_django_jobfile', ['job_description_id', 'remote_path', 'action'])

        # Adding unique constraint on 'JobFile', fields ['job_description', 'peculiarity']
        db.create_unique(u'saga_django_jobfile', ['job_description_id', 'peculiarity'])

        # Adding model 'FileTransfer'
        db.create_table(u'saga_django_filetransfer', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('job', self.gf('django.db.models.fields.related.ForeignKey')(related_name='file_transfers', to=orm['saga_django.Job'])),
            ('file', self.gf('django.db.models.fields.related.ForeignKey')(related_name='transfers', to=orm['saga_django.JobFile'])),
            ('done', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('result', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('saga_django', ['FileTransfer'])

        # Adding unique constraint on 'FileTransfer', fields ['job', 'file']
        db.create_unique(u'saga_django_filetransfer', ['job_id', 'file_id'])

        # Adding model 'JobService'
        db.create_table(u'saga_django_jobservice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('scheme', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('host', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('port', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=22)),
            ('works', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('last_attempt', self.gf('django.db.models.fields.DateTimeField')(null=True)),
        ))
        db.send_create_signal('saga_django', ['JobService'])

        # Adding M2M table for field contexts on 'JobService'
        m2m_table_name = db.shorten_name(u'saga_django_jobservice_contexts')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('jobservice', models.ForeignKey(orm['saga_django.jobservice'], null=False)),
            ('context', models.ForeignKey(orm['saga_django.context'], null=False))
        ))
        db.create_unique(m2m_table_name, ['jobservice_id', 'context_id'])

        # Adding model 'JobDescription'
        db.create_table(u'saga_django_jobdescription', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=15, blank=True)),
            ('executable', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('arguments', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('working_directory', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('environment', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('queue', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('project', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('total_cpu_count', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('wall_time_limit', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=0)),
        ))
        db.send_create_signal('saga_django', ['JobDescription'])

        # Adding model 'Job'
        db.create_table(u'saga_django_job', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('service', self.gf('django.db.models.fields.related.ForeignKey')(related_name='executions', to=orm['saga_django.JobService'])),
            ('description', self.gf('django.db.models.fields.related.ForeignKey')(related_name='executions', to=orm['saga_django.JobDescription'])),
            ('dispatch_time', self.gf('django.db.models.fields.DateTimeField')(null=True)),
            ('saga_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('state', self.gf('django.db.models.fields.CharField')(default='Unknown', max_length=10)),
            ('last_state_change', self.gf('model_utils.fields.MonitorField')(default=datetime.datetime.now, null=True, monitor='state')),
            ('exit_code', self.gf('django.db.models.fields.SmallIntegerField')(null=True)),
            ('zipped_results', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('_task', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('saga_django', ['Job'])


    def backwards(self, orm):
        # Removing unique constraint on 'FileTransfer', fields ['job', 'file']
        db.delete_unique(u'saga_django_filetransfer', ['job_id', 'file_id'])

        # Removing unique constraint on 'JobFile', fields ['job_description', 'peculiarity']
        db.delete_unique(u'saga_django_jobfile', ['job_description_id', 'peculiarity'])

        # Removing unique constraint on 'JobFile', fields ['job_description', 'remote_path', 'action']
        db.delete_unique(u'saga_django_jobfile', ['job_description_id', 'remote_path', 'action'])

        # Deleting model 'Context'
        db.delete_table(u'saga_django_context')

        # Deleting model 'SSHContext'
        db.delete_table(u'saga_django_sshcontext')

        # Deleting model 'JobFile'
        db.delete_table(u'saga_django_jobfile')

        # Deleting model 'FileTransfer'
        db.delete_table(u'saga_django_filetransfer')

        # Deleting model 'JobService'
        db.delete_table(u'saga_django_jobservice')

        # Removing M2M table for field contexts on 'JobService'
        db.delete_table(db.shorten_name(u'saga_django_jobservice_contexts'))

        # Deleting model 'JobDescription'
        db.delete_table(u'saga_django_jobdescription')

        # Deleting model 'Job'
        db.delete_table(u'saga_django_job')


    models = {
        'saga_django.context': {
            'Meta': {'object_name': 'Context'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'saga_django.filetransfer': {
            'Meta': {'unique_together': "(('job', 'file'),)", 'object_name': 'FileTransfer'},
            'done': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'file': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transfers'", 'to': "orm['saga_django.JobFile']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'file_transfers'", 'to': "orm['saga_django.Job']"}),
            'result': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        },
        'saga_django.job': {
            'Meta': {'object_name': 'Job'},
            '_task': ('django.db.models.fields.TextField', [], {}),
            'description': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'executions'", 'to': "orm['saga_django.JobDescription']"}),
            'dispatch_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'exit_code': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_state_change': ('model_utils.fields.MonitorField', [], {'default': 'datetime.datetime.now', 'null': 'True', u'monitor': "'state'"}),
            'saga_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'service': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'executions'", 'to': "orm['saga_django.JobService']"}),
            'state': ('django.db.models.fields.CharField', [], {'default': "'Unknown'", 'max_length': '10'}),
            'zipped_results': ('django.db.models.fields.files.FileField', [], {'max_length': '100'})
        },
        'saga_django.jobdescription': {
            'Meta': {'object_name': 'JobDescription'},
            'arguments': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'environment': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'executable': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'}),
            'project': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'queue': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'total_cpu_count': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'wall_time_limit': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'working_directory': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'saga_django.jobfile': {
            'Meta': {'unique_together': "(('job_description', 'remote_path', 'action'), ('job_description', 'peculiarity'))", 'object_name': 'JobFile'},
            'action': ('django.db.models.fields.SmallIntegerField', [], {'default': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'job_description': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'files'", 'to': "orm['saga_django.JobDescription']"}),
            'local': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'peculiarity': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True'}),
            'remote_path': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        'saga_django.jobservice': {
            'Meta': {'object_name': 'JobService'},
            'contexts': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['saga_django.Context']", 'symmetrical': 'False'}),
            'host': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'jobs': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['saga_django.JobDescription']", 'through': "orm['saga_django.Job']", 'symmetrical': 'False'}),
            'last_attempt': ('django.db.models.fields.DateTimeField', [], {'null': 'True'}),
            'port': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '22'}),
            'scheme': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'works': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'saga_django.sshcontext': {
            'Meta': {'object_name': 'SSHContext', '_ormbases': ['saga_django.Context']},
            'alias': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'context_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['saga_django.Context']", 'unique': 'True', 'primary_key': 'True'}),
            'key_fingerprint': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'public_key': ('django.db.models.fields.TextField', [], {}),
            'user_cert': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'user_id': ('django.db.models.fields.CharField', [], {'max_length': '108'}),
            'user_key': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'user_pass': ('django.db.models.fields.CharField', [], {'default': "u'gmq2hQpbpMBQyowWJoIPYx1vS53biH'", 'max_length': '108'})
        }
    }

    complete_apps = ['saga_django']