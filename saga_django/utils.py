
from django.conf import settings
from django.contrib.sites.models import Site


def get_current_site_name():
    return getattr(settings, 'SITE_NAME', None) or Site.objects.get_current().name
