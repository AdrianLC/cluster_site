
from django.conf.urls import patterns, url

from .views.contexts import (
    SSHContextCreateView, SSHContextDetailView,
    SSHContextUpdateView, SSHContextDeleteView,
    SSHContextListView,
)
from .views.jobs import (
    JobServiceCreateView, JobServiceTestWaitView,
    JobServiceDetailView, JobServiceListView,
    JobServiceUpdateView, JobServiceDeleteView,

    JobDescriptionCreateView, JobDescriptionDetailView,
    JobDescriptionUpdateView, JobDescriptionDeleteView,
    JobDescriptionListView,

    JobCreateView, JobDetailView,
    JobCancelView, JobListView,
    DownloadJobResultsView,
)


_task_pattern = r'(?P<task_id>[\w\d\-\.]+)'

urlpatterns = patterns('',
    url(r'^context/ssh/create/$', SSHContextCreateView.as_view(), name='create_ssh_context'),
    url(r'^context/ssh/detail/(?P<pk>\d+)/$', SSHContextDetailView.as_view(), name='detail_ssh_context'),
    url(r'^context/ssh/update/(?P<pk>\d+)/$', SSHContextUpdateView.as_view(), name='update_ssh_context'),
    url(r'^context/ssh/delete/(?P<pk>\d+)/$', SSHContextDeleteView.as_view(), name='delete_ssh_context'),
    url(r'^context/ssh/list/$', SSHContextListView.as_view(), name='list_ssh_contexts'),

    url(r'^service/register/$', JobServiceCreateView.as_view(), name='register_job_service'),
    url(r'^service/register/wait/(?P<pk>\d+)/%s/$' % _task_pattern, JobServiceTestWaitView.as_view(),
        name='wait_test_job_service'),
    url(r'^service/detail/(?P<pk>\d+)/$', JobServiceDetailView.as_view(), name='detail_job_service'),
    url(r'^service/update/(?P<pk>\d+)/$', JobServiceUpdateView.as_view(), name='update_job_service'),
    url(r'^service/delete/(?P<pk>\d+)/$', JobServiceDeleteView.as_view(), name='delete_job_service'),
    url(r'^service/list/$', JobServiceListView.as_view(), name='list_job_services'),

    url(r'^job/create/$', JobDescriptionCreateView.as_view(), name='create_job_description'),
    url(r'^job/detail/(?P<pk>\d+)/$', JobDescriptionDetailView.as_view(), name='detail_job_description'),
    url(r'^job/update/(?P<pk>\d+)/$', JobDescriptionUpdateView.as_view(), name='update_job_description'),
    url(r'^job/delete/(?P<pk>\d+)/$', JobDescriptionDeleteView.as_view(), name='delete_job_description'),
    url(r'^job/list/$', JobDescriptionListView.as_view(), name='list_job_descriptions'),

    url(r'^execution/create/$', JobCreateView.as_view(), name='create_job'),
    url(r'^execution/detail/(?P<pk>\d+)/$', JobDetailView.as_view(), name='detail_job'),
    url(r'^execution/cancel/(?P<pk>\d+)/$', JobCancelView.as_view(), name='cancel_job'),
    url(r'^execution/list/$', JobListView.as_view(), name='list_jobs'),
    url(r'^execution/(?P<pk>\d+)/results/$', DownloadJobResultsView.as_view(), name='download_results'),
)
