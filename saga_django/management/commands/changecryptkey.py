"""
A django management command which changes the primary encryption key at the
keyczar http://www.keyczar.org key directory in the location specified in
settings.ENCRYPTED_FIELD_KEYS_DIR.
"""

import os

from django.conf import settings as django_settings, ImproperlyConfigured
from django.core.management.base import NoArgsCommand

from keyczar import keyczart
from keyczar.errors import KeyczarError
from keyczar.keyinfo import PRIMARY, DECRYPT_AND_ENCRYPT

from ... import settings


class Command(NoArgsCommand):

    help = ("Changes the primary key of the keyczar key directory in the location specified in"
            "settings.ENCRYPTED_FIELD_KEYS_DIR.")

    requires_model_validation = False

    try:
        loc = django_settings.ENCRYPTED_FIELD_KEYS_DIR
    except AttributeError:
        raise ImproperlyConfigured("'ENCRYPTED_FIELD_KEYS_DIR' must be set.")

    key_size = settings.ENCRYPTED_FIELD_KEYS_SIZE

    def __init__(self, *args, **kwargs):
        super(Command, self).__init__(*args, **kwargs)

    def handle_noargs(self, **options):
        try:
            try:
                keyczart.AddKey(loc=self.loc, status=PRIMARY, size=self.key_size)
            except KeyczarError as ex:
                if not os.path.exists(self.loc):
                    os.makedirs(self.loc)
                keyczart.Create(loc=self.loc, purpose=DECRYPT_AND_ENCRYPT, name='saga_django')
                keyczart.AddKey(loc=self.loc, status=PRIMARY, size=self.key_size)
        except KeyczarError as ex:
            self.stderr.write(str(ex))
        else:
            self.stdout.write("Primary encryption key has been successfully stablished.")
