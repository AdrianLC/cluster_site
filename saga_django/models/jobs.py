
from contextlib import contextmanager
import logging
import os.path
import posixpath
import pickle
import zipfile

from django.core.validators import MinValueValidator, RegexValidator
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

import celery
from model_utils.choices import Choices
from model_utils.fields import MonitorField
import saga

from .. import validators
from .. import settings
from ..files import default_private_storage, walk
from ..settings import ENABLED_JOB_SERVICE_ADAPTORS
from .base import SagaModel
from .contexts import Context
from .files import JobFile


logger = logging.getLogger(__name__)


class JobService(SagaModel):

    _help_texts = {
        'scheme': _("Job service type. I.e. resource management solution."),
        'host': _("Hostname or IP of the remote system."),
        'port': _("Port used by the remote SSH server."),
        'contexts': _("Credentials for authentication with the remote system. One should be enough."),
        'last_attempt': _("Time of the last failed connection.")
    }

    SCHEMES = Choices(*filter(
        lambda choice: choice[0] in ENABLED_JOB_SERVICE_ADAPTORS,
        (
            ('ssh', 'ssh', "SSH"),
            ('pbs+ssh', 'pbs_ssh', "PBS"),
            ('sge+ssh', 'sge_ssh', "SGE"),
        )
    ))
    scheme = models.CharField(
        _("type"), choices=SCHEMES, max_length=10,
        help_text=_help_texts['scheme']
    )
    host = models.CharField(
        _("host"), max_length=255,
        validators=[validators.full_domain],
        help_text=_help_texts['host']
    )
    port = models.PositiveSmallIntegerField(
        _("port"), default=22,
        help_text=_help_texts['port']
    )
    contexts = models.ManyToManyField(
        Context, verbose_name=_("contexts"),
        help_text=_help_texts['contexts']
    )
    jobs = models.ManyToManyField('JobDescription', through='Job', verbose_name=_("jobs"))
    works = models.BooleanField(_("functional?"), default=False)
    last_attempt = models.DateTimeField(
        _("last attempt"), null=True,
        help_text=_help_texts['last_attempt']
    )

    class Meta(SagaModel.Meta):
        verbose_name = _("job service")
        verbose_name_plural = _("job services")

    def __unicode__(self):
        if self.scheme and self.host:
            string = "%s @ %s" % (self.get_scheme_display(), self.host)
            if self.port:
                string += ':' + str(self.port)
            return string
        return "%s object" % self.__class__.__name__

    def get_absolute_url(self):
        return reverse('detail_job_service', args=[self.pk])

    def to_saga(self):
        session = saga.Session(default=False)  # don't load default contexts
        for context in self.contexts.select_subclasses().all():
            session.add_context(context.to_saga())
        return saga.job.Service("%s://%s:%s" % (self.scheme, self.host, self.port), session=session)

    @contextmanager
    def connect(self, save=True):
        """Ensure that the saga.job.JobService is closed and keep track of service failures."""
        try:
            self.works = False
            saga_job_service = self.to_saga()
            self.works = True
            yield saga_job_service
        except saga.SagaException:
            logger.debug("Job service '%s' failed." % self, exc_info=True)
            self.last_attempt = now()
            raise
        finally:
            try:
                saga_job_service.close()
            except Exception:
                pass
            if save:
                self.save()


class JobDescription(SagaModel):

    _help_texts = {
        'name': _("You can assign a name for this job in the cluster."),
        'executable': _("The starting point of your job. You may use an executable already available "
                        "in the server or you can upload your own script below."),
        'arguments': _("A list of arguments just like it is done in a terminal. <br/>"
                       "E.g. \"-la -c --argument1=arg -debug 0 ...\""),
        'working_directory': _("Path of the remote directory used for execution."),
        'environment': _("A list of environment declarations. Use one line for each. <br/>"
                         "E.g. \"CLASSPATH=/opt/java/\"."),
        'queue': _("Specify the job queue if needed. For example \"batch\"."),
        'project': _("The project or execution group this task belong to."),
        'total_cpu_count': _("Number of CPUs requested for execution."),
        'wall_time_limit': _("Hard time limit. In minutes.")
    }

    name = models.CharField(
        _("name"), max_length=15, blank=True,
        validators=[RegexValidator(r'^[a-zA-Z]\S*$')],
        help_text=_help_texts['name']
    )
    executable = models.CharField(
        _("executable"), max_length=50,
        help_text=_help_texts['executable']
    )
    arguments = models.CharField(
        _("arguments"), max_length=255, blank=True,
        help_text=_help_texts['arguments']
    )
    working_directory = models.CharField(
        _("working directory"), max_length=255,
        # blank=True,  must be required, saga's sftp cant handle relative paths yet
        validators=[validators.file_path],
        help_text=_help_texts['working_directory']
    )
    environment = models.TextField(
        _("environment variables"), blank=True,
        validators=[validators.sh_declaration_list],
        help_text=_help_texts['environment']
    )
    queue = models.CharField(
        _("queue"), max_length=50, blank=True,
        help_text=_help_texts['queue']
    )
    project = models.CharField(
        _("project"), max_length=100, blank=True,
        help_text=_help_texts['project']
    )  # account string
    total_cpu_count = models.PositiveSmallIntegerField(
        _("nodes"), default=1,
        validators=[MinValueValidator(1)],
        help_text=_help_texts['total_cpu_count']
    )
    wall_time_limit = models.PositiveSmallIntegerField(
        _("time limit"), default=0,
        help_text=_help_texts['wall_time_limit']
    )  # minutes

    # Some other attributes not supported/working with the PBS adaptor:
        # input
        # spmd_variation
        # number_of_processes
        # processes_per_host
        # threads_per_process
        # cleaup
        # job_start_time
        # total_physical_memory
        # cpu_architecture
        # operating_system_type
        # candidate_hosts

    class Meta(SagaModel.Meta):
        verbose_name = _("job description")
        verbose_name_plural = _("job description")

    def __unicode__(self):
        if self.executable:
            parts = [self.executable, self.arguments]
            if self.name and self.name != self.executable:
                parts.insert(0, "(%s)" % self.name)
            return " ".join(parts).strip()
        return "%s object" % self.__class__.__name__

    def save(self, *args, **kwargs):
        self.name = self.name or self.executable
        super(JobDescription, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('detail_job_description', args=[self.pk])

    def environment_as_dict(self):
        env = {}
        if not self.environment:
            return env
        declarations = filter(None, (line.strip() for line in self.environment.splitlines()))
        for declaration in declarations:
            identifier, value = (component.strip() for component in declaration.split('=', 1))
            env[identifier] = value
        return env

    @property
    def output(self):
        return self.files.get(peculiarity=JobFile.PECULIAR_FILES.output)

    @property
    def error(self):
        return self.files.get(peculiarity=JobFile.PECULIAR_FILES.error)

    def to_saga(self):
        job_description = saga.job.Description()
        for attribute in (
            'name', 'executable', 'working_directory',
            'queue', 'project',
            'total_cpu_count', 'wall_time_limit'
        ):
            value = getattr(self, attribute)
            if value:  # -> do not remove this check, Saga can't handle empty not None values
                setattr(job_description, attribute, value)
        # pass all the arguments as arguments[0], avoiding Saga's string to list conversion
        # (it uses str.split() without considering quotes, parenthesis, or anything)
        if self.arguments:
            job_description.arguments = [self.arguments]
        # convert the env declarations in string format to a python dict
        if self.environment:
            job_description.environment = self.environment_as_dict()
        for job_file in ('output', 'error'):
            setattr(job_description, job_file, getattr(self, job_file).remote_path)
        return job_description


class Job(SagaModel):

    _help_texts = {
        'dispatch_time': _("Time at which the execution was dispatched to the remote service."),
        'saga_id': _("Internal SAGA ID."),
        'state': _("Remote execution status."),
        'last_state_change': _("Time of last state change."),
        'zipped_results': _("Zip file containing all the files product of the execution.")
    }

    STATES = Choices(
        # Direct assignment of Saga constants:
        (saga.job.constants.UNKNOWN, _("unknown")),
        (saga.job.constants.NEW, _("new")),
        (saga.job.constants.PENDING, _("pending")),
        (saga.job.constants.RUNNING, _("running")),
        (saga.job.constants.SUSPENDED, _("suspended")),
        (saga.job.constants.DONE, _("done")),
        (saga.job.constants.FAILED, _("failed")),
        (saga.job.constants.CANCELED, _("canceled")),
    )
    FINISHED_STATES = (STATES.Done, STATES.Failed, STATES.Canceled)

    service = models.ForeignKey(
        JobService, verbose_name=_("service"),
        related_name='executions'
    )
    description = models.ForeignKey(
        JobDescription, verbose_name=_("description"),
        related_name='executions'
    )

    dispatch_time = models.DateTimeField(
        _("dispatch time"), null=True,
        help_text=_help_texts['dispatch_time']
    )
    saga_id = models.CharField(_("id"), max_length=255, help_text=_help_texts['saga_id'])
    state = models.CharField(
        _("state"), choices=STATES,
        max_length=10, default=STATES.Unknown,
        help_text=_help_texts['state']
    )
    last_state_change = MonitorField(
        _("last state change"), monitor='state', null=True,
        help_text=_help_texts['last_state_change']
    )
    exit_code = models.SmallIntegerField(_("exit code"), null=True)

    def get_zipped_results_filename(self, filename=None):
        filename = self.saga_id.replace('://', '@') + '.zip'
        return posixpath.join(settings.RESULT_FILES_DIR, filename)
    zipped_results = models.FileField(
        _("zipped results"),
        storage=default_private_storage,
        upload_to=get_zipped_results_filename,
        help_text=_help_texts['zipped_results']
    )

    _task = models.TextField(_('celery task'))

    class Meta(SagaModel.Meta):
        verbose_name = _("job")
        verbose_name_plural = _("jobs")

    def __unicode__(self):
        return self.saga_id if self.saga_id else "%s object" % self.__class__.__name__

    def get_absolute_url(self):
        return reverse('detail_job', args=[self.pk])

    @property
    def sent(self):
        return bool(self.saga_id)

    @property
    def finished(self):
        return self.state in Job.FINISHED_STATES

    def to_saga(self, service=None):
        service = service or self.service.to_saga()
        if self.saga_id:
            return service.get_job(self.saga_id)
        else:
            description = self.description.to_saga()
            return service.create_job(description)

    def handle_async(self):
        from .. import tasks

        subtasks = []
        if not self.sent:
            dep_files = self.description.files.uploads()
            if dep_files:
                upload_deps = celery.group(tasks.transfer_file.si(self.pk, dep_file.pk)
                                           for dep_file in dep_files)
                subtasks.append(upload_deps)
            subtasks.append(tasks.send_job.si(self.pk))

        subtasks.append(tasks.poll_job.si(self.pk))

        result_files = self.description.files.downloads()
        if result_files:
            download_results = celery.group(tasks.transfer_file.si(self.pk, result_file.pk)
                                            for result_file in result_files)
            subtasks.append(download_results)
            subtasks.append(tasks.zip_results.si(self.pk))

        result = celery.chain(*subtasks).delay()
        self._task = pickle.dumps(result.serializable())
        self.save(update_fields=['_task'])
        return result

    def send(self, save=True):
        with self.service.connect() as service:
            saga_job = self.to_saga(service)
            saga_job.run()
        self.dispatch_time = now()
        self.saga_id = saga_job.id
        # use same saga job instance and avoid
        # recovering job from remote host
        self.update(saga_job, save=False)
        if save:
            self.save()

    def update(self, saga_job=None, save=True):
        if not saga_job:
            with self.service.connect() as service:
                saga_job = self.to_saga(service)
        self.state = saga_job.state
        if self.finished:
            self.exit_code = saga_job.exit_code
        if save:
            self.save()

    def cancel(self, save=True):
        if self._task:
            task = celery.result.from_serializable(pickle.loads(self._task))
            while task:
                task.revoke()
                task = task.parent
            self._task = ""
        if self.sent and not self.finished:
            with self.service.connect() as service:
                saga_job = self.to_saga(service)
                saga_job.cancel()
        self.state = self.STATES.Canceled
        if save:
            self.save()

    def zip_results(self, save=True):
        self.zipped_results.name = self.get_zipped_results_filename()
        with zipfile.ZipFile(self.zipped_results.path, 'w',
                             zipfile.ZIP_DEFLATED, allowZip64=True) as zip_file:

            results_dir = settings.RESULT_FILES_DIR
            for download in self.file_transfers.filter(done=True).exclude(result=''):
                result = download.result
                storage = result.field.storage

                if not os.path.isdir(result.path):
                    destination_path = os.path.relpath(result.name, results_dir)
                    zip_file.write(result.path, destination_path)
                else:
                    for dirname, subdirs, files in walk(storage, result.name):
                        if not files:  # empty directory
                            destination_path = os.path.relpath(dirname, results_dir)
                            zip_file.write(storage.path(dirname), destination_path)
                        for filename in files:
                            file_path = os.path.join(dirname, filename)
                            destination_path = os.path.relpath(file_path, results_dir)
                            zip_file.write(storage.path(file_path), destination_path)
        if save:
            self.save()
