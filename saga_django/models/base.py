
from django.db import models

from guardian.shortcuts import assign_perm, get_objects_for_user
from model_utils.managers import InheritanceManager, InheritanceQuerySet


class SagaModelQuerySet(InheritanceQuerySet):

    def accessible_by(self, user):
        return get_objects_for_user(user, 'can_access', self)


class SagaModelManager(InheritanceManager):

    def get_query_set(self):
        return SagaModelQuerySet(self.model)

    def accessible_by(self, user):
        return self.get_query_set().accessible_by(user)

    def create_and_grant_access(self, user=None, **kwargs):
        user = user or kwargs.pop('user')
        instance = self.create(**kwargs)
        assign_perm('can_access', user, instance)
        return instance


class SagaModel(models.Model):

    objects = SagaModelManager()

    class Meta:
        abstract = True
        app_label = 'saga_django'
        permissions = (
            ('can_access', "can access this"),
        )

    def grant_access(self, user):
        assign_perm('can_access', user, self)

    def to_saga(self):
        raise NotImplementedError("SagaModel subclasses must implement a 'to_saga' method.")
