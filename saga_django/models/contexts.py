
from base64 import b64decode
import posixpath
from uuid import uuid4

from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import signals
from django.template.defaultfilters import slugify
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _

from Crypto.Hash import MD5
from Crypto.PublicKey import RSA
from django_extensions.db.fields.encrypted import EncryptedCharField
import saga

from .. import settings
from ..files import FileCleaner, PrivateFileSystemStorage, PrivateKeyFilesStorage
from ..utils import get_current_site_name
from .base import SagaModel


class Context(SagaModel):

    class Meta(SagaModel.Meta):
        verbose_name = _("context")
        verbose_name_plural = _("contexts")


class SSHContext(Context):

    _help_texts = {
        'alias': _("Any value to help you distinguish this credential from others."),
        'user_id': _("The system user ID account that will be used to manage those remote resources"
                     " with this key assigned."),
    }

    alias = models.CharField(
        _("alias"), max_length=30,
        help_text=_help_texts['alias']
    )
    user_id = EncryptedCharField(
        _("user ID"), max_length=30,
        validators=[RegexValidator(
            r'^[a-z]\w*$',
            message=_("This value may contain only letters, numbers and underscores"
                      " (but it cannot begin with '_').")
        )],
        help_text=_help_texts['user_id']
    )
    user_pass = EncryptedCharField(
        _("key password"),
        max_length=30,
        default=lambda: get_random_string(length=30)
    )

    def get_user_cert_filename(self, filename):
        return posixpath.join(settings.SSH_KEYS_DIR, str(uuid4()))
    user_cert = models.FileField(
        _("private key file"),
        storage=PrivateKeyFilesStorage(), upload_to=get_user_cert_filename
    )
    user_key = models.FileField(
        _("public key file"),
        storage=PrivateFileSystemStorage(), upload_to=settings.SSH_KEYS_DIR
    )

    key_fingerprint = models.CharField(_("key fingerprint"), max_length=32)
    public_key = models.TextField(_("public key"))

    class Meta(Context.Meta):
        verbose_name = _("SSH security context")
        verbose_name_plural = _("SSH security contexts")

    def __unicode__(self):
        if self.alias:
            template = "%(alias)s: SSH (RSA) %(fingerprint)s"
            return template % {'alias': self.alias, 'fingerprint': self.fingerprint}
        return "%s object" % self.__class__.__name__

    def get_absolute_url(self):
        return reverse('detail_ssh_context', args=[self.pk])

    def save(self, *args, **kwargs):
        if not self.pk:
            self.generate()
        super(SSHContext, self).save(*args, **kwargs)

    def generate(self, save=False):
        private_key, self.public_key = self.generate_key_pair(passphrase=self.user_pass)
        self.key_fingerprint = self.construct_fingerprint(self.public_key)
        self.user_cert.save('', ContentFile(private_key), save=False)
        self.user_key.save(self.user_cert.name + '.pub', ContentFile(self.public_key), save=False)
        if save:
            self.save()

    @classmethod
    def generate_key_pair(cls, passphrase=None):
        key = RSA.generate(2048)
        private_key = key.exportKey(passphrase=passphrase)
        public_key = "%(key)s %(description)s" % {
            'key': key.publickey().exportKey(format='OpenSSH'),
            'description': slugify(get_current_site_name())
        }
        return private_key, public_key

    @classmethod
    def construct_fingerprint(cls, public_key):
        """Returns an MD5 fingerprint of the public key `from_key`

        As defined in http://www.ietf.org/rfc/rfc4716.
        """
        key_data = b64decode(public_key.split()[1])  # remove 'ssh-rsa' and OpenSSH trailing descriptions
        return MD5.new(key_data).hexdigest()

    @property
    def fingerprint(self):
        """Humanized representation of the public key fingerprint (split with ':').

        Example: 49:d3:cb:f6:00:d2:93:43:a6:27:07:ca:12:fd:5d:98
        """
        return ':'.join(a + b for a, b in zip(self.key_fingerprint[::2], self.key_fingerprint[1::2]))

    def to_saga(self):
        context = saga.Context('SSH')
        context.user_id = self.user_id
        context.user_pass = self.user_pass
        context.user_cert = self.user_cert.path
        return context


signals.pre_save.connect(FileCleaner.on_change, sender=SSHContext)
signals.post_delete.connect(FileCleaner.on_delete, sender=SSHContext)
