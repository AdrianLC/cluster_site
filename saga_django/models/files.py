
import posixpath

from django.db import models
from django.utils.translation import ugettext_lazy as _

from model_utils.choices import Choices
import saga

from ..files import default_private_storage
from .. import settings
from .. import validators
from .base import SagaModel, SagaModelManager


class JobFileManager(SagaModelManager):

    def uploads(self):
        return self.get_queryset().filter(action=JobFile.ACTIONS.upload)

    def downloads(self):
        return self.get_queryset().filter(action=JobFile.ACTIONS.download)


class JobFile(SagaModel):

    _help_texts = {
        'local': _("The file that must be sent to the remote host."),
        'remote_path': _("The remote path. You can use absolute paths (starting with '/') "
                         "or paths relative to the working directory.")
    }

    ACTIONS = Choices(
        (1, 'upload', _("upload")),
        (2, 'download', _("download")),
    )

    PECULIAR_FILES = Choices(
        (1, 'output', _("output")),
        (2, 'error', _("error")),
    )

    job_description = models.ForeignKey('JobDescription', related_name='files', verbose_name=_("job"))
    action = models.SmallIntegerField(_("action"), choices=ACTIONS, default=ACTIONS.download)
    peculiarity = models.SmallIntegerField(_("file peculiarity"), choices=PECULIAR_FILES, null=True)
    local = models.FileField(
        _("local file"),
        storage=default_private_storage,
        upload_to=settings.DEPENDENCY_FILES_DIR,
        help_text=_help_texts['local']
    )
    remote_path = models.CharField(
        _("remote path"), max_length=255,
        validators=[validators.file_path],
        help_text=_help_texts['remote_path']
    )

    objects = JobFileManager()

    class Meta(SagaModel.Meta):
        verbose_name = _("job file")
        verbose_name_plural = _("job files")
        unique_together = (('job_description', 'remote_path', 'action'),
                           ('job_description', 'peculiarity'))

    def __unicode__(self):
        if self.remote_path:
            return "%s file: %s" % (self.get_action_display().capitalize(), self.remote_path)
        return "%s object" % self.__class__.__name__


class FileTransfer(SagaModel):

    _help_texts = {
        'done': _("Transfer complete?")
    }

    job = models.ForeignKey('Job', related_name='file_transfers', verbose_name=_("job"))
    file = models.ForeignKey(JobFile, related_name='transfers', verbose_name=_("file"))
    done = models.BooleanField(_("done"), default=False, help_text=_help_texts['done'])

    def get_result_filename(self, filename):
        escaped_job_saga_id = self.job.saga_id.replace('://', '@')
        filename = posixpath.normpath(posixpath.join(escaped_job_saga_id, filename))
        return posixpath.join(settings.RESULT_FILES_DIR, filename)
    result = models.FileField(
        _("obtained file"),
        storage=default_private_storage,
        upload_to=get_result_filename
    )

    class Meta(SagaModel.Meta):
        verbose_name = _("file transfer")
        verbose_name_plural = _("file transfers")
        unique_together = ('job', 'file')

    def run(self, save=True):
        if self.file.action == JobFile.ACTIONS.upload:
            self.upload()
        elif self.file.action == JobFile.ACTIONS.download:
            self.download()
        self.done = True
        if save:
            self.save()

    def upload(self):
        remote_workdir = saga.filesystem.Directory(
            self._get_remote_url(self.job.description.working_directory, absolute=True),
            flags=saga.filesystem.CREATE_PARENTS,
            session=self._get_session()
        )
        remote_workdir.copy(
            self._get_local_url(), self._get_remote_url(),
            flags=saga.filesystem.CREATE_PARENTS | saga.filesystem.RECURSIVE
        )
        remote_workdir.close()

    def download(self):
        remote_file = saga.filesystem.File(self._get_remote_url(), session=self._get_session())
        local_filename = posixpath.basename(self.file.remote_path)
        self.result.name = self.get_result_filename(local_filename)
        remote_file.copy(
            self._get_local_url(self.result.path),
            flags=saga.filesystem.CREATE_PARENTS | saga.filesystem.RECURSIVE
        )
        remote_file.close()

    def _get_session(self):
        session = saga.Session(default=False)  # don't load default contexts
        for context in self.job.service.contexts.select_subclasses().all():
            session.add_context(context.to_saga())
        return session

    def _get_local_url(self, path=''):
        return "file://localhost/" + (path or self.file.local.path)

    def _get_remote_url(self, path='', absolute=False):
        path = path or self.file.remote_path
        if not absolute and not posixpath.isabs(path):
            path = posixpath.join(self.job.description.working_directory, path)
        return "sftp://%s/%s" % (self.job.service.host, path)
