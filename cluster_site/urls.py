
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import TemplateView, RedirectView
from django.views.i18n import javascript_catalog


# Uncomment the next two lines to enable the admin:
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url=reverse_lazy('list_jobs'))),
    url(r'^sitemap/$', TemplateView.as_view(template_name='sitemap.html'), name='sitemap'),

    url(r'^admin/', include(admin.site.urls)),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^account/', include('%s.auth_urls' % settings.SITE_NAME)),
    url(r'^account/', include('approval_registration.urls')),

    url(r'', include('saga_django.urls')),

    url(r'^jsi18n/$', javascript_catalog, settings.JS_TRANSLATIONS_CATALOG, name='jstranslations'),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
