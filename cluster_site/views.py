
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse_lazy
from django.views.generic import DeleteView

from braces.views import LoginRequiredMixin


User = get_user_model()


class DeleteOwnAccountView(LoginRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy('logout')
    template_name = "registration/account_confirm_delete.html"

    def get_object(self, queryset=None):
        return self.request.user
