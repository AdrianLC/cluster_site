
from django.conf.urls import patterns, url
from django.contrib.auth.views import (
    login, logout,
    password_change, password_change_done,
    password_reset, password_reset_done, password_reset_confirm, password_reset_complete
)

from .views import DeleteOwnAccountView


urlpatterns = patterns('',
    url(r'^login/$', login, name='login'),
    url(r'^logout/$', logout, name='logout'),

    url(r'^password_change/$', password_change, name='password_change'),
    url(r'^password_change/done/$', password_change_done, name='password_change_done'),

    url(r'^password_reset/$', password_reset, name='password_reset'),
    url(r'^password_reset/confirm/pending/$', password_reset_done, name='password_reset_done'),
    url(r'^password_reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        password_reset_confirm, name='password_reset_confirm'),
    url(r'^password_reset/done/$', password_reset_complete, name='password_reset_complete'),

    url(r'^settings/$', password_change, name='account_settings'),
    url(r'^delete/$', DeleteOwnAccountView.as_view(), name="delete_account"),
)
