
(function($) {

$.extend($.fn.select2.defaults, {
    formatNoMatches: function() {
        return gettext("No matches found");
    },
    formatInputTooShort: function(input, min) {
        var n = min - input.length,
            str = ngettext("Please enter another character", "Please enter %s characters", n);
        return interpolate(str, [n]);
    },
    formatInputTooLong: function(input, max) {
        var n = input.length - max,
            str = ngettext("Please delete another character", "Please delete %s characters", n);
        return interpolate(str, [n]);
    },
    formatSelectionTooBig: function(limit) {
        var str = ngettext("You can only select an item", "You can only select %s items", n);
        return interpolate(str, [limit]);
    },
    formatLoadMore: function(pageNumber) {
        return gettext("Loading more results...");
    },
    formatSearching: function() {
        return gettext("Searching...");
    }
});

})(jQuery);
