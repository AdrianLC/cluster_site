
/* global AjaxController */

(function($, AjaxController) {

    $.extend(AjaxController.prototype, {

        periodicallyRefreshJobTable: function(frequency) {
            var refresh = function() {
                window.ui.jobTable.api().ajax.reload();
                setTimeout(refresh, frequency);
            };
            setTimeout(refresh, frequency);
        }

    });

})(jQuery, AjaxController);
