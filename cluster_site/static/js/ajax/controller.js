
window.AjaxController = (function($) {

    var AjaxController = function() {
        this.csrfPatch();
    };

    AjaxController.prototype = {

        csrfPatch: function() {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            var csrfSafeMethod = function(method) {
                return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
            };
            $(document).ajaxSend(function(event, xhr, settings) {
                if (!settings.crossDomain && !csrfSafeMethod(settings.type)) {
                    xhr.setRequestHeader("X-CSRFToken", $.cookie('csrftoken'));
                }
            });
        }

    };

    window.ajaxController = new AjaxController();

    return AjaxController;

})(jQuery);
