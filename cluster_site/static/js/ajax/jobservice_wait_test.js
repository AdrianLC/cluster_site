
/* global AjaxController */

(function($, AjaxController) {

    $.extend(AjaxController.prototype, {

        waitJobServiceTest: function(url, frequency) {
            var poll = function() {
                $.ajax({
                    url: url,
                    dataType: 'json'
                })
                .always(function() {
                    setTimeout(poll, frequency);
                })
                .done(function(data) {
                    if(data) {
                        window.ui.setProgressBarState(100, gettext("Test finished! Redirecting..."));
                        window.location = data.redirect;
                    }
                });
            };
            setTimeout(poll, frequency);
        }

    });

})(jQuery, AjaxController);
