
/* global UI */

(function($, UI) {

    $.extend(UI.prototype, {

        drawJobTable: function() {
            return $('#jobs').dataTable({
                ajax: {
                    url: "",
                    dataSrc: ""
                },
                columns: [
                    {
                        data: 'description',
                        render: function(description, type, jobData) {
                            return '<a href="' + description.get_absolute_url + '">' +
                                        description.__str__ +
                                   '</a>';
                        }
                    },
                    {
                        data: 'service',
                        render: function(service, type, jobData) {
                            return '<a href="' + service.get_absolute_url + '">' +
                                        service.__str__ +
                                   '</a>';
                        }
                    },
                    { data: 'dispatch_time' },
                    { data: 'state' },
                    { data: 'last_state_change' },
                    { data: 'exit_code' },
                    {
                        data: 'user_action',
                        render: function(linkData, type, jobData) {
                            if(linkData === null) { return "&nbsp;"; }
                            var url = linkData[0], anchorText = linkData[1];
                            return '<a href="' + url + '">' + anchorText + '</a>';
                        }
                    },
                ]
            });
        }

    });

    $(document).ready(function() {
        this.jobTable = this.drawJobTable();
    }.bind(window.ui));

})(jQuery, UI);
