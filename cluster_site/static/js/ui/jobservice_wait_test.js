
/* global UI, gettext */

(function($, UI) {

    $.extend(UI.prototype, {

        addProgressBar: function() {
            var $progressbar = $('.progressbar'),
                $label = $progressbar.find('.label');
            $progressbar.progressbar({value: false});
            $label.text(gettext("Please wait..."));
            $progressbar.data('label', $label);
            return $progressbar;
        },

        setProgressBarState: function(percentage, label) {
            if(this.$progressbar) {
                if(percentage) {
                    this.$progressbar.progressbar('option', 'value', percentage);
                }
                if(label) {
                    this.$progressbar.data('label').text(label);
                }
            }
        }

    });

    $(document).ready(function() {
        this.$progressbar = this.addProgressBar();
    }.bind(window.ui));

})(jQuery, UI);
