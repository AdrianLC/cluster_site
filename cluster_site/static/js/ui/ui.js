
window.UI = (function($) {

    var UI = function() { };

    UI.prototype = {

        STYLE: {

            icons: {
                close: 'ui-icon ui-icon-closethick'
            },

            helptextTooltip: {
                cls: 'helptext-tooltip',
                distance: '20'
            }

        },

        init: function() {
            $(document).ready(function() {
                this.improveSelects();  // select2
                this.addFormTooltips();  // jQuery UI tooltip
                this.addCloseButtons();
                $('textarea').autosize({append: '\n'});
            }.bind(this));
            return this;
        },

        // INTERACTION

        improveSelects: function($form) {
            // Replace <selects/> with autocomplete inputs:
            // (Keep before the tooltips so that the autocomplete can get its tooltip as well)
            var $selects = $form ? $form.find('select') : $('select').not(':hidden'),
                $singles = $selects.not('[multiple]');
            $singles.find('option:first:contains("----")').text(gettext("Choose..."));
            $selects.select2({
                placeholder: gettext("Choose one or more"),
                width: 'resolve',
                dropdownAutoWidth: true,
                minimumResultsForSearch: 8
            });
        },

        addFormTooltips: function() {
            // Form hover/focus tooltips with content taken from django's generated span
            // which can be identified byt their .helptext class
            $('form').tooltip({
                position: { // my = tooltip, at = target
                    my: 'left+' + this.STYLE.helptextTooltip.distance,
                    at: 'right'
                },
                items: ':input:not(.select2-container input), .select2-container',
                content: function() {
                    return $(this).siblings('.helptext').first().html(); // this = input | container
                },
                tooltipClass: this.STYLE.helptextTooltip.cls
            });
        },

        addCloseButtons: function() {
            // Add close buttons to elements with class '.closable':
            var $button = this._getIconButton(
                    'close', gettext("Close"),
                    function() {
                        $(this).parent().remove();
                    }
                ).addClass('right');
            $('.closable').prepend($button);
        },

        // UTILS

        _getIconButton: function(icon, title, clickCallback) {
            var $icon = $('<span/>').addClass(this.STYLE.icons[icon]),
                $button = $('<button type="button" class="icon"/>').append($icon);
            if(typeof title !== 'undefined') {
                $button.attr('title', title);
            }
            if(typeof clickCallback === 'function') {
                $button.click(clickCallback);
            }
            return $button;
        }

    };

    window.ui = new UI().init();

    return UI;

})(jQuery);
