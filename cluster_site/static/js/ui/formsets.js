
/* global UI, gettext */

(function($, UI, gettext) {

    $.extend($.fn.formset.defaults, {
        addText: gettext("Add"),
        deleteText: gettext("Remove")
    });


    $.extend(UI.prototype, {

        makeFormSetDynamic: function(prefix, buildWidgets) {
            $formContainer = $('.' + prefix + '-form');
            if(buildWidgets) {
                $formContainer.find('select').select2('destroy');
            }
            $formContainer.formset({
                prefix: prefix,
                formTemplate: '#' + prefix + '-form-template',
                added: function($newForm) {
                    if(buildWidgets) {
                        window.ui.improveSelects($newForm);
                    }
                }
            });
        }

    });

})(jQuery, UI, gettext);
