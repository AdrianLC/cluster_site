
from .base import *  # noqa


DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cluster_site',
        'USER': 'cluster_site_django_user',
        'PASSWORD': 'clusters',
        'HOST': '',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',  # Set to empty string for default.
    }
}

# See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}

STATIC_ROOT = PROJECT_DIR.child('collected')
MEDIA_ROOT = PROJECT_DIR.child('files')
PRIVATE_MEDIA_ROOT = PROJECT_DIR.child('priv')

COMPRESS_ENABLED = True

BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/1"

CELERYBEAT_SCHEDULE = {
    # Executes every Saturday night at 0:00 A.M
    # crontab(hour=0, minute=0, day_of_week='saturday')
    # 'schedule-name': {
    #     'task': 'saga_django.tasks...',
    #     'schedule': crontab(...')
    # },
}
CELERYBEAT_SCHEDULE_FILENAME = PROJECT_DIR.parent.child('celerybeat-schedule')

THIRD_PARTY_APPS += (
    'django_extensions',
    'debug_toolbar',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# DEBUG TOOLBAR CONFIG
# See: http://django-debug-toolbar.readthedocs.org/en/1.0/installation.html

DEBUG_TOOLBAR_PANELS = (
    'debug_toolbar.panels.timer.TimerPanel',
    'debug_toolbar.panels.headers.HeadersPanel',
    'debug_toolbar.panels.request.RequestPanel',
    'debug_toolbar.panels.settings.SettingsPanel',
    'debug_toolbar.panels.templates.TemplatesPanel',
    'debug_toolbar.panels.staticfiles.StaticFilesPanel',
    'debug_toolbar.panels.sql.SQLPanel',
    'debug_toolbar.panels.cache.CachePanel',
    'debug_toolbar.panels.signals.SignalsPanel',
    'debug_toolbar.panels.logging.LoggingPanel',
    'debug_toolbar.panels.versions.VersionsPanel',
    # 'debug_toolbar.panels.redirects.RedirectsPanel',
)

# SAGA_DJANGO:
ENCRYPTED_FIELD_KEYS_DIR = PRIVATE_MEDIA_ROOT.child('enc')
