
from tempfile import gettempdir

from base import *  # noqa


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:',
    },
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'cluster_site',
#         'USER': 'cluster_site_django_user',
#         'PASSWORD': 'clusters',
# }

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
)

INSTALLED_APPS += ('saga_django.tests', )

MEDIA_ROOT = Path(gettempdir()).child('%s_testfiles' % SITE_NAME)

LANGUAGE_CODE = 'en-us'

TEST_DISCOVER_TOP_LEVEL = PROJECT_DIR.parent
TEST_DISCOVER_PATTERN = "test*.py"

#ignore the following error when using ipython:
#/django/db/backends/sqlite3/base.py:50: RuntimeWarning:
#SQLite received a naive datetime (2012-11-02 11:20:15.156506) while time zone support is active.

import warnings
import exceptions
warnings.filterwarnings("ignore", category=exceptions.RuntimeWarning,
                        module='django.db.backends.sqlite3.base', lineno=58)
