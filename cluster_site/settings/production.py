
from .base import *  # noqa

DEBUG = False

ALLOWED_HOSTS = [
    # "citius.clusters.es",
    "localhost", "127.0.0.1",  # TODO: Comentar cuando haya hostname
]

ADMINS = (
    ("admin", "admin@citius.clusters.es"),
)
MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cluster_site',
        'USER': 'cluster_site',
        'PASSWORD': '',  # Leave empty for postgresql's peer authentication (domain sockets)
        'HOST': '',  # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',  # Set to empty string for default.
    }
}

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.cache.RedisCache',
        'LOCATION': '127.0.0.1:6379:2',
        'OPTIONS': {
            'CLIENT_CLASS': 'redis_cache.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True,
        }
    }
}

DEPLOYMENT_ROOT = Path('/var/www/wsgi/cluster_site/')

STATIC_ROOT = DEPLOYMENT_ROOT.child('static')
MEDIA_ROOT = DEPLOYMENT_ROOT.child('media')
PRIVATE_MEDIA_ROOT = DEPLOYMENT_ROOT.child('priv')

COMPRESS_ENABLED = True

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

# EMAIL CONFIG

EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_PASSWORD = ''
EMAIL_HOST_USER = 'your_email@example.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

SERVER_EMAIL = EMAIL_HOST_USER

# END EMAIL CONFIG

# APP SETTINGS:

# CELERY:
BROKER_URL = "redis://localhost:6379/0"
CELERY_RESULT_BACKEND = "redis://localhost:6379/1"
CELERYBEAT_SCHEDULE_FILENAME = PROJECT_DIR.parent.child('celerybeat-schedule')

# DOWNLOADVIEW
MIDDLEWARE_CLASSES += ('django_downloadview.SmartDownloadMiddleware', )
DOWNLOADVIEW_BACKEND = 'django_downloadview.apache.XSendfileMiddleware'
DOWNLOADVIEW_RULES = [
    {
        'source_url': PRIVATE_MEDIA_URL,
        'destination_dir': PRIVATE_MEDIA_ROOT,
    },
]

# SAGA_DJANGO:
ENCRYPTED_FIELD_KEYS_DIR = PRIVATE_MEDIA_ROOT.child('enc')

# END APP SETTINGS

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'null': {
            'class': 'logging.NullHandler',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG' if DEBUG else 'WARNING'
        },
        'mail_admins': {
            'class': 'django.utils.log.AdminEmailHandler',
            'level': 'ERROR',
            'filters': ['require_debug_false'],
        }
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django.security': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'py.warnings': {
            'handlers': ['console'],
        },
        'saga_django': {
            'handlers': ['console'],
        }
    }
}

SECRET_KEY = '8r(&2pxj_*%hvp+3n#-%pf2hrr544zaxb#w2%7me&#v+@y0tqp'
