# Instrucciones de instalación y despliegue

*La instalación requiere permisos de superusuario.
Sesión `sudo su` o ejecutar todos los comandos con `sudo`.*


## Dependencias

```bash
$ apt-get install apache2 libapache2-mod-wsgi libapache2-mod-xsendfile \
                  postgresql postgresql-server-dev-all                 \
                  supervisor redis-server openssh-client               \
                  git python-pip python-virtualenv                     \
                  python-dev build-essential gettext
```

Clonar el respositorio y instalar las dependencias en un virtualenv:
```bash
$ cd /usr/local
# o donde se quiera guardar
# (pero entonces habría que modificar rutas en todos los ficheros de configuración)
$ git clone https://AdrianLC@bitbucket.org/AdrianLC/cluster_site.git
$ virtualenv virtualenvs/cluster_site
$ source virtualenvs/cluster_site/bin/activate
$ pip install -r cluster_site/requirements/production.txt
```

## Configuración

Crear un usuario para todo lo relacionado con el portal:
```
$ useradd cluster_site --system --create-home  # Saga necesita un $HOME
```

Crear base de datos:
```bash
$ su postgres -c "createuser cluster_site && createdb cluster_site --owner=cluster_site"
```

Crear directorios para el despliegue:
```bash
$ mkdir -p /var/www/wsgi/cluster_site /var/log/cluster_site
```

Asignar los permisos al usuario:
```bash
$ chown -R cluster_site:cluster_site \
           /var/www/wsgi/cluster_site /usr/local/cluster_site /var/log/cluster_site
```

Actualizar configuración de django:
```bash
$ vim /usr/local/cluster_site/cluster_site/settings/production.py
```
```python
# Esto tiene que coincidir con el ServerName/ServerAlias de la conf. de apache:
ALLOWED_HOSTS = [
    # "citius.clusters.es",
    # Si no se tiene hostname:
    "localhost", "127.0.0.1",
    # "XXX.XXX.XXX.XXX",  # La IP estática o,
    # "192.168.XXX.XXX",  # Si es para probar en una red privada
]

# configurar el acceso a la BD:
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'cluster_site',  # nombre de BD creada
        'USER': 'cluster_site',  # nombre de usuario
        'PASSWORD': ''  # no se necesita al utilizar autentificación 'peer' de postgres.
        # ....
    }
}

# en la sección de EMAIL CONFIG,
# la configuración es similar a la de cualquier cliente de correo:
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_PASSWORD = 'xxxx'
EMAIL_HOST_USER = 'your_email@example.com'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# también conviene cambiar la SECRET_KEY que está al final
# y que se usa para encriptación, etc.
# (Se puede usar cualquier cadena de caraceteres)
```

Configurar apache:
```bash
$ cp /usr/local/cluster_site/cluster_site/server_conf/httpd.conf \
     /etc/apache2/sites-available/cluster_site.conf
$ vim /etc/apache2/sites-available/cluster_site.conf
# Modificar ServerName y/o ServerAlias con el hostname y/o IPs igual que en ALLOWED_HOSTS
```

Configurar supervisord para daemonizar redis y celery:
```bash
# desactivar el servicio automático que viene con la instalación de redis de ubuntu:
$ service redis-server stop && update-rc redis-server disable

cp /usr/local/cluster_site/cluster_site/server_conf/supervisord.ini \
   /etc/supervisor/conf.d/cluster_site.conf
```

## Despliegue

```bash
$ su cluster_site
$ cd /usr/local/cluster_site
$ source /usr/local/virtualenvs/cluster_site/bin/activate
$ export DJANGO_SETTINGS_MODULE=cluster_site.settings.production
```

Copiar el script wsgi:
```bash
$ cp /usr/local/cluster_site/cluster_site/wsgi.py /var/www/wsgi/cluster_site
```

Desplegar el contenido estático de la web:
```bash
# crear una clave de cifrado
$ ./manage.py changecryptkey
# compilar traducciones:
$ ./manage.py compilemessages
$ ./manage.py compilejsi18n
# desplegar scripts, css, imágenes...
$ ./manage.py collectstatic
```

Crear las tablas en la BD:
```bash
$ ./manage.py syncdb && ./manage.py migrate
# (Crear un usuario administrador de django)
```

```bash
$ exit # volver a root
```

Iniciar redis y celery mediante supervisor
```
$ service supervisor restart
$ supervisorctl status  # deben aparecer dos lineas para redis y celery
```

Activar el sitio en apache
```bash
$ a2ensite cluster_site
$ service apache2 reload
```






